from scour import scour
import argparse
import os
from os.path import isfile, join
import subprocess

def dir_path(path):
    if os.path.isdir(path):
        return path
    else:
        raise argparse.ArgumentTypeError(f"{path} is not a valid path")


def parse_args():
    parser = argparse.ArgumentParser(description='Create model.')
    parser.add_argument("--path", required=True, metavar="OUTPUT", type=dir_path, help="output directory")
    args = parser.parse_args()
    return args.path


def optimize_svg_image(input_svg_image, output_svg_image):
    print(f"Convert File {input_svg_image} to {output_svg_image}")
    with open(input_svg_image, 'r', encoding='UTF-8') as s:
        svg_content = s.read()

        # Standard-Optionen für die Optimierung einstellen
        options = scour.parse_args([])
        # Optimierte SVG-Datei generieren

        optimized_svg = scour.scourString(svg_content, options)

        # Ausgabe der optimierten SVG
        with open(output_svg_image, 'w', encoding='UTF-8') as f:
            f.write(optimized_svg)


def convert_dot_file(input_dot_file, output_image_file):
    print(f"{input_dot_file} => {output_image_file}")
    subprocess.run(["dot", "-Tsvg", input_dot_file, "-o", output_image_file], check=True)


path = parse_args()

# Change Working directory, for references png Images
os.chdir(path)

for input_file in os.listdir("."):
    if not isfile(input_file):
        continue

    # Convert dot files
    if input_file.lower().endswith(".dot"):
        tmp_file = input_file[:-4] + ".tmp.svg"
        convert_dot_file(input_file, tmp_file)

        output_file = input_file[:-4] + ".svg"
        optimize_svg_image(tmp_file, output_file)

        os.remove(tmp_file)
