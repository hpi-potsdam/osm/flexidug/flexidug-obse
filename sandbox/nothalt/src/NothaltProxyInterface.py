# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.

from abc import ABC, abstractmethod
from typing import Callable
from .EmergencyStop import EmergencyStop
from .EmergencyStopControl import EmergencyStopControl
from .EmergencyStopRequest import EmergencyStopRequest

class NothaltProxyInterface(ABC):

    @abstractmethod
    def set_EmergencyStop_callback(self,callback : Callable[[EmergencyStop], None]) -> None: pass
    @abstractmethod
    def set_EmergencyStopControl_callback(self,callback : Callable[[EmergencyStopControl], None]) -> None: pass
  
    @abstractmethod
    def apply_EmergencyStopRequest(self,emergency_stop_request : EmergencyStopRequest) -> None: pass

    @abstractmethod
    def tick(self,cycle) -> None: pass


