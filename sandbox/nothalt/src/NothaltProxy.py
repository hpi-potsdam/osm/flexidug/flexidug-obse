from typing import Callable
import logging
import os
import uuid
from datetime import datetime
from datetime import timedelta
from fastapi import Depends

from zlicutils.tan import TanManager
from zlicutils.server import Server

from .EmergencyStop import EmergencyStop
from .EmergencyStopRequest import EmergencyStopRequest
from .EmergencyStopControl import EmergencyStopControl
from .NothaltProxyInterface import NothaltProxyInterface



class NothaltProxy(NothaltProxyInterface):

    def __init__(self, with_server=True):

        self.set_emergency_stop = None
        self.set_emergency_stop_control = None
        self.emergency_stop_repeat_msg = None
        self.emergency_stop_repeat_timer = None
        self.protocol = []
        self.nothalt = False

        if with_server:
            self.tan_manager = TanManager()
            server_port = os.environ.get("SERVER_PORT")
            if not server_port:
                logging.error("environment SERVER_PORT must be set. Using default port 8080.")
                server_port = '8080'

            self.server = Server(host='0.0.0.0', port=int(server_port))
            self.server.router.add_api_route("/protocol", self._protocol, methods=["GET"])
            self.server.router.add_api_route("/nothalt/{source_id}", self._nothalt, methods=["GET"])
            self.server.router.add_api_route("/tan", self.tan_manager.tans_as_html, methods=["GET"])
            self.server.router.add_api_route("/nothalt_aufheben/{source_id}", self._nothalt_aufheben, methods=["GET"], dependencies=[Depends(self.tan_manager.verify_tan)])

            self.server.start()

    async def _nothalt(self, source_id):

        logging.info(f"Recv Nothalt via Api id: {source_id}")
        self._trigger_nothalt("api", source_id)

        return {"msg": "Emergency stop triggered"}

    async def _nothalt_aufheben(self, source_id):

        logging.info(f"Recv Nothalt Aufheben via Api id: {source_id}")
        self._reset_nothalt("api", source_id)

        return {"msg": "Cancel emergency stop triggered"}

    def _trigger_nothalt(self, trigger, source_id):


        # prüfen ob es bereits einen Nothalt gibt
        if self.nothalt:
            self.protocol.append({
                "type": "set_duplicate",
                "trigger": trigger,
                "id": source_id,
                "date": datetime.now()
            })
            return

        self.nothalt = True

        self.protocol.append({
            "type": "set",
            "trigger": trigger,
            "id": source_id,
            "date": datetime.now()
        })

        # TODO Zugmeldebuch informieren 

        # Digitales Belegblatt blockieren
        emergency_stop_control = EmergencyStopControl("disable")
        self.set_emergency_stop_control(emergency_stop_control)

        # Nothalt auslösen
        emergency_stop = EmergencyStop("Betriebsgefahr, alle Fahrten sofort anhalten!")
        self.set_emergency_stop(emergency_stop)

        self.emergency_stop_repeat_msg = EmergencyStop("Ich wiederhole. Betriebsgefahr, alle Fahrten sofort anhalten!")
        self.emergency_stop_repeat_timer = datetime.now() + timedelta(seconds=5)

    def _reset_nothalt(self, trigger, source_id):

        if not self.nothalt:
            self.protocol.append({
                "type": "cancel_duplicate",
                "trigger": trigger,
                "id": source_id,
                "date": datetime.now()
            })
            return
        
        self.protocol.append({
            "type": "cancel",
            "trigger": trigger,
            "id": source_id,
            "date": datetime.now()
        })

        # Zugmeldebuch informieren
        # TODO

        # Digitales Belegblatt freigeben
        emergency_stop_control = EmergencyStopControl("enable")
        self.set_emergency_stop_control(emergency_stop_control)

        # lokalen Zustand zurücksetzen
        self.nothalt = False

    async def _protocol(self):
        return list(reversed(self.protocol))

    def set_EmergencyStop_callback(self, callback: Callable[[EmergencyStop], None]) -> None:
        self.set_emergency_stop = callback

    def set_EmergencyStopControl_callback(self, callback: Callable[[EmergencyStopControl], None]) -> None:
        self.set_emergency_stop_control = callback

    def apply_EmergencyStopRequest(self, emergency_stop_request: EmergencyStopRequest) -> None:
        logging.info(f"Recv Nothalt via Call caller_id: {emergency_stop_request.caller_id} ")
        self._trigger_nothalt("call", emergency_stop_request.caller_id)

    def tick(self, _) -> None:
        if not self.emergency_stop_repeat_timer or not self.emergency_stop_repeat_msg:
            return

        if self.emergency_stop_repeat_timer > datetime.now():
            return

        # Repeat Nothalt
        logging.info("Repeat Nothalt")

        self.set_emergency_stop(self.emergency_stop_repeat_msg)
        self.emergency_stop_repeat_msg = None
        self.emergency_stop_repeat_timer = None
