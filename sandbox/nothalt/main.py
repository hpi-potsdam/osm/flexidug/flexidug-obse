import logging
import time
import os
from datetime import datetime, timedelta

from src.NothaltProxy import NothaltProxy
from src.EmergencyStop import EmergencyStop
from src.EmergencyStopControl import EmergencyStopControl
from src.EmergencyStopRequest import EmergencyStopRequest

logging.basicConfig(level=logging.INFO)


def emergency_stop(emergencyStop: EmergencyStop):
    print("Send emergencyStop", emergencyStop.text)


def emergency_stop_control(emergencyStopControl: EmergencyStopControl):
    print("Send emergencyStopControl", emergencyStopControl.command)


proxy = NothaltProxy()
proxy.set_EmergencyStop_callback(emergency_stop)
proxy.set_EmergencyStopControl_callback(emergency_stop_control)

next_emergency_stop_request = datetime.now() + timedelta(seconds=10)
try:
    while True:
        time.sleep(1)
        proxy.tick(1)

        if next_emergency_stop_request < datetime.now():
            next_emergency_stop_request = datetime.now() + timedelta(seconds=60)
            proxy.apply_EmergencyStopRequest(EmergencyStopRequest(caller_id="xxx"))


except Exception as e:
    print("Exception",e)
    os._exit(1)
