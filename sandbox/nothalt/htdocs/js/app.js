


function get_protocol() {

	$.get( "/protocol", function( data ) {
		table = ""

		console.log(data)

		table += "<table border='1'>"
		table += '<tr>'
		table += '<th>Typ</th>'
		table += '<th>Datum</th>'
		table += '<th>Auslöser</th>'
		table += '<th>Id</th>'
		table += '</tr>' 


		for(var i = 0;i < data.length;i++) {
			table += '<tr>'

			const date_str = moment(data[i].date).format("DD.MM.YYYY HH:mm:ss")

			table += '<td>' + data[i].type + '</td>'
			table += '<td>' + date_str + '</td>'
			table += '<td>' + data[i].trigger + '</td>'
			table += '<td>' + data[i].id + '</td>'
			table += '</tr>' 
		}
		table += "</table>"

		$("#protocol").html(table)


	});
	setTimeout(function() {
		get_protocol();
	}, 1000);

}




$( document ).ready(function() {


	$("#nothalt").click(function(ev){
		ev.preventDefault();

		$.ajax({
			url: "/nothalt/manuell",
			type: "get", //send it through get method
			success: function(response) {
				console.log("successful triggered!",response)
			},
			error: function(xhr) {
				alert(xhr.responseText);			}
		  });
	})

	$("#nothalt_delete").click(function(ev){
		ev.preventDefault();

		let tan = prompt("Nothalt aufheben.\nBitte geben Sie eine TAN ein:");
		if(tan) {
			$.ajax({
				type: "GET",
				url: "/nothalt_aufheben/manuell",
				cache: false,
				headers: {"Flexidug-Authorization-TAN": tan},
				success: function(data) {
					console.log('Success!',data); 
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			});
		}


	})

	get_protocol()
});