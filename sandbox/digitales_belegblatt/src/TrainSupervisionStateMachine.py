
class State:
    def __init__(self, name): 
        self.name = name
    def __repr__(self): 
        return self.name

class Event:
    def __init__(self, name): 
        self.name = name
    def __repr__(self): 
        return self.name

class Transition:
    def __init__(self,name, event , guard , action , source , target):
        self.name = name
        self.event = event
        self.guard = guard
        self.action = action
        self.source = source
        self.target = target


class TrainSupervisionStateMachine:


    # States
    state_init = State("Init")
    state_error = State("Error")
    state_deregistered = State("Deregistered")
    state_in_movement = State("InMovement")
    state_arrival = State("Arrival")
    state_confirmed_position = State("ConfirmedPosition")
    state_free_route = State("FreeRoute")
    state_driving_request = State("DrivingRequest")
    state_driving_permission = State("DrivingPermission")
    state_set_position = State("SetPosition")
    state_delete_position = State("DeletePosition")

    # Events
    event_timer = Event("Timer")
    event_registration_message = Event("RegistrationMessage")
    event_arrival_message = Event("ArrivalMessage")
    event_positive_confirmation_message = Event("PositiveConfirmationMessage")
    event_negative_confirmation_message = Event("NegativeConfirmationMessage")
    event_set_route_response_message = Event("SetRouteResponseMessage")
    event_free_route_response_message = Event("FreeRouteResponseMessage")
    event_driving_request_message = Event("DrivingRequestMessage")
    event_driving_permission_replay_message = Event("DrivingPermissionReplayMessage")
    event_deregister = Event("Deregister")

	# Guards
    def is_true(self,_):
        return True

    def is_valid_arrival_message(self,_) -> bool:
        raise ValueError("is_valid_arrival_message not implemented")

    def is_arrival_timeout(self,_) -> bool:
        raise ValueError("is_arrival_timeout not implemented")

    def is_correct_driving_permission(self,_) -> bool:
        raise ValueError("is_correct_driving_permission not implemented")

    def is_wrong_driving_permission(self,_) -> bool:
        raise ValueError("is_wrong_driving_permission not implemented")

    def is_driving_permission_timeout(self,_) -> bool:
        raise ValueError("is_driving_permission_timeout not implemented")

    def is_first_arrival(self,_) -> bool:
        raise ValueError("is_first_arrival not implemented")

    def is_last_arrival(self,_) -> bool:
        raise ValueError("is_last_arrival not implemented")

    def is_valid_route(self,_) -> bool:
        raise ValueError("is_valid_route not implemented")

    def is_positive_free_route_response(self,_) -> bool:
        raise ValueError("is_positive_free_route_response not implemented")

    def is_negative_free_route_response(self,_) -> bool:
        raise ValueError("is_negative_free_route_response not implemented")

    def is_free_route_timeout(self,_) -> bool:
        raise ValueError("is_free_route_timeout not implemented")

    def is_valid_driving_request(self,_) -> bool:
        raise ValueError("is_valid_driving_request not implemented")

    def is_positive_set_route_response(self,_) -> bool:
        raise ValueError("is_positive_set_route_response not implemented")

    def is_negative_set_route_response(self,_) -> bool:
        raise ValueError("is_negative_set_route_response not implemented")

    def is_set_route_timeout(self,_) -> bool:
        raise ValueError("is_set_route_timeout not implemented")


	# Actions
    def do_nop(self,_): #no operation
        pass

    # Send RegistrationConfirmation message
    # Set train number
    def do_registration(self,_) -> bool:
        raise ValueError("do_registration not implemented")

    # Send ArrivalReplay message
    # set Meldestelle
    # set ArrivalTimeout
    # set CallerId
    def do_arrival(self,_) -> bool:
        raise ValueError("do_arrival not implemented")

    # reset SetRouteTimeout
    # send DrivingPermission
    # set DrivingPermissionTimeout
    def do_positive_set_route_response(self,_) -> bool:
        raise ValueError("do_positive_set_route_response not implemented")

    # reset SetRouteTimeout
    # send WaitingNotification
    # reset meldestelle_from and meldestelle_to
    # reset CallerId
    def do_negative_set_route_response(self,_) -> bool:
        raise ValueError("do_negative_set_route_response not implemented")

    # reset SetRouteTimeout
    # reset meldestelle_from and meldestelle_to
    # reset CallerId
    def do_clear_driving_request(self,_) -> bool:
        raise ValueError("do_clear_driving_request not implemented")

    # reset DrivingPermissionTimeout
    # Send PositivConfirmation
    # reset CallerId
    def do_correct_driving_permission_replay(self,_) -> bool:
        raise ValueError("do_correct_driving_permission_replay not implemented")

    # reset DrivingPermissionTimeout
    # Send NegativConfirmation
    # reset CallerId
    # send free route
    # set free route timeout
    def do_wrong_driving_permission_replay(self,_) -> bool:
        raise ValueError("do_wrong_driving_permission_replay not implemented")

    # reset DrivingPermissionTimeout
    # reset CallerId
    def do_driving_permission_timeout(self,_) -> bool:
        raise ValueError("do_driving_permission_timeout not implemented")

    # set SetRouteTimeout
    # send SetRoute
    def do_set_position(self,_) -> bool:
        raise ValueError("do_set_position not implemented")

    # reset SetRouteTimeout
    # set first arrival false
    def do_positive_set_position(self,_) -> bool:
        raise ValueError("do_positive_set_position not implemented")

    # reset SetRouteTimeout
    # Send System Fehler
    def do_negative_set_position(self,_) -> bool:
        raise ValueError("do_negative_set_position not implemented")

    # reset SetRouteTimeout
    # Send System Fehler
    def do_set_position_timeout(self,_) -> bool:
        raise ValueError("do_set_position_timeout not implemented")

    # reset ArrivalTimeout
    # reset CallerId
    # reset Meldestelle
    def do_arrival_timeout(self,_) -> bool:
        raise ValueError("do_arrival_timeout not implemented")

    # set FreeRouteTimeout
    # send FreeRoute
    def do_delete_position(self,_) -> bool:
        raise ValueError("do_delete_position not implemented")

    # set FreeRouteTimeout
    # send FreeRoute
    def do_automatic_delete_position(self,_) -> bool:
        raise ValueError("do_automatic_delete_position not implemented")

    # reset FreeRouteTimeout
    # reset meldestelle_from and meldestelle_to
    def do_positive_delete_position(self,_) -> bool:
        raise ValueError("do_positive_delete_position not implemented")

    # reset FreeRouteTimeout
    # Send System Fehler
    def do_negative_delete_position(self,_) -> bool:
        raise ValueError("do_negative_delete_position not implemented")

    # reset FreeRouteTimeout
    # Send System Fehler
    def do_delete_position_timeout(self,_) -> bool:
        raise ValueError("do_delete_position_timeout not implemented")

    # reset ArrivalTimeout
    # reset CallerId
    # set Meldestelle in DigitalBelegblatt
    def do_positiv_arrival_confirmation(self,_) -> bool:
        raise ValueError("do_positiv_arrival_confirmation not implemented")

    # reset ArrivalTimeout
    # reset CallerId
    # reset Meldestelle
    def do_negativ_arrival_confirmation(self,_) -> bool:
        raise ValueError("do_negativ_arrival_confirmation not implemented")

    # set FreeRouteTimeout
    # send FreeRoute
    def do_free_route(self,_) -> bool:
        raise ValueError("do_free_route not implemented")

    # reset FreeRouteTimeout
    # reset meldestelle_from and meldestelle_to
    def do_positive_free_route_response(self,_) -> bool:
        raise ValueError("do_positive_free_route_response not implemented")

    # reset FreeRouteTimeout
    # reset meldestelle_from and meldestelle_to
    # Send System Fehler
    def do_negative_free_route_response(self,_) -> bool:
        raise ValueError("do_negative_free_route_response not implemented")

    # reset FreeRouteTimeout
    # reset meldestelle_from and meldestelle_to
    # Send System Fehler
    def do_free_route_timeout(self,_) -> bool:
        raise ValueError("do_free_route_timeout not implemented")

    # send SetRouteRequest
    # set SetRoute Timeout
    # set meldestelle_from and meldestelle_to
    # set CallerId
    def do_set_route(self,_) -> bool:
        raise ValueError("do_set_route not implemented")





    def __init__(self):
        self.state = self.state_init
        # Transitions
        self.transitions = [
            Transition("Registration",self.event_registration_message , self.is_true , self.do_registration , self.state_init , self.state_in_movement),
            Transition("Arrival",self.event_arrival_message , self.is_valid_arrival_message , self.do_arrival , self.state_in_movement , self.state_arrival),
            Transition("DrivingPermission",self.event_set_route_response_message , self.is_positive_set_route_response , self.do_positive_set_route_response , self.state_driving_request , self.state_driving_permission),
            Transition("TryAgainLater",self.event_set_route_response_message , self.is_negative_set_route_response , self.do_negative_set_route_response , self.state_driving_request , self.state_confirmed_position),
            Transition("DrivingRequestTimeout",self.event_timer , self.is_set_route_timeout , self.do_clear_driving_request , self.state_driving_request , self.state_confirmed_position),
            Transition("CorrectDrivingPermissionReplay",self.event_driving_permission_replay_message , self.is_correct_driving_permission , self.do_correct_driving_permission_replay , self.state_driving_permission , self.state_in_movement),
            Transition("WrongDrivingPermissionReplay",self.event_driving_permission_replay_message , self.is_wrong_driving_permission , self.do_wrong_driving_permission_replay , self.state_driving_permission , self.state_free_route),
            Transition("DrivingPermissionTimeout",self.event_timer , self.is_driving_permission_timeout , self.do_driving_permission_timeout , self.state_driving_permission , self.state_free_route),
            Transition("SetPosition",self.event_timer , self.is_first_arrival , self.do_set_position , self.state_confirmed_position , self.state_set_position),
            Transition("PositiveSetPosition",self.event_set_route_response_message , self.is_positive_set_route_response , self.do_positive_set_position , self.state_set_position , self.state_confirmed_position),
            Transition("NegativeSetPosition",self.event_set_route_response_message , self.is_negative_set_route_response , self.do_negative_set_position , self.state_set_position , self.state_error),
            Transition("SetPositionTimeout",self.event_timer , self.is_set_route_timeout , self.do_set_position_timeout , self.state_set_position , self.state_error),
            Transition("ArrivalTimeout",self.event_timer , self.is_arrival_timeout , self.do_arrival_timeout , self.state_arrival , self.state_in_movement),
            Transition("DeletePosition",self.event_deregister , self.is_true , self.do_delete_position , self.state_confirmed_position , self.state_delete_position),
            Transition("AutomaticDeletePosition",self.event_timer , self.is_last_arrival , self.do_automatic_delete_position , self.state_confirmed_position , self.state_delete_position),
            Transition("PositiveDeletePosition",self.event_free_route_response_message , self.is_positive_free_route_response , self.do_positive_delete_position , self.state_delete_position , self.state_deregistered),
            Transition("NegativeDeletePosition",self.event_free_route_response_message , self.is_negative_free_route_response , self.do_negative_delete_position , self.state_delete_position , self.state_error),
            Transition("DeletePositionTimeout",self.event_timer , self.is_free_route_timeout , self.do_delete_position_timeout , self.state_delete_position , self.state_error),
            Transition("ArrivalConfirmed",self.event_positive_confirmation_message , self.is_true , self.do_positiv_arrival_confirmation , self.state_arrival , self.state_confirmed_position),
            Transition("ArrivalDeviated",self.event_negative_confirmation_message , self.is_true , self.do_negativ_arrival_confirmation , self.state_arrival , self.state_in_movement),
            Transition("FreeRoute",self.event_timer , self.is_valid_route , self.do_free_route , self.state_confirmed_position , self.state_free_route),
            Transition("PositiveFreeRouteResponse",self.event_free_route_response_message , self.is_positive_free_route_response , self.do_positive_free_route_response , self.state_free_route , self.state_confirmed_position),
            Transition("NegativeFreeRouteResponse",self.event_free_route_response_message , self.is_negative_free_route_response , self.do_negative_free_route_response , self.state_free_route , self.state_confirmed_position),
            Transition("FreeRouteTimeout",self.event_timer , self.is_free_route_timeout , self.do_free_route_timeout , self.state_free_route , self.state_confirmed_position),
            Transition("DrivingRequest",self.event_driving_request_message , self.is_valid_driving_request , self.do_set_route , self.state_confirmed_position , self.state_driving_request),
        ]

    def raise_event(self,event,event_data):
        for transition in self.transitions:

            if self.state != transition.source:
                continue
            if transition.event != event:
                continue
            if not transition.guard(event_data):
                continue
            transition.action(event_data)
            self.state = transition.target
            return True
        return False