from datetime import datetime, timedelta

from .train_supervision import TrainSupervision
from .Anmeldung import Anmeldung
from .Ankunftsmeldung import Ankunftsmeldung
from .FahrbefehlWiederholung import FahrbefehlWiederholung
from .Command import Command
from .Fahranfrage import Fahranfrage
from .FreeRouteResponse import FreeRouteResponse
from .SetRouteResponse import SetRouteResponse

class BelegblattMock:

    def __init__(self):
        self.betriebsstellen = ["AAA","BBB","CCC"]
        self.position = {}
    def get_zug_position(self,zugnummer):
        return self.position[zugnummer]
    def set_zug_position(self,zugnummer,position):
        self.position[zugnummer] = position
    def block_strecke_for_zugnummer(self,zugnummer,meldestelle):
        pass

class RouteManagerMock:

    def get_direction_for_start(self,meldestelle):
        assert meldestelle is "AAA"
        return "out"
    
    def get_routes_for_meldestelle(self,meldestelle,direction):
        assert meldestelle is "AAA"
        assert direction is "out"
        return [("A2","B2")]

    def get_set_routes_for_strecke(self,meldestelle_from,meldestelle_to,direction):
        assert meldestelle_from is "AAA"
        assert meldestelle_to is "BBB"
        assert direction is "out"
        return [("A2","B2")]
    
    def get_free_routes_for_strecke(self,meldestelle_from,meldestelle_to,direction):
        assert meldestelle_from is "AAA"
        assert meldestelle_to is "BBB"
        assert direction is "out"
        return [("A2","B2")]

class ProxyMock:

    def __init__(self):
        self.digitales_belegblatt = BelegblattMock()
        self.route_manager = RouteManagerMock()

        self.commands = []
        self.routes = []

    def set_command(self,cmd):
        self.commands.append(cmd)
    def set_route(self,cmd):
        self.routes.append(cmd)
    def free_route(self,cmd):
        self.routes.append(cmd)




def test_registration():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)

    assert state_machine.state == state_machine.state_init

    assert True is state_machine.raise_event(TrainSupervision.event_registration_message,Anmeldung(caller_id="xyz",zugnummer=99))

    assert state_machine.state == state_machine.state_in_movement
    assert proxy_mock.commands[0] == Command("xyz", "TestExec", "Zug 99 ja.")

def test_arrival():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)

    state_machine.state = state_machine.state_in_movement
    state_machine.meldestelle_from = "AAA"
    state_machine.meldestelle_to = "CCC"

    assert True is state_machine.raise_event(TrainSupervision.event_arrival_message,
                                             Ankunftsmeldung(caller_id="xyz",zugnummer=4711,meldestelle="CCC"))

    assert state_machine.state == state_machine.state_arrival
    assert proxy_mock.commands[0] == Command("xyz", "TestExec", "Ich wiederhole. Zug 4711 in CCC.")

def test_wrong_arrival():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)

    state_machine.state = state_machine.state_in_movement
    state_machine.meldestelle_from = "AAA"
    state_machine.meldestelle_to = "CCC"


    assert False is state_machine.raise_event(TrainSupervision.event_arrival_message,
                                             Ankunftsmeldung(caller_id="xyz",zugnummer=4711,meldestelle="BBB"))

    assert state_machine.state == state_machine.state_in_movement
    assert proxy_mock.commands[0] == Command("xyz", "TestExec", "Eine Ankunftsmeldung in BBB für Zug 4711 kann nicht verarbeitet werden. Zug fährt nach CCC.")

def test_arrival_timeout():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)

    state_machine.state = state_machine.state_arrival
    state_machine.arrival_timeout = datetime.now()

    assert True is state_machine.raise_event(TrainSupervision.event_timer,None)
    assert state_machine.state == state_machine.state_in_movement
    assert len(proxy_mock.commands) == 0

def test_arrival_no_timeout():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)

    state_machine.state = state_machine.state_arrival
    state_machine.arrival_timeout = datetime.now() + timedelta(seconds=60)

    assert False is state_machine.raise_event(TrainSupervision.event_timer,None)
    assert state_machine.state == state_machine.state_arrival
    assert len(proxy_mock.commands) == 0

def test_confirmation():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)

    state_machine.state = state_machine.state_arrival
    state_machine.ankunftsmeldung = Ankunftsmeldung(caller_id="xyz",zugnummer=4711,meldestelle="AAA")
    state_machine.first_arrival = False

    assert True is state_machine.raise_event(TrainSupervision.event_positive_confirmation_message,None)
    assert False is state_machine.raise_event(TrainSupervision.event_timer,None)

    assert state_machine.state == state_machine.state_confirmed_position
    assert len(proxy_mock.commands) == 0

def test_arrival_and_free_route():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)
    state_machine.meldestelle_from = "AAA"
    state_machine.meldestelle_to = "BBB"
    state_machine.direction = "out"
    state_machine.first_arrival = False

    state_machine.state = state_machine.state_arrival
    state_machine.ankunftsmeldung = Ankunftsmeldung(caller_id="xyz",zugnummer=4711,meldestelle="AAA")

    assert True is state_machine.raise_event(TrainSupervision.event_positive_confirmation_message,None)
    assert True is state_machine.raise_event(TrainSupervision.event_timer,None)

    assert state_machine.state == state_machine.state_free_route
    assert len(proxy_mock.commands) == 0
    assert len(proxy_mock.routes) == 1

def test_free_route_response():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)
    state_machine.meldestelle_from = "AAA"
    state_machine.meldestelle_to = "BBB"

    state_machine.state = state_machine.state_free_route

    assert True is state_machine.raise_event(TrainSupervision.event_free_route_response_message,FreeRouteResponse(zugnummer=7,status=0))

    assert state_machine.state == state_machine.state_confirmed_position
    assert not state_machine.meldestelle_from and not state_machine.meldestelle_to
    assert len(proxy_mock.commands) == 0
    assert len(proxy_mock.routes) == 0

def test_deviation():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)

    state_machine.state = state_machine.state_arrival

    assert True is state_machine.raise_event(TrainSupervision.event_negative_confirmation_message,None)

    assert state_machine.state == state_machine.state_in_movement
    assert len(proxy_mock.commands) == 0


def test_fahranfrage():


    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)
    state_machine.state = state_machine.state_confirmed_position
    state_machine.proxy.digitales_belegblatt.set_zug_position(4711,"AAA")
    state_machine.direction = "out"

    assert True is state_machine.raise_event(TrainSupervision.event_driving_request_message,Fahranfrage(caller_id="123",zugnummer=4711,meldestelle="BBB"))
    
    assert state_machine.state == state_machine.state_driving_request
    assert len(proxy_mock.commands) == 0
    assert len(proxy_mock.routes) == 1


def test_positive_set_route_response():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)
    state_machine.proxy.digitales_belegblatt.set_zug_position(4711,"AAA")
    state_machine.zugnummer = 4711
    state_machine.caller_id = "123"
    state_machine.meldestelle_from = "AAA"
    state_machine.meldestelle_to = "BBB"

    state_machine.state = state_machine.state_driving_request
    assert True is state_machine.raise_event(TrainSupervision.event_set_route_response_message,SetRouteResponse(status=0,zugnummer=4711))
        
    assert state_machine.state == state_machine.state_driving_permission
    assert proxy_mock.commands[0] == Command("123", "TestExec", "Zug 4711 darf bis BBB fahren.")

def test_negative_set_route_response():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)
    state_machine.state = state_machine.state_driving_request
    state_machine.caller_id = "123"
    state_machine.zugnummer = 4711

    assert True is state_machine.raise_event(TrainSupervision.event_set_route_response_message,SetRouteResponse(status=-1,zugnummer=4711))
        
    assert state_machine.state == state_machine.state_confirmed_position
    assert proxy_mock.commands[0] == Command("123", "TestExec", "Nein. Warten.")

def test_set_route_response_timeout():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)

    state_machine.state = state_machine.state_driving_request
    state_machine.set_route_timeout = datetime.now()
    state_machine.caller_id = "123"
    state_machine.zugnummer = 4711
    
    assert True is state_machine.raise_event(TrainSupervision.event_timer,None)
    assert state_machine.state == state_machine.state_confirmed_position
    assert proxy_mock.commands[0] == Command("123", "TestExec", "Nein. Warten.")


def test_driving_permission_replay():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)

    state_machine.state = state_machine.state_driving_permission
    state_machine.zugnummer = 1000
    state_machine.meldestelle_to = "BBB"

    state_machine.raise_event(TrainSupervision.event_driving_permission_replay_message,FahrbefehlWiederholung("123",1000,"BBB"))
    
    assert state_machine.state == state_machine.state_in_movement
    assert proxy_mock.commands[0] == Command("123", "TestExec", "Ja. Richtig.")

def test_driving_permission_timeout():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)

    state_machine.state = state_machine.state_driving_permission
    state_machine.driving_permission_timeout = datetime.now()
    state_machine.meldestelle_from = "AAA"
    state_machine.meldestelle_to = "BBB"
    state_machine.direction = "out"

    assert True is state_machine.raise_event(TrainSupervision.event_timer,None)
    assert state_machine.state == state_machine.state_free_route
    assert len(proxy_mock.commands) == 0


def test_lock_position():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)
    state_machine.state = state_machine.state_confirmed_position
    state_machine.meldestelle = "AAA"
    state_machine.direction = "out"
    
    assert True is state_machine.raise_event(TrainSupervision.event_timer,None)


    assert state_machine.state == state_machine.state_set_position
    assert len(proxy_mock.commands) == 0
    assert len(proxy_mock.routes) == 1

def test_deregister():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)
    state_machine.state = state_machine.state_confirmed_position
    state_machine.meldestelle = "AAA"
    state_machine.direction = "out"

    assert True is state_machine.raise_event(TrainSupervision.event_deregister,None)
    
    assert state_machine.state == state_machine.state_delete_position
    assert len(proxy_mock.commands) == 0
    assert len(proxy_mock.routes) == 1

def test_deregister_after_response():

    proxy_mock = ProxyMock()
    state_machine = TrainSupervision(proxy_mock)
    state_machine.state = state_machine.state_delete_position
    state_machine.zugnummer = 7

    assert True is state_machine.raise_event(TrainSupervision.event_free_route_response_message,FreeRouteResponse(zugnummer=7,status=0))
    
    assert state_machine.state == state_machine.state_deregistered
    assert len(proxy_mock.commands) == 0
    assert len(proxy_mock.routes) == 0
