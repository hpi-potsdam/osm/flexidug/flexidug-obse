# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.

from dataclasses import dataclass
from typing import List

from .Route import Route

@dataclass
class SetRoute():
   zugnummer : int
   routes : List[Route]
