
from .route_manager import RouteManager


datamodel = {
    "out": [
        {
            "from_signal" : "ES1",
            "to_signal" : "AS2",
            "station" : "Jöhstadt"
        },
        {
            "from_signal" : "AS2",
            "to_signal" : "ES3",
            "station" : "" # outside of station -> freie Strecke
        },
        {
            "from_signal" : "ES3",
            "to_signal" : "AS4",
            "station" : "Fahrzeughalle"
        },
        {
            "from_signal" : "AS4",
            "to_signal" : "ES5",
            "station" : "" # outside of station -> freie Strecke
        },
        {
            "from_signal" : "ES4",
            "to_signal" : "AS6",
            "station" : "Schlössel"
        }
    ],
    "back" : [
        {
            "from_signal" : "ES7",
            "to_signal" : "AS8",
            "station" : "Schlössel"
        },
        {
            "from_signal" : "AS8",
            "to_signal" : "ES9",
            "station" : "" # outside of station -> freie Strecke
        },
        {
            "from_signal" : "ES9",
            "to_signal" : "AS10",
            "station" : "Fahrzeughalle"
        },
        {
            "from_signal" : "AS10",
            "to_signal" : "ES11",
            "station" : "" # outside of station -> freie Strecke
        },
        {
            "from_signal" : "ES11",
            "to_signal" : "AS12",
            "station" : "Jöhstadt"
        }
    ]
}





def test_start_direction_out():
    route_manager = RouteManager(datamodel)
    assert "out" == route_manager.get_direction_for_start("Jöhstadt")

def test_start_direction_back():
    route_manager = RouteManager(datamodel)
    assert "back" == route_manager.get_direction_for_start("Schlössel")

def test_first_position_out():
    route_manager = RouteManager(datamodel)

    routes = route_manager.get_routes_for_meldestelle("Jöhstadt",direction = "out") 
    assert routes == [("ES1","AS2")]


def test_first_position_back():
    route_manager = RouteManager(datamodel)

    routes = route_manager.get_routes_for_meldestelle("Jöhstadt",direction = "back")
    assert routes == [("ES11","AS12")]


def test_get_set_route():
    route_manager = RouteManager(datamodel)
    routes = route_manager.get_set_routes_for_strecke("Jöhstadt","Fahrzeughalle",direction = "out")
    assert routes == [("AS2","ES3"),("ES3","AS4")]

def test_get_free_route():
    route_manager = RouteManager(datamodel)
    routes = route_manager.get_free_routes_for_strecke("Jöhstadt","Fahrzeughalle",direction = "out")
    assert routes == [("ES1","AS2"),("AS2","ES3")]


def test_get_set_route_long():
    route_manager = RouteManager(datamodel)
    routes = route_manager.get_set_routes_for_strecke("Schlössel","Jöhstadt",direction = "back")
    assert routes == [("AS8","ES9"),("ES9","AS10"),("AS10","ES11"),("ES11","AS12")]


def test_get_last_meldestelle():
    route_manager = RouteManager(datamodel)
    assert route_manager.get_last_meldestelle("out") == "Schlössel"
    assert route_manager.get_last_meldestelle("back") == "Jöhstadt"

