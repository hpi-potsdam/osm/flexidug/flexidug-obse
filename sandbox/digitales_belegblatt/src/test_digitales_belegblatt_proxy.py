from datetime import datetime

from .DigitalesBelegblattProxy import DigitalesBelegblattProxy
from .Command import Command
from .Ankunftsmeldung import Ankunftsmeldung
from .Anmeldung import Anmeldung
from .Fahranfrage import Fahranfrage
from .FahrbefehlWiederholung import FahrbefehlWiederholung
from .PositiveBestaetigung import PositiveBestaetigung
from .NegativeBestaetigung import NegativeBestaetigung
from .SetRoute import SetRoute
from .Route import Route
from .SetRouteResponse import SetRouteResponse
from .FreeRoute import FreeRoute
from .FreeRouteResponse import FreeRouteResponse


class TimerMock:
    pass

class DeregisterMock:

    def __init__(self,zugnummer):
        self.zugnummer = zugnummer

class ArrivalTimeoutMock:

    def __init__(self,zugnummer,timeout):
        self.zugnummer = zugnummer
        self.timeout = timeout

class ProxyWrapper:

    def recv(self,msg):
        self.messages.append(msg)

    def __init__(self):
        self.proxy = DigitalesBelegblattProxy(with_server=False)
        self.proxy.set_Command_callback(self.recv)
        self.proxy.set_SetRoute_callback(self.recv)
        self.proxy.set_FreeRoute_callback(self.recv)

    def send(self,msg):

        self.messages = []
        if isinstance(msg, Ankunftsmeldung):
            self.proxy.apply_Ankunftsmeldung(msg)
        elif isinstance(msg, Anmeldung):
            self.proxy.apply_Anmeldung(msg)
        elif isinstance(msg, PositiveBestaetigung):
            self.proxy.apply_PositiveBestaetigung(msg)
        elif isinstance(msg, NegativeBestaetigung):
            self.proxy.apply_NegativeBestaetigung(msg)
        elif isinstance(msg, Fahranfrage):
            self.proxy.apply_Fahranfrage(msg)
        elif isinstance(msg, FahrbefehlWiederholung):
            self.proxy.apply_FahrbefehlWiederholung(msg)
        elif isinstance(msg, SetRouteResponse):
            self.proxy.apply_SetRouteResponse(msg)
        elif isinstance(msg, FreeRouteResponse):
            self.proxy.apply_FreeRouteResponse(msg)
        elif isinstance(msg, TimerMock):
            self.proxy.tick(1)
        elif isinstance(msg, ArrivalTimeoutMock):
            assert self.proxy.state_machines[msg.zugnummer].arrival_timeout > datetime.now()
            self.proxy.state_machines[msg.zugnummer].arrival_timeout = msg.timeout
        elif isinstance(msg,DeregisterMock):
            self.proxy.deregister(msg.zugnummer)
        else:
            raise ValueError(f"Unknown Message {msg}")


        if len(self.messages) == 0:
            return None

        if len(self.messages) == 1:
            return self.messages[-1]
        
        return self.messages
    
    def check(self,protocol):
        for msg, cmd in protocol:
            print(cmd,"=>",msg)
            assert cmd == self.send(msg)

            # Validate State machines
            for zugnummer , state_machine in self.proxy.state_machines.items():
                assert zugnummer == state_machine.zugnummer

                #if state_machine.state is TrainSupervisionStateMachine.state_in_movement:
                #    assert state_machine.meldestelle is None
                #    assert state_machine.meldestelle_from is not None
                #    assert state_machine.meldestelle_to is not None
                #    assert state_machine.caller_id is None

def generate_anmeldung(caller_id,zugnummer):
    return [
        (Anmeldung(caller_id,zugnummer),
         Command(caller_id, command_type="TestExec", text=f"Zug {zugnummer} ja.")),
    ]

def generate_ankunft(caller_id,zugnummer,meldestelle,routes):
    return [
        (Ankunftsmeldung(caller_id,zugnummer,meldestelle),
         Command(caller_id, command_type="TestExec", text=f"Ich wiederhole. Zug {zugnummer} in {meldestelle}.")),

        (PositiveBestaetigung(caller_id),
         None),
        
        (TimerMock(),
         SetRoute(zugnummer,routes)),

        (SetRouteResponse(zugnummer,status=0),
         None),

        (TimerMock(),
         None)
    ]

def generate_ankunft_nach_fahrt(caller_id,zugnummer,meldestelle,routes):
    return [
        (Ankunftsmeldung(caller_id,zugnummer,meldestelle),
         Command(caller_id, command_type="TestExec", text=f"Ich wiederhole. Zug {zugnummer} in {meldestelle}.")),

        (PositiveBestaetigung(caller_id),
         None),

        (TimerMock(),
         FreeRoute(zugnummer,routes)),

        (FreeRouteResponse(zugnummer,status=0),
         None),

    ]

def generate_fahranfrage(caller_id,zugnummer,meldestelle,routes):
    return [
        (Fahranfrage(caller_id,zugnummer,meldestelle),
         SetRoute(zugnummer,routes)),

        (SetRouteResponse(zugnummer,status=0),
         Command(caller_id, command_type="TestExec", text=f"Zug {zugnummer} darf bis {meldestelle} fahren.")),

        (FahrbefehlWiederholung(caller_id,zugnummer,meldestelle),
         Command(caller_id, command_type="TestExec", text="Ja. Richtig.")),
    ]

def generate_fahranfrage_falsch(caller_id,zugnummer,meldestelle,meldestelle_falsch,routes):
    return [
        (Fahranfrage(caller_id,zugnummer,meldestelle),
         SetRoute(zugnummer,routes)),

        (SetRouteResponse(zugnummer,status=0),
         Command(caller_id, command_type="TestExec", text=f"Zug {zugnummer} darf bis {meldestelle} fahren.")),

        (FahrbefehlWiederholung(caller_id,zugnummer,meldestelle_falsch),
         [Command(caller_id, command_type="TestExec", text=f"Nein. Falsch. Für die Zugnummer {zugnummer} wurde kein Fahrbefehl nach {meldestelle_falsch} erteilt. Warten."),
          FreeRoute(zugnummer,routes)]),

        (TimerMock(),
         None),

        (FreeRouteResponse(zugnummer,status=0),
         None),
    ]


def generate_fahranfrage_abgelehnt(caller_id,zugnummer,meldestelle,routes):
    return [
        (Fahranfrage(caller_id,zugnummer,meldestelle),
         SetRoute(zugnummer,routes)),

        (SetRouteResponse(zugnummer,status=-1),
         Command(caller_id, command_type="TestExec", text="Nein. Warten.")),

        (TimerMock(),
         None),
    ]

def generate_deregister(zugnummer,routes):
    return [
        (DeregisterMock(zugnummer),
         FreeRoute(zugnummer,routes)),

        (SetRouteResponse(zugnummer,status=0),
         None),

    ]


def test_very_simple_protocol():

    proxy = ProxyWrapper()

    protocol = []

    
    protocol.extend(generate_anmeldung(caller_id="phone1",zugnummer=2))
    protocol.extend(generate_ankunft(caller_id="phone1",zugnummer=2,meldestelle="Jöhstadt",routes=[Route("60ES1","60AS2")]))
    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60AS2","60ES3"),Route("60ES3","60AS4")]))
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60ES1","60AS2"),Route("60AS2","60ES3")]))


    proxy.check(protocol)

def test_two_steps_protocol():

    proxy = ProxyWrapper()

    protocol = []

    
    protocol.extend(generate_anmeldung(caller_id="phone1",zugnummer=2))
    protocol.extend(generate_ankunft(caller_id="phone1",zugnummer=2,meldestelle="Jöhstadt",routes=[Route("60ES1","60AS2")]))
    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60AS2","60ES3"),Route("60ES3","60AS4")]))
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60ES1","60AS2"),Route("60AS2","60ES3")]))

    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=2,meldestelle="Schlössel",routes=[Route("60AS4","60ES5"),Route("60ES5","60AS6")]))
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=2,meldestelle="Schlössel",routes=[Route("60ES3","60AS4"),Route("60AS4","60ES5")]))

    proxy.check(protocol)


def test_intermediate_message_protocol():

    proxy = ProxyWrapper()

    protocol = []

    
    protocol.extend(generate_anmeldung(caller_id="phone1",zugnummer=2))
    protocol.extend(generate_ankunft(caller_id="phone1",zugnummer=2,meldestelle="Jöhstadt",routes=[Route("60ES1","60AS2")]))
    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=2,meldestelle="Schlössel",
                                         routes=[Route("60AS2","60ES3"),Route("60ES3","60AS4"),Route("60AS4","60ES5"),Route("60ES5","60AS6")]))
    
    protocol.extend([
        (Ankunftsmeldung(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle"),
         Command(caller_id="phone1", command_type="TestExec", text="Eine Ankunftsmeldung in Fahrzeughalle für Zug 2 kann nicht verarbeitet werden. Zug fährt nach Schlössel.")),
    ])

    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=2,meldestelle="Schlössel",
                                                routes=[Route("60ES1","60AS2"),Route("60AS2","60ES3"),Route("60ES3","60AS4"),Route("60AS4","60ES5")]))

    proxy.check(protocol)


def test_out_and_back_protocol():

    proxy = ProxyWrapper()

    protocol = []

    
    protocol.extend(generate_anmeldung(caller_id="phone1",zugnummer=2))
    protocol.extend(generate_ankunft(caller_id="phone1",zugnummer=2,meldestelle="Jöhstadt",routes=[Route("60ES1","60AS2")]))
    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60AS2","60ES3"),Route("60ES3","60AS4")]))
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60ES1","60AS2"),Route("60AS2","60ES3")]))

    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=2,meldestelle="Schlössel",routes=[Route("60AS4","60ES5"),Route("60ES5","60AS6")]))
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=2,meldestelle="Schlössel",routes=[Route("60ES3","60AS4"),Route("60AS4","60ES5")]))

    #TODO reset Zug 1
    protocol.extend(generate_deregister(zugnummer=2,routes=[Route("60ES5","60AS6")]))

    protocol.extend(generate_anmeldung(caller_id="phone1",zugnummer=3))
    protocol.extend(generate_ankunft(caller_id="phone1",zugnummer=3,meldestelle="Schlössel",routes=[Route("60ES7","60AS8")]))
    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=3,meldestelle="Fahrzeughalle",routes=[Route("60AS8","60ES9"),Route("60ES9","60AS10")]))
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=3,meldestelle="Fahrzeughalle",routes=[Route("60ES7","60AS8"),Route("60AS8","60ES9")]))

    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=3,meldestelle="Jöhstadt",routes=[Route("60AS10","60ES11"),Route("60ES11","60AS12")]))
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=3,meldestelle="Jöhstadt",routes=[Route("60ES9","60AS10"),Route("60AS10","60ES11")]))




    proxy.check(protocol)

def test_simple_protocol():

    proxy = ProxyWrapper()

    protocol = []

    
    protocol.extend(generate_anmeldung(caller_id="phone1",zugnummer=2))
    protocol.extend(generate_anmeldung(caller_id="phone2",zugnummer=7))
    protocol.extend(generate_ankunft(caller_id="phone1",zugnummer=2,meldestelle="Jöhstadt",routes=[Route("60ES1","60AS2")]))
    protocol.extend(generate_ankunft(caller_id="phone2",zugnummer=7,meldestelle="Schlössel",routes=[Route("60ES7","60AS8")]))

    protocol.extend(generate_fahranfrage_abgelehnt(caller_id="phone2",zugnummer=7,meldestelle="Fahrzeughalle",routes=[Route("60AS8","60ES9"),Route("60ES9","60AS10")]))
    
    protocol.extend(generate_fahranfrage_falsch(caller_id="phone2",zugnummer=7,meldestelle="Fahrzeughalle",meldestelle_falsch="Schlössel",routes=[Route("60AS8","60ES9"),Route("60ES9","60AS10")]))



    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60AS2","60ES3"),Route("60ES3","60AS4")]))
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60ES1","60AS2"),Route("60AS2","60ES3")]))

    # not possible, here we habe dead lock
    #protocol.extend(generate_fahranfrage(caller_id="phone2",zugnummer=7,meldestelle="Boxberg",routes=[Route("B1","A1")]))
    #protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone2",zugnummer=7,meldestelle="Boxberg",routes=[Route("B1","A1")]))

    proxy.check(protocol)


def test_arrival_timeout_protocol():

    proxy = ProxyWrapper()

    protocol = []

    protocol.extend(generate_anmeldung(caller_id="phone1",zugnummer=2))

    #Ankunftsmeldung mit Timeout
    protocol.append(
        (Ankunftsmeldung(caller_id="phone1",zugnummer=2,meldestelle="Jöhstadt"),
         Command("phone1", command_type="TestExec", text="Ich wiederhole. Zug 2 in Jöhstadt."))
    )

    protocol.append(
        (ArrivalTimeoutMock(zugnummer=2,timeout=datetime.now()),
         None)
    )
    protocol.append(
        (TimerMock(),
         None)
    )
    protocol.extend(generate_ankunft(caller_id="phone1",zugnummer=2,meldestelle="Jöhstadt",routes=[Route("60ES1","60AS2")]))

    proxy.check(protocol)

def test_error_protocol():

    proxy = ProxyWrapper()

    protocol = []

    protocol.append(
        (Ankunftsmeldung(caller_id="1",zugnummer=1,meldestelle="AAA"),
         Command(caller_id="1", command_type="TestExec", text="Fehler. Zug 1 muss erst angemeldet werden."))
    )

    protocol.extend(generate_anmeldung(caller_id="phone1",zugnummer=2))
   
    #Kann nicht auftreten, da BS schon bei NLU bekannt sein muss. Dennoch testen.
    protocol.append(
        (Ankunftsmeldung(caller_id="phone1",zugnummer=2,meldestelle="AAA"),
         None)
    )

    # Fahrauftrag ohne Position
    protocol.append(
         (Fahranfrage(caller_id="phone1",zugnummer=2,meldestelle="Schwarze Pumpe"),
         Command(caller_id="phone1", command_type="TestExec", text="Fehler. Eine valide Zugposition für Zug 2 ist nicht bekannt."))
    )

    protocol.extend(generate_ankunft(caller_id="phone1",zugnummer=2,meldestelle="Jöhstadt",routes=[Route("60ES1","60AS2")]))

    # Neue Position obwohl schon an Standort stehend
    protocol.append(
        (Ankunftsmeldung(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle"),
         Command(caller_id="phone1", command_type="TestExec", text="Fehler. Zug 2 ist bereits in Jöhstadt gemeldet.")),
    )

    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60AS2","60ES3"),Route("60ES3","60AS4")]))

    # Fahrauftrag während Fahrt Position
    protocol.append(
         (Fahranfrage(caller_id="phone1",zugnummer=2,meldestelle="Schlössel"),
         Command(caller_id="phone1", command_type="TestExec", text="Fehler. Eine valide Zugposition für Zug 2 ist nicht bekannt."))
    )


    # Meldung der Position mit negativer Bestätigung
    protocol.append(
        (Ankunftsmeldung(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle"),
         Command(caller_id="phone1", command_type="TestExec", text="Ich wiederhole. Zug 2 in Fahrzeughalle.")),
    )
    protocol.append(
        (NegativeBestaetigung(caller_id="phone1"),
         None),
    )

    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60ES1","60AS2"),Route("60AS2","60ES3")]))

    proxy.check(protocol)



#change caller id, during test caller changes device
def test_change_caller_id_protocol():

    proxy = ProxyWrapper()
    protocol = []

    protocol.extend(generate_anmeldung(caller_id="phone1",zugnummer=2))
    protocol.extend(generate_anmeldung(caller_id="phone2",zugnummer=7))



    protocol.extend(generate_ankunft(caller_id="phone3",zugnummer=2,meldestelle="Jöhstadt",routes=[Route("60ES1","60AS2")]))
    protocol.extend(generate_ankunft(caller_id="phone4",zugnummer=7,meldestelle="Schlössel",routes=[Route("60ES7","60AS8")]))


    protocol.extend(generate_fahranfrage_abgelehnt(caller_id="phone5",zugnummer=7,meldestelle="Fahrzeughalle",routes=[Route("60AS8","60ES9"),Route("60ES9","60AS10")]))
    protocol.extend(generate_fahranfrage(caller_id="phone6",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60AS2","60ES3"),Route("60ES3","60AS4")]))

    
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone7",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60ES1","60AS2"),Route("60AS2","60ES3")]))

    protocol.extend(generate_deregister(zugnummer=2,routes=[Route("60ES3","60AS4")]))

    protocol.extend(generate_fahranfrage(caller_id="phone8",zugnummer=7,meldestelle="Fahrzeughalle",routes=[Route("60AS8","60ES9"),Route("60ES9","60AS10")]))
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone9",zugnummer=7,meldestelle="Fahrzeughalle",routes=[Route("60ES7","60AS8"),Route("60AS8","60ES9")]))

  
    proxy.check(protocol)
   



#same caller id (during Demonstration)
def test_same_caller_id_protocol():

    proxy = ProxyWrapper()
    protocol = []

    protocol.extend(generate_anmeldung(caller_id="phone1",zugnummer=2))
    protocol.extend(generate_anmeldung(caller_id="phone1",zugnummer=7))



    protocol.extend(generate_ankunft(caller_id="phone1",zugnummer=2,meldestelle="Jöhstadt",routes=[Route("60ES1","60AS2")]))
    protocol.extend(generate_ankunft(caller_id="phone1",zugnummer=7,meldestelle="Schlössel",routes=[Route("60ES7","60AS8")]))


    protocol.extend(generate_fahranfrage_abgelehnt(caller_id="phone1",zugnummer=7,meldestelle="Fahrzeughalle",routes=[Route("60AS8","60ES9"),Route("60ES9","60AS10")]))
    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60AS2","60ES3"),Route("60ES3","60AS4")]))

    
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=2,meldestelle="Fahrzeughalle",routes=[Route("60ES1","60AS2"),Route("60AS2","60ES3")]))

    protocol.extend(generate_deregister(zugnummer=2,routes=[Route("60ES3","60AS4")]))

    protocol.extend(generate_fahranfrage(caller_id="phone1",zugnummer=7,meldestelle="Fahrzeughalle",routes=[Route("60AS8","60ES9"),Route("60ES9","60AS10")]))
    protocol.extend(generate_ankunft_nach_fahrt(caller_id="phone1",zugnummer=7,meldestelle="Fahrzeughalle",routes=[Route("60ES7","60AS8"),Route("60AS8","60ES9")]))

  
    proxy.check(protocol)
   
