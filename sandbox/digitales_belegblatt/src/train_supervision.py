from datetime import datetime, timedelta
import logging

from .TrainSupervisionStateMachine import TrainSupervisionStateMachine
from .Anmeldung import Anmeldung
from .Ankunftsmeldung import Ankunftsmeldung
from .Fahranfrage import Fahranfrage
from .FahrbefehlWiederholung import FahrbefehlWiederholung

from .Command import Command
from .SetRoute import SetRoute
from .SetRouteResponse import SetRouteResponse
from .FreeRoute import FreeRoute
from .FreeRouteResponse import FreeRouteResponse
from .Route import Route


class TrainSupervision(TrainSupervisionStateMachine):

    ARRIVAL_TIMEOUT = timedelta(seconds = 60)
    FREE_ROUTE_TIMEOUT = timedelta(seconds = 60)
    SET_ROUTE_TIMEOUT = timedelta(seconds = 60)
    DRIVING_PERMISSION_TIMEOUT = timedelta(seconds = 60)

    def __init__(self,proxy):
        self.proxy = proxy

        # Status
        self.zugnummer = None
        self.caller_id = None

        self.first_arrival = True

        ## Richtung
        self.direction = None

        ## Standort
        self.meldestelle = None

        ## Route gesetzt
        self.meldestelle_from = None
        self.meldestelle_to = None

        # Timeouts
        self.arrival_timeout = None
        self.free_route_timeout = None
        self.set_route_timeout = None
        self.driving_permission_timeout = None

        super().__init__()


    #Guards
    def is_valid_arrival_message(self,ankunftsmeldung : Ankunftsmeldung) -> bool:

        if ankunftsmeldung.meldestelle not in self.proxy.digitales_belegblatt.betriebsstellen:
            logging.error(f"Betriebsstelle {ankunftsmeldung.meldestelle} is not valid.")
            return False
        

        if self.meldestelle_to and ankunftsmeldung.meldestelle != self.meldestelle_to:
            logging.error(f"Wrong Meldestelle {ankunftsmeldung.meldestelle} , Train is driving to {self.meldestelle_to}.")

            #Mock, move to FSM
            # ERROR:root:Wrong Meldestelle Schlössel , Train is driving to Schlössel.
            self.proxy.set_command(Command(caller_id = ankunftsmeldung.caller_id,
                                           command_type="AnkunftsmeldungFalschesZiel",
                                           text = f"Eine Ankunftsmeldung in {ankunftsmeldung.meldestelle} für Zug {ankunftsmeldung.zugnummer} kann nicht verarbeitet werden. Zug fährt nach {self.meldestelle_to}."))

            return False

        return True

    def is_arrival_timeout(self,_) -> bool:
        return self.arrival_timeout and (self.arrival_timeout <= datetime.now())

    def is_correct_driving_permission(self,fahrbefehl_wiederholung : FahrbefehlWiederholung) -> bool:

        if fahrbefehl_wiederholung.zugnummer != self.zugnummer:
            logging.error(f"Train {fahrbefehl_wiederholung.zugnummer} is not same as {self.zugnummer}.")
            return False

        if fahrbefehl_wiederholung.meldestelle != self.meldestelle_to:
            return False
        
        return True

    def is_wrong_driving_permission(self,fahrbefehl_wiederholung : FahrbefehlWiederholung) -> bool:
        return not self.is_correct_driving_permission(fahrbefehl_wiederholung)
    
    def is_driving_permission_timeout(self,_) -> bool:
        return self.driving_permission_timeout and (self.driving_permission_timeout <= datetime.now())

    def is_valid_route(self,_) -> bool:
        return self.meldestelle_from and self.meldestelle_to

    def is_positive_free_route_response(self,free_route_response : FreeRouteResponse) -> bool:
        if free_route_response.zugnummer != self.zugnummer:
            logging.error(f"Train {free_route_response.zugnummer} is not same as {self.zugnummer}.")
            return False
        return free_route_response.status == 0

    def is_negative_free_route_response(self,free_route_response : FreeRouteResponse) -> bool:
        return not self.is_positive_free_route_response(free_route_response)

    def is_free_route_timeout(self,_) -> bool:
        return self.free_route_timeout and (self.free_route_timeout <= datetime.now())

    def is_valid_driving_request(self,fahranfrage : Fahranfrage) -> bool:
        meldestelle_from = self.proxy.digitales_belegblatt.get_zug_position(fahranfrage.zugnummer)
        meldestelle_to = fahranfrage.meldestelle
        logging.info(f"Recv Fahranfrage zugnummer: {fahranfrage.zugnummer} / {meldestelle_from} => {meldestelle_to}")

        if meldestelle_from is None:
            logging.error(f"Train {fahranfrage.zugnummer} has no Betriebsstelle.")
            return False

        digitales_belegblatt_routes = self.proxy.route_manager.get_set_routes_for_strecke(meldestelle_from,meldestelle_to,self.direction)
        logging.info(f"Resolved routes {digitales_belegblatt_routes}")
        if not digitales_belegblatt_routes or len(digitales_belegblatt_routes) == 0:
            #TODO Action in Guard nach Action verschieben (State Machine erweitern)
            self.proxy.set_command(Command(caller_id = fahranfrage.caller_id,
                                           command_type="FahranfrageRouteNichtGefunden",
                                           text = f"Fahranfrage kann nicht beantwortet werden, da keine Route von {meldestelle_from} nach {meldestelle_to} gefunden wurde."))
            return False
        return True

    def is_positive_set_route_response(self,set_route_response : SetRouteResponse) -> bool:
        if set_route_response.zugnummer != self.zugnummer:
            logging.error(f"Train {set_route_response.zugnummer} is not same as {self.zugnummer}.")
            return False
        return set_route_response.status == 0
    
    def is_negative_set_route_response(self,set_route_response : SetRouteResponse) -> bool:
        return not self.is_positive_set_route_response(set_route_response)

    def is_set_route_timeout(self,_) -> bool:  
        return self.set_route_timeout and (self.set_route_timeout <= datetime.now())

    def is_first_arrival(self,_) -> bool:
        return self.first_arrival

    def is_last_arrival(self, _) -> bool:

        # Option nicht aktiviert
        if not self.proxy.automatic_train_deregistration:
            return False

        # Muss Route erst noch freigegeben werden
        if self.is_valid_route(None):
            return False

        # Ist es die letzte Meldestelle
        if self.proxy.route_manager.get_last_meldestelle(self.direction) == self.meldestelle:
            return True

        return False

	# Actions
    def do_nop(self,_): #no operation
        pass

        # Send RegistrationConfirmation message
        # Set train number
    def do_registration(self,anmeldung: Anmeldung) -> bool:
        self.zugnummer = anmeldung.zugnummer
        self.proxy.set_command(Command(anmeldung.caller_id,
                                       command_type="AnmeldungErfolgreich",
                                       text=f"Zug {anmeldung.zugnummer} ja."))

        # Send ArrivalReplay message
        # set Meldestelle
        # set ArrivalTimeout
        # set CallerId
    def do_arrival(self,ankunftsmeldung : Ankunftsmeldung) -> bool:
        self.caller_id = ankunftsmeldung.caller_id
        self.meldestelle = ankunftsmeldung.meldestelle

        self.arrival_timeout = datetime.now() + self.ARRIVAL_TIMEOUT
        self.proxy.set_command(Command(caller_id = ankunftsmeldung.caller_id,
                                       command_type="AnkunftsmeldungWiederholung",
                                       text = f"Ich wiederhole. Zug {ankunftsmeldung.zugnummer} in {ankunftsmeldung.meldestelle}."))

        # reset SetRouteTimeout
        # send DrivingPermission
        # set DrivingPermissionTimeout
    def do_positive_set_route_response(self,_) -> bool:
        self.proxy.digitales_belegblatt.block_strecke_for_zugnummer(self.zugnummer,self.meldestelle_to)
        self.proxy.set_command(Command(caller_id = self.caller_id,
                                       command_type="FahranfragePositiveAntwort",
                                       text = f"Zug {self.zugnummer} darf bis {self.meldestelle_to} fahren."))
        self.set_route_timeout = None
        self.driving_permission_timeout = datetime.now() + self.DRIVING_PERMISSION_TIMEOUT

        # reset SetRouteTimeout
        # send WaitingNotification
        # reset CallerId
    def do_negative_set_route_response(self,_) -> bool:
        self.proxy.set_command(Command(caller_id = self.caller_id,
                                       command_type="FahranfrageNegativeAntwort",
                                       text = "Nein. Warten."))
        self.caller_id = None
        self.meldestelle_from = None
        self.meldestelle_to = None
        self.set_route_timeout = None

 
        # reset SetRouteTimeout
        # reset meldestelle_from and meldestelle_to
        # reset CallerId
    def do_clear_driving_request(self,_) -> bool:
        self.proxy.set_command(Command(caller_id = self.caller_id,
                                       command_type="FahranfrageNegativeAntwort",
                                       text = "Nein. Warten."))
        self.caller_id = None
        self.meldestelle_from = None
        self.meldestelle_to = None
        self.set_route_timeout = None


        # reset DrivingPermissionTimeout
        # Send PositivConfirmation
        # reset CallerId
    def do_correct_driving_permission_replay(self,fahrbefehl_wiederholung : FahrbefehlWiederholung) -> bool:
        self.proxy.set_command(Command(caller_id = fahrbefehl_wiederholung.caller_id,
                                       command_type="FahranfrageRichtigeBestaetigung",
                                       text = "Ja. Richtig."))
        self.caller_id = None
        self.driving_permission_timeout = None


        # reset DrivingPermissionTimeout
        # Send NegativConfirmation
        # reset CallerId
    def do_wrong_driving_permission_replay(self,fahrbefehl_wiederholung : FahrbefehlWiederholung) -> bool:
        self.proxy.set_command(Command(caller_id = fahrbefehl_wiederholung.caller_id,
                                       command_type="FahranfrageFalscheBestaetigung",
                                       text = f"Nein. Falsch. Für die Zugnummer {fahrbefehl_wiederholung.zugnummer} wurde kein Fahrbefehl nach {fahrbefehl_wiederholung.meldestelle} erteilt. Warten."))
        self.driving_permission_timeout = None
        self.caller_id = None

        #Reset routes (use set_routes, train not moved)
        digitales_belegblatt_routes = self.proxy.route_manager.get_set_routes_for_strecke(self.meldestelle_from,self.meldestelle_to,self.direction)
        logging.info(f"Reset routes {digitales_belegblatt_routes}")
        if not digitales_belegblatt_routes or len(digitales_belegblatt_routes) == 0:
            logging.error(f"Fehler. Keine RouteSection for {self.meldestelle_from} => {self.meldestelle_from} gefunden.")
            return
        self._send_free_routes_and_set_timer(digitales_belegblatt_routes)

        # revert_strecke_for_zugnummer
        self.proxy.digitales_belegblatt.revert_strecke_for_zugnummer(self.zugnummer, self.meldestelle_to)
        # Protokollierung ans Zugmeldebuch
        # TODO

    # reset DrivingPermissionTimeout
    # reset CallerId
    def do_driving_permission_timeout(self,_) -> bool:
        self.caller_id = None
        self.driving_permission_timeout = None

        #Reset routes (use set_routes, train not moved)
        digitales_belegblatt_routes = self.proxy.route_manager.get_set_routes_for_strecke(self.meldestelle_from,self.meldestelle_to,self.direction)
        logging.info(f"Reset routes {digitales_belegblatt_routes}")
        if not digitales_belegblatt_routes or len(digitales_belegblatt_routes) == 0:
            logging.error(f"Fehler. Keine RouteSection for {self.meldestelle_from} => {self.meldestelle_from} gefunden.")
            return
        self._send_free_routes_and_set_timer(digitales_belegblatt_routes)

        # revert_strecke_for_zugnummer
        self.proxy.digitales_belegblatt.revert_strecke_for_zugnummer(self.zugnummer, self.meldestelle_to)
        # Protokollierung ans Zugmeldebuch
        # TODO

    # reset ArrivalTimeout
    # reset CallerId
    # reset Meldestelle
    def do_arrival_timeout(self,_) -> bool:
        self.caller_id = None
        self.arrival_timeout = None
        self.meldestelle = None

    # reset ArrivalTimeout
    # reset CallerId
    # set Meldestelle in DigitalBelegblatt
    def do_positiv_arrival_confirmation(self,_) -> bool:
        self.caller_id = None
        self.arrival_timeout = None
        logging.info(f"Bestätigte Ankunftsmeldung zugnummer: {self.zugnummer} status: {self.meldestelle}")
        self.proxy.digitales_belegblatt.set_zug_position(self.zugnummer,self.meldestelle)

    # reset ArrivalTimeout
    # reset CallerId
    # reset Meldestelle
    def do_negativ_arrival_confirmation(self,_) -> bool:
        self.caller_id = None
        self.arrival_timeout = None
        self.meldestelle = None




    # set FreeRouteTimeout
    # send FreeRoute
    def do_free_route(self,_) -> bool:
        logging.info(f"Fahrstrasse auflösen zugnummer: {self.zugnummer} / {self.meldestelle_from} => {self.meldestelle_to}")
        
        # resolve routes
        digitales_belegblatt_routes = self.proxy.route_manager.get_free_routes_for_strecke(self.meldestelle_from,self.meldestelle_to,self.direction)
        if not digitales_belegblatt_routes or len(digitales_belegblatt_routes) == 0:
            logging.error(f"Fehler. Keine Route von {self.meldestelle_from} nach {self.meldestelle_to} gefunden.")
            return

        # free route
        self._send_free_routes_and_set_timer(digitales_belegblatt_routes)
        


        # set FreeRouteTimeout
        # send FreeRoute
    def do_delete_position(self,_) -> bool:
        logging.info(f"Aktuelle Position auflösen: {self.zugnummer}")
        digitales_belegblatt_routes = self.proxy.route_manager.get_routes_for_meldestelle(self.meldestelle,self.direction)
        if not digitales_belegblatt_routes or len(digitales_belegblatt_routes) == 0:
            logging.error(f"Fehler. Keine RouteSection for {self.meldestelle} gefunden.")
            return

        # free route
        self._send_free_routes_and_set_timer(digitales_belegblatt_routes)

        # set FreeRouteTimeout
        # send FreeRoute
    def do_automatic_delete_position(self, _) -> bool:
        self.do_delete_position(None)

        # reset FreeRouteTimeout
        # reset meldestelle_from and meldestelle_to
    def do_positive_free_route_response(self,_) -> bool:
        self.free_route_timeout = None
        self.meldestelle_from = None
        self.meldestelle_to = None

        # reset FreeRouteTimeout
        # reset meldestelle_from and meldestelle_to
        # Send System Fehler
    def do_negative_free_route_response(self,_) -> bool:
        #TODO Send System Error
        logging.error(f"Negative Free Route Response for Route {self.meldestelle_from} : {self.meldestelle_to}")
        self.free_route_timeout = None
        self.meldestelle_from = None
        self.meldestelle_to = None




        # reset FreeRouteTimeout
        # reset meldestelle_from and meldestelle_to
        # Send System Fehler
    def do_free_route_timeout(self,_) -> bool:
        #TODO Send System Error
        logging.error(f"Timeout for Free Route Response for Route {self.meldestelle_from} : {self.meldestelle_to}")
        self.free_route_timeout = None
        self.meldestelle_from = None
        self.meldestelle_to = None

        # send SetRouteRequest
        # set SetRoute Timeout
        # set meldestelle_from and meldestelle_to
        # set CallerId
    def do_set_route(self,fahranfrage: Fahranfrage) -> bool:
        #Routen-Id's zu Zugfahrt A-> B ermitteln
        meldestelle_from = self.proxy.digitales_belegblatt.get_zug_position(fahranfrage.zugnummer)
        meldestelle_to = fahranfrage.meldestelle
        logging.info(f"Recv Fahranfrage zugnummer: {fahranfrage.zugnummer} / {meldestelle_from} => {meldestelle_to}")

        digitales_belegblatt_routes = self.proxy.route_manager.get_set_routes_for_strecke(meldestelle_from,meldestelle_to,self.direction)
        logging.info(f"Resolved routes {digitales_belegblatt_routes}")
        if not digitales_belegblatt_routes or len(digitales_belegblatt_routes) == 0:
            self.proxy.set_command(Command(caller_id = fahranfrage.caller_id,
                                           command_type="FahranfrageRouteNichtGefunden",
                                           text = f"Fahranfrage kann nicht beantwortet werden, da keine Route von {meldestelle_from} nach {meldestelle_to} gefunden wurde."))
            return

        # set route
        routes = []
        for route in digitales_belegblatt_routes:
            routes.append(Route(meldestelle_from=route[0],meldestelle_to=route[1]))
        self.proxy.set_route(SetRoute(zugnummer=fahranfrage.zugnummer,routes= routes))
        self.caller_id = fahranfrage.caller_id
        self.meldestelle_from = meldestelle_from
        self.meldestelle_to = meldestelle_to
        self.set_route_timeout = datetime.now() + self.SET_ROUTE_TIMEOUT


        # set SetRouteTimeout
        # send SetRoute
    def do_set_position(self,_) -> bool:

        #TODO
        if not self.direction:
            self.direction = self.proxy.route_manager.get_direction_for_start(self.meldestelle)

        logging.info(f"Aktuelle Position sperren: {self.zugnummer} {self.meldestelle} / {self.direction}")
        digitales_belegblatt_routes = self.proxy.route_manager.get_routes_for_meldestelle(self.meldestelle,self.direction)
        if not digitales_belegblatt_routes or len(digitales_belegblatt_routes) == 0:
            logging.error(f"Fehler. Keine RouteSection for {self.meldestelle} gefunden.")
            return
        
        # set route
        routes = []
        for route in digitales_belegblatt_routes:
            routes.append(Route(meldestelle_from=route[0],meldestelle_to=route[1]))
        self.proxy.set_route(SetRoute(zugnummer=self.zugnummer,routes= routes))
        self.set_route_timeout = datetime.now() + self.SET_ROUTE_TIMEOUT

        # reset SetRouteTimeout
        # set first arrival false
    def do_positive_set_position(self,_) -> bool:
        self.first_arrival = False
        self.set_route_timeout = None

        # reset SetRouteTimeout
        # Send System Fehler
    def do_negative_set_position(self,_) -> bool:
        logging.error(f"Negative Free Route Response for station {self.meldestelle}")
        self.first_arrival = False
        self.set_route_timeout = None

        # reset SetRouteTimeout
        # Send System Fehler
    def do_set_position_timeout(self,_) -> bool:
        #TODO Send System Error 
        logging.error(f"Negative Free Route Response for station {self.meldestelle}")
        self.first_arrival = False
        self.set_route_timeout = None




        # reset FreeRouteTimeout
        # reset meldestelle
    def do_positive_delete_position(self,_) -> bool:
        self.free_route_timeout = None
        self.meldestelle = None
        self.direction = None

        # reset FreeRouteTimeout
        # Send System Fehler
    def do_negative_delete_position(self,_) -> bool:
        #TODO Send System Error 
        logging.error(f"Negative Free Route Response for station {self.meldestelle}")
        self.free_route_timeout = None

        # reset FreeRouteTimeout
        # Send System Fehler
    def do_delete_position_timeout(self,_) -> bool:
        #TODO Send System Error
        logging.error(f"Timeout for Free Route Response for station {self.meldestelle}")
        self.free_route_timeout = None




    # Helper
    def _send_free_routes_and_set_timer(self,digitales_belegblatt_routes):
        routes = []
        for route in digitales_belegblatt_routes:
            routes.append(Route(meldestelle_from=route[0],meldestelle_to=route[1]))
        self.proxy.free_route(FreeRoute(zugnummer=self.zugnummer,routes= routes))
        self.free_route_timeout = datetime.now() + self.FREE_ROUTE_TIMEOUT