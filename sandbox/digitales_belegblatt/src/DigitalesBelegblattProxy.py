from typing import Callable
import logging
import os
import yaml
import asyncio

from  datetime import datetime

from fastapi import WebSocket, Depends, HTTPException

from digitales_belegblatt.digitales_belegblatt import DigitalesBelegblatt

from zlicutils.server import Server
from zlicutils.tan import TanManager

from .Fahranfrage import Fahranfrage
from .FahrbefehlWiederholung import FahrbefehlWiederholung
from .SetRoute import SetRoute
from .FreeRoute import FreeRoute
from .Command import Command
from .Anmeldung import Anmeldung
from .Ankunftsmeldung import Ankunftsmeldung
from .SetRouteResponse import SetRouteResponse
from .FreeRouteResponse import FreeRouteResponse
from .NegativeBestaetigung import NegativeBestaetigung
from .PositiveBestaetigung import PositiveBestaetigung
from .EmergencyStopControl import EmergencyStopControl



from .train_supervision import TrainSupervision
from .route_manager import RouteManager
from .DigitalesBelegblattProxyInterface import DigitalesBelegblattProxyInterface


def parse_offset(offset):
    if not offset:
        return None
    return datetime.strptime(offset, '%Y-%m-%dT%H:%M')  # 2023-08-09T21:00


def str2bool(v):
    return v.lower() in ("yes", "true", "1")


class DigitalesBelegblattProxy(DigitalesBelegblattProxyInterface):

    def __init__(self, with_server=True):
        self.set_route = None
        self.free_route = None
        self.set_command = None
        self.state_machines = {}
        self.nothalt = False

        # Only use in demonstration mode
        self.automatic_train_registration = False
        self.automatic_train_deregistration = False

        if with_server:
            self.tan_manager = TanManager()
            server_port = os.environ.get("SERVER_PORT")
            if not server_port:
                logging.error("environment SERVER_PORT must be set. Using default port 8080.")
                server_port = '8080'

            self.server = Server(host='0.0.0.0', port=int(server_port))
            self.server.router.add_api_route("/debug", self._debug, methods=["GET"])
            self.server.router.add_api_route("/tan", self.tan_manager.tans_as_html, methods=["GET"])
            self.server.router.add_api_route("/deregister/{zugnummer}", self._deregister, methods=["GET"], dependencies=[Depends(self.tan_manager.verify_tan)])
            self.server.router.add_api_route("/register/{zugnummer}", self._register, methods=["GET"], dependencies=[Depends(self.tan_manager.verify_tan)])
            self.server.router.add_api_route("/set_direction/{zugnummer}/{direction}", self._set_direction, methods=["GET"])
            self.server.router.add_api_websocket_route("/ws_image", self._ws_image)

            self.server.start()

        dirname = os.path.dirname(__file__)
        data_model_file = os.path.join(dirname, '../datamodel/model.yml')
        with open(data_model_file, "r",encoding='UTF-8') as f:
            data_model = yaml.safe_load(f)
            if 'tracks' not in data_model:
                raise ValueError(f"routes have to be defined in {data_model_file}")
            if len(data_model['tracks']) != 1:
                raise ValueError(f"implementation only can handle one (not {len(data_model['tracks'])}) tracks")
            track = data_model['tracks'][0]
            self.digitales_belegblatt = DigitalesBelegblatt(track['stop_over_stations'])
            self.route_manager = RouteManager(track['routes'])

            # Voice protocol Options
            if 'automatic_train_registration' not in data_model:
                raise ValueError(f"automatic_train_registration has to be defined in {data_model_file}")
            self.automatic_train_registration = str2bool(data_model['automatic_train_registration'])
            if 'automatic_train_deregistration' not in data_model:
                raise ValueError(f"automatic_train_deregistration has to be defined in {data_model_file}")
            self.automatic_train_deregistration = str2bool(data_model['automatic_train_deregistration'])

    async def _debug(self):

        debug = {
            "trains": {},
            "statemachines": {},
        }
        for betriebsstelle in self.digitales_belegblatt.betriebsstellen:
            debug["trains"][betriebsstelle] = []
        for train in self.digitales_belegblatt.get_trains():
            betriebsstelle = self.digitales_belegblatt.get_zug_position(train)
            debug["trains"][betriebsstelle].append(train)

        for zugnummer, statemachine in self.state_machines.items():
            debug["statemachines"][zugnummer] = {
                "state": str(statemachine.state),
                "direction": f"{statemachine.direction}",
                "route": f"{statemachine.meldestelle_from} -> {statemachine.meldestelle_to}",
                "meldestelle": f"{statemachine.meldestelle}",
                "caller_id": f"{statemachine.caller_id}",
                "arrival_timeout": f"{statemachine.arrival_timeout}",
                "free_route_timeout": f"{statemachine.free_route_timeout}",
                "set_route_timeout": f"{statemachine.set_route_timeout}",
                "driving_permission_timeout": f"{statemachine.driving_permission_timeout}",
            }

        return debug

    async def _register(self, zugnummer):

        if self.register(int(zugnummer)):
            return {"msg": "Ok"}

        raise HTTPException(
            status_code=409,
            detail="Status does not allow registration at the moment"
        )

    async def _deregister(self, zugnummer):

        if self.deregister(int(zugnummer)):
            return {"msg": "Ok"}

        raise HTTPException(
            status_code=409,
            detail="Status does not allow deregistration at the moment"
        )

    async def _set_direction(self, zugnummer, direction):
        if self.set_direction(int(zugnummer), direction):
            return {"msg": "Ok"}
        else:
            return {"msg": f"deregister {zugnummer} failed"}

    async def _ws_image(self, websocket: WebSocket, refresh="0.1", minutes="15", offset=None):
        await websocket.accept()
        try:
            while True:
                svg_img = self.digitales_belegblatt.generate_image(minutes=int(minutes), offset=parse_offset(offset))
                await websocket.send_text(svg_img)
                await asyncio.sleep(float(refresh))

        except Exception as e:
            print(e)
        finally:
            websocket.close()

    def _get_state_machine_by_caller_id(self, caller_id) -> TrainSupervision:
        result = []
        for state_machine in self.state_machines.values():
            if state_machine.caller_id and (state_machine.caller_id == caller_id):
                result.append(state_machine)

        if len(result) == 0:
            return None
        if len(result) == 1:
            return result[0]

        raise ValueError(f"ambiguous state machines found {result}")

    def _control_disabled(self):
        return self.nothalt

    def set_FreeRoute_callback(self, callback: Callable[[FreeRoute], None]) -> None:
        self.free_route = callback

    def set_SetRoute_callback(self, callback: Callable[[SetRoute], None]) -> None:
        self.set_route = callback

    def set_Command_callback(self, callback: Callable[[Command], None]) -> None:
        self.set_command = callback

    def apply_Anmeldung(self, anmeldung: Anmeldung) -> None:
        logging.info(f"Recv Anmeldung zugnummer: {anmeldung.zugnummer} caller_id: {anmeldung.caller_id} ")

        if self._control_disabled():
            self.set_command(Command(caller_id=anmeldung.caller_id,
                                     command_type="SystemDeaktiviert",
                                     text="Fehler. Anmeldung kann nicht angenommen werden da das System deaktiviert ist. Warten!"))
            return

        if anmeldung.zugnummer in self.state_machines:
            self.set_command(Command(caller_id=anmeldung.caller_id,
                                     command_type="ZugBereitsAngemeldet",
                                     text=f"Fehler. Zug {anmeldung.zugnummer} bereits angemeldet."))
            return

        self.state_machines[anmeldung.zugnummer] = TrainSupervision(self)
        self.state_machines[anmeldung.zugnummer].raise_event(TrainSupervision.event_registration_message, anmeldung)

    def apply_Ankunftsmeldung(self, ankunftsmeldung: Ankunftsmeldung) -> None:         
        logging.info(f"Recv Ankunftsmeldung zugnummer: {ankunftsmeldung.zugnummer} status: {ankunftsmeldung.meldestelle}")

        if ankunftsmeldung.zugnummer not in self.state_machines:

            if not self.automatic_train_registration:
                self.set_command(Command(caller_id=ankunftsmeldung.caller_id,
                                         command_type="ZugNichtAngemeldet",
                                         text=f"Fehler. Zug {ankunftsmeldung.zugnummer} muss erst angemeldet werden."))
                return

            # Option automatische Anmeldung
            if not self.register(ankunftsmeldung.zugnummer):
                self.set_command(Command(caller_id=ankunftsmeldung.caller_id,
                                         command_type="AutomatischeZuganmeldungNichtVerarbeitet",
                                         text=f"Fehler. Zug {ankunftsmeldung.zugnummer} kann nicht automatisch angemeldet werden."))
                return

        # Check Current State #TODO move to fsm
        if self.state_machines[ankunftsmeldung.zugnummer].state == TrainSupervision.state_confirmed_position:
            pos = self.digitales_belegblatt.get_zug_position(ankunftsmeldung.zugnummer)
            self.set_command(Command(caller_id=ankunftsmeldung.caller_id,
                                     command_type="ZugBereitsGemeldet",
                                     text=f"Fehler. Zug {ankunftsmeldung.zugnummer} ist bereits in {pos} gemeldet."))
            return

        if self.state_machines[ankunftsmeldung.zugnummer].state != TrainSupervision.state_in_movement:
            self.set_command(Command(caller_id=ankunftsmeldung.caller_id,
                                     command_type="ZuganmeldungNichtVerarbeitet",
                                     text=f"Fehler. Eine Ankunftsmeldung für Zug {ankunftsmeldung.zugnummer} kann nicht verarbeitet werden."))
            return

        self.state_machines[ankunftsmeldung.zugnummer].raise_event(TrainSupervision.event_arrival_message, ankunftsmeldung)

    def apply_Fahranfrage(self, fahranfrage: Fahranfrage) -> None:
        logging.info(f"Recv Fahranfrage zugnummer: {fahranfrage.zugnummer} meldestelle: {fahranfrage.meldestelle} caller_id: {fahranfrage.caller_id} ")

        if self._control_disabled():
            self.set_command(Command(caller_id=fahranfrage.caller_id,
                                     command_type="SystemDeaktiviert",
                                     text="Fehler. Fahranfrage kann nicht bearbeitet werden, da das System deaktiviert ist. Warten!"))
            return

        if fahranfrage.zugnummer not in self.state_machines:
            self.set_command(Command(caller_id=fahranfrage.caller_id,
                                     command_type="ZugNichtAngemeldet",
                                     text=f"Fehler. Zug {fahranfrage.zugnummer} muss erst angemeldet werden."))
            return

        # Check Current State
        if self.state_machines[fahranfrage.zugnummer].state != TrainSupervision.state_confirmed_position:
            self.set_command(Command(caller_id=fahranfrage.caller_id,
                                     command_type="ZugpositionNichtBekannt",
                                     text=f"Fehler. Eine valide Zugposition für Zug {fahranfrage.zugnummer} ist nicht bekannt."))
            return

        self.state_machines[fahranfrage.zugnummer].raise_event(TrainSupervision.event_driving_request_message,fahranfrage)

    def apply_SetRouteResponse(self, set_route_response: SetRouteResponse) -> None:
        logging.info(f"Recv SetRouteResponse zugnummer: {set_route_response.zugnummer} status: {set_route_response.status}")

        if set_route_response.zugnummer not in self.state_machines:
            logging.error(f"For train {set_route_response.zugnummer } does not exists a state machine")
            return

        self.state_machines[set_route_response.zugnummer].raise_event(TrainSupervision.event_set_route_response_message, set_route_response)

    def apply_FreeRouteResponse(self, free_route_response: FreeRouteResponse) -> None:

        logging.info(f"Recv FreeRouteResponse zugnummer: {free_route_response.zugnummer} status: {free_route_response.status}")
        if free_route_response.zugnummer not in self.state_machines:
            logging.error(f"For train {free_route_response.zugnummer } does not exists state machine")
            return
        self.state_machines[free_route_response.zugnummer].raise_event(TrainSupervision.event_free_route_response_message, free_route_response)

    def apply_FahrbefehlWiederholung(self, fahrbefehl_wiederholung: FahrbefehlWiederholung) -> None:

        logging.info(f"Recv FahrbefehlWiederholung zugnummer: {fahrbefehl_wiederholung.zugnummer} meldestelle: {fahrbefehl_wiederholung.meldestelle} caller_id: {fahrbefehl_wiederholung.caller_id}")

        if self._control_disabled():
            self.set_command(Command(caller_id=fahrbefehl_wiederholung.caller_id,
                                     command_type="SystemDeaktiviert",
                                     text="Fehler. Fahrbefehlwiederholung kann nicht bearbeitet werden, da das System deaktiviert ist. Warten!"))
            return

        if fahrbefehl_wiederholung.zugnummer not in self.state_machines:
            self.set_command(Command(caller_id=fahrbefehl_wiederholung.caller_id,
                                     command_type="ZugKeinFahrbefehl",
                                     text=f"Nein. Falsch. Für die Zugnummer {fahrbefehl_wiederholung.zugnummer} wurde kein Fahrbefehl erteilt. Warten."))
            return

        # Check Current State
        if self.state_machines[fahrbefehl_wiederholung.zugnummer].state != TrainSupervision.state_driving_permission:
            self.set_command(Command(caller_id=fahrbefehl_wiederholung.caller_id,
                                     command_type="ZugKeinFahrbefehl",
                                     text=f"Nein. Falsch. Für die Zugnummer {fahrbefehl_wiederholung.zugnummer} wurde kein Fahrbefehl erteilt. Warten."))
            return

        self.state_machines[fahrbefehl_wiederholung.zugnummer].raise_event(TrainSupervision.event_driving_permission_replay_message,
                                                                           fahrbefehl_wiederholung)

    def apply_NegativeBestaetigung(self, negative_bestaetigung: NegativeBestaetigung) -> None:

        state_machine = self._get_state_machine_by_caller_id(negative_bestaetigung.caller_id)
        if not state_machine:
            self.set_command(Command(caller_id=negative_bestaetigung.caller_id,
                                     command_type="BestätigungNichtErwartet",
                                     text="Fehler. Keine Bestätigung erwartet."))
            return

        state_machine.raise_event(TrainSupervision.event_negative_confirmation_message, negative_bestaetigung)

    def apply_PositiveBestaetigung(self, positive_bestaetigung: PositiveBestaetigung) -> None:

        state_machine = self._get_state_machine_by_caller_id(positive_bestaetigung.caller_id)
        if not state_machine:
            self.set_command(Command(caller_id=positive_bestaetigung.caller_id,
                                     command_type="BestätigungNichtErwartet",
                                     text="Fehler. Keine Bestätigung erwartet."))
            return

        state_machine.raise_event(TrainSupervision.event_positive_confirmation_message, positive_bestaetigung)

    def apply_EmergencyStopControl(self, emergency_stop_control: EmergencyStopControl) -> None:

        if emergency_stop_control.command == "enable":
            logging.info("Enable Control, Emergency stop disabled")
            self.nothalt = False
            self.digitales_belegblatt.set_nothalt(False)
        else:
            logging.info(f"Block any Control, Emergency  stop active command={emergency_stop_control.command}")
            self.nothalt = True
            self.digitales_belegblatt.set_nothalt(True)

    def tick(self, _) -> None:
        for state_machine in self.state_machines.values():
            state_machine.raise_event(TrainSupervision.event_timer, None)

    def register(self, zugnummer) -> None:

        if zugnummer in self.state_machines:
            logging.error(f"Train {zugnummer } has already been registered.")
            return False

        dummy_anmeldung = Anmeldung(caller_id="None", zugnummer=zugnummer)

        self.state_machines[zugnummer] = TrainSupervision(self)
        return self.state_machines[zugnummer].raise_event(TrainSupervision.event_registration_message, dummy_anmeldung)

    def deregister(self, zugnummer) -> None:

        if zugnummer not in self.state_machines:
            logging.error(f"For train {zugnummer } does not exists a state machine")
            return False

        return self.state_machines[zugnummer].raise_event(TrainSupervision.event_deregister, None)

    def set_direction(self, zugnummer, direction) -> None:

        if direction not in ["out", "back"]:
            logging.error(f"direction {zugnummer } not allowed")
            return False

        if zugnummer not in self.state_machines:
            logging.error(f"For train {zugnummer } does not exists a state machine")
            return False

        self.state_machines[zugnummer].direction = direction
        return True
