from src.DigitalesBelegblattProxy import DigitalesBelegblattProxy
import time
import os
from src.Anmeldung import Anmeldung
from src.Ankunftsmeldung import Ankunftsmeldung
from src.PositiveBestaetigung import PositiveBestaetigung


def dump(message):
    print("Send", message)


proxy = DigitalesBelegblattProxy()

proxy.set_Command_callback(dump)

try:

    proxy.apply_Anmeldung(Anmeldung(caller_id="x", zugnummer=27))
    proxy.apply_Anmeldung(Anmeldung(caller_id="y", zugnummer=18))

    proxy.apply_Ankunftsmeldung(Ankunftsmeldung(caller_id="x", zugnummer=27, meldestelle='S-Berg'))
    proxy.apply_PositiveBestaetigung(PositiveBestaetigung(caller_id="x"))
 
    while True:
        time.sleep(10)

except:
    os._exit(1)
