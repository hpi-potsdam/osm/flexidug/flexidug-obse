RUN apt-get update && apt-get -y install git
COPY requirements.txt requirements.txt
RUN pip install -r ./requirements.txt
COPY htdocs htdocs