var ws = undefined
function connect() {

	minutes = $("#minutes").val()
	offset = $("#offset").val()
	refresh = $("#refresh").val()

	url = "/ws_image?refresh="+refresh+"&minutes="+minutes

	if (offset)
		url += "&offset="+offset

	ws =  new WebSocket(((window.location.protocol === "https:") ? "wss://" : "ws://") + window.location.host + url)

	ws.onopen = function() {
		console.log("[open] Connection established");
	}
  
	ws.onmessage = async function(e) {
		console.log(`[message] Data received from server`);
		try 
		{
            src = 'data:image/svg+xml;utf8,' + e.data;
			
            $("#belegblatt").attr("src",src);
		}
		catch( error ) {
            console.log(error)
        }
		
		
	}
  
	ws.onclose = function(e) {
		if (e.wasClean) {
			console.log(`[close] Connection closed cleanly, code=${e.code} reason=${e.reason}`);
		} else {
			// e.g. server process killed or network down
			// event.code is usually 1006 in this case
			console.log('[close] Connection died');
		}	  
		setTimeout(function() {
			connect();
	  	}, 1000);
	};
  
	ws.onerror = function(err) {
	  console.error('[error] Socket encountered error: ', err.message, 'Closing socket');
	  ws.close();
	};
}

function get_debug_info() {

	$.get( "debug", function( data ) {
		table = ""

		table += "<h5>Betriebsstellen</h5>"

		table += "<table>"
		for(var bs in data.trains) {
			table += '<tr><td>' + bs + '</td><td>'+data.trains[bs]+'</td></tr>' 
		}
		table += "</table>"
		table += "<h5>Zustandsautomaten</h5>"

		table += "<table>"
		
		table += '<tr>'
		table += '<th>Zug</th>'
		table += '<th>State</th>'
		table += '<th>State</th>'
		table += '<th>Direction</th>'
		table += '<th>Route</th>'
		table += '<th>Meldestelle</th>'
		table += '<th>caller_id</th>'
		table += '<th>arrival_timeout</th>'
		table += '<th>free_route_timeout</th>'
		table += '<th>set_route_timeout</th>'
		table += '<th>driving_permission_timeout</th>'
		table += '</tr>' 


		for(var train in data.statemachines) {
			table += '<tr>'
			table += '<td>'+train+'</td>'
			table += '<td>'+data.statemachines[train].state+'</td>' 

			table += '<td><a href="#" data-train='+train+' class="deregister_train">deregister</a><td>'

			table += '<td>'+data.statemachines[train].direction+'</td>'
			table += '<td>'+data.statemachines[train].route+'</td>'
			table += '<td>'+data.statemachines[train].meldestelle+'</td>'
			table += '<td>'+data.statemachines[train].caller_id+'</td>'
			table += '<td>'+data.statemachines[train].arrival_timeout+'</td>'
			table += '<td>'+data.statemachines[train].free_route_timeout+'</td>'
			table += '<td>'+data.statemachines[train].set_route_timeout+'</td>'
			table += '<td>'+data.statemachines[train].driving_permission_timeout+'</td>'
			table += '</tr>' 

		}
		table += "</table>"

		$("#debug").html(table)

		$(".deregister_train").on("click", function(ev){
		
			ev.preventDefault()
			train = $(this).data('train')
			let tan = prompt("Zug abmelden "+train+".\nBitte geben Sie eine TAN ein:");
			console.log(tan,train)
			if(tan) {
				$.ajax({
					type: "GET",
					url: "/deregister/"+train,
					cache: false,
					headers: {"Flexidug-Authorization-TAN": tan},
					success: function(data) {
						console.log('Success!',data); 
					},
					error: function (request, status, error) {
						alert(request.responseText);
					}
				});
			}

		})



	});

	setTimeout(function() {
		get_debug_info();
	}, 1000);

}


$( document ).ready(function() {


	//Websocket
	console.log("started")

	$( ".input" ).on( "change", function(ev) {
		ev.preventDefault()
		ws.close()
	});


	$("#register_train").on("submit", function(ev){
		ev.preventDefault()
		train = $("#train").val()
		let tan = prompt("Zug anmelden "+train+".\nBitte geben Sie eine TAN ein:");
		console.log(tan,train)
		if(tan) {
			$.ajax({
				type: "GET",
				url: "/register/"+train,
				cache: false,
				headers: {"Flexidug-Authorization-TAN": tan},
				success: function(data) {
					console.log('Success!',data); 
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			});
		}

	})

	
	connect()

	//get debug info
	get_debug_info()

});