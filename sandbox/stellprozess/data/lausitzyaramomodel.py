from yaramo.model import Node, DbrefGeoNode, Edge, Signal, SignalDirection, SignalFunction, SignalKind, Topology
from railwayroutegenerator.routegenerator import RouteGenerator
from planproexporter.generator import Generator

def get_model():
    topology = Topology(name="Lausitz-Beispiel")

    # S-Berg
    bs_s_berg_end_t1 = Node(name="BS S-Berg Gleisende Gleis 1")
    bs_s_berg_end_t1.geo_node = DbrefGeoNode(-20, 10)
    topology.add_node(bs_s_berg_end_t1)

    bs_s_berg_end_t2 = Node(name="BS S-Berg Gleisende Gleis 2")
    bs_s_berg_end_t2.geo_node = DbrefGeoNode(-20, 0)
    topology.add_node(bs_s_berg_end_t2)

    bs_s_berg_w1 = Node(name="BS S-Berg Weiche W1")
    bs_s_berg_w1.geo_node = DbrefGeoNode(10, 0)
    topology.add_node(bs_s_berg_w1)

    bs_s_berg_w2 = Node(name="BS S-Berg Weiche W2")
    bs_s_berg_w2.geo_node = DbrefGeoNode(20, 10)
    topology.add_node(bs_s_berg_w2)

    bs_s_berg_w3 = Node(name="BS S-Berg Weiche W3")
    bs_s_berg_w3.geo_node = DbrefGeoNode(50, 0)
    topology.add_node(bs_s_berg_w3)

    # C-Dorf
    bs_c_stadt_w1 = Node(name="BS C-Dorf Weiche W1")
    bs_c_stadt_w1.geo_node = DbrefGeoNode(80, 0)
    topology.add_node(bs_c_stadt_w1)

    bs_c_stadt_w2 = Node(name="BS C-Dorf Weiche W2")
    bs_c_stadt_w2.geo_node = DbrefGeoNode(110, 10)
    topology.add_node(bs_c_stadt_w2)

    bs_c_stadt_w3 = Node(name="BS C-Dorf Weiche W3")
    bs_c_stadt_w3.geo_node = DbrefGeoNode(120, 0)
    topology.add_node(bs_c_stadt_w3)

    bs_c_stadt_end_t1 = Node(name="BS C-Dorf Gleisende Gleis 1")
    bs_c_stadt_end_t1.geo_node = DbrefGeoNode(150, 10)
    topology.add_node(bs_c_stadt_end_t1)

    bs_c_stadt_end_t2 = Node(name="BS C-Dorf Gleisende Gleis 2")
    bs_c_stadt_end_t2.geo_node = DbrefGeoNode(150, 0)
    topology.add_node(bs_c_stadt_end_t2)

    # Edges S-Berg
    bs_s_berg_e1 = Edge(bs_s_berg_end_t1, bs_s_berg_w2)
    bs_s_berg_e1.update_length()
    topology.add_edge(bs_s_berg_e1)
    bs_s_berg_end_t1.set_connection_head(bs_s_berg_w2)
    bs_s_berg_w2.set_connection_right(bs_s_berg_end_t1)

    bs_s_berg_e2 = Edge(bs_s_berg_w2, bs_s_berg_w3)
    bs_s_berg_e2.intermediate_geo_nodes.append(DbrefGeoNode(40, 10))
    bs_s_berg_e2.update_length()
    topology.add_edge(bs_s_berg_e2)
    bs_s_berg_w2.set_connection_head(bs_s_berg_w3)
    bs_s_berg_w3.set_connection_right(bs_s_berg_w2)

    bs_s_berg_e3 = Edge(bs_s_berg_end_t2, bs_s_berg_w1)
    bs_s_berg_e3.update_length()
    topology.add_edge(bs_s_berg_e3)
    bs_s_berg_end_t2.set_connection_head(bs_s_berg_w1)
    bs_s_berg_w1.set_connection_head(bs_s_berg_end_t2)

    bs_s_berg_e4 = Edge(bs_s_berg_w1, bs_s_berg_w3)
    bs_s_berg_e4.update_length()
    topology.add_edge(bs_s_berg_e4)
    bs_s_berg_w1.set_connection_right(bs_s_berg_w3)
    bs_s_berg_w3.set_connection_left(bs_s_berg_w1)

    bs_s_berg_e5 = Edge(bs_s_berg_w1, bs_s_berg_w2)
    bs_s_berg_e5.update_length()
    topology.add_edge(bs_s_berg_e5)
    bs_s_berg_w1.set_connection_left(bs_s_berg_w2)
    bs_s_berg_w2.set_connection_left(bs_s_berg_w1)

    # Connections S-Berg C-Dorf
    bs_connection = Edge(bs_s_berg_w3, bs_c_stadt_w1)
    bs_connection.update_length()
    topology.add_edge(bs_connection)
    bs_s_berg_w3.set_connection_head(bs_c_stadt_w1)
    bs_c_stadt_w1.set_connection_head(bs_s_berg_w3)

    # Edges C-Dorf
    bs_c_stadt_e1 = Edge(bs_c_stadt_w1, bs_c_stadt_w2)
    bs_c_stadt_e1.intermediate_geo_nodes.append(DbrefGeoNode(90, 10))
    bs_c_stadt_e1.update_length()
    topology.add_edge(bs_c_stadt_e1)
    bs_c_stadt_w1.set_connection_left(bs_c_stadt_w2)
    bs_c_stadt_w2.set_connection_head(bs_c_stadt_w1)

    bs_c_stadt_e2 = Edge(bs_c_stadt_w2, bs_c_stadt_end_t1)
    bs_c_stadt_e2.update_length()
    topology.add_edge(bs_c_stadt_e2)
    bs_c_stadt_w2.set_connection_left(bs_c_stadt_end_t1)
    bs_c_stadt_end_t1.set_connection_head(bs_c_stadt_w2)

    bs_c_stadt_e3 = Edge(bs_c_stadt_w1, bs_c_stadt_w3)
    bs_c_stadt_e3.update_length()
    topology.add_edge(bs_c_stadt_e3)
    bs_c_stadt_w1.set_connection_right(bs_c_stadt_w3)
    bs_c_stadt_w3.set_connection_left(bs_c_stadt_w1)

    bs_c_stadt_e4 = Edge(bs_c_stadt_w3, bs_c_stadt_end_t2)
    bs_c_stadt_e4.update_length()
    topology.add_edge(bs_c_stadt_e4)
    bs_c_stadt_w3.set_connection_head(bs_c_stadt_end_t2)
    bs_c_stadt_end_t2.set_connection_head(bs_c_stadt_w3)

    bs_c_stadt_e5 = Edge(bs_c_stadt_w2, bs_c_stadt_w3)
    bs_c_stadt_e5.update_length()
    topology.add_edge(bs_c_stadt_e5)
    bs_c_stadt_w2.set_connection_right(bs_c_stadt_w3)
    bs_c_stadt_w3.set_connection_right(bs_c_stadt_w2)

    for key in topology.edges:
        topology.edges[key].maximum_speed = 30

    # Signals S-Berg
    signal_e1 = Signal(bs_s_berg_e3, 25, direction=SignalDirection.IN, function=SignalFunction.Einfahr_Signal,
                       kind=SignalKind.Hauptsignal, name="E1")
    topology.add_signal(signal_e1)
    bs_s_berg_e3.signals.append(signal_e1)

    signal_a1 = Signal(bs_s_berg_e2, 5, direction=SignalDirection.GEGEN, function=SignalFunction.Ausfahr_Signal,
                       kind=SignalKind.Hauptsignal, name="A1")
    topology.add_signal(signal_a1)
    bs_s_berg_e2.signals.append(signal_a1)

    # Weichenende
    signal_we2 = Signal(bs_s_berg_e2, 20, direction=SignalDirection.GEGEN, function=SignalFunction.Block_Signal,
                        kind=SignalKind.Hauptsignal, name="WE2")
    topology.add_signal(signal_we2)
    bs_s_berg_e2.signals.append(signal_we2)

    signal_a1b = Signal(bs_s_berg_e4, 5, direction=SignalDirection.GEGEN, function=SignalFunction.Ausfahr_Signal,
                        kind=SignalKind.Hauptsignal, name="A1b")
    topology.add_signal(signal_a1b)
    bs_s_berg_e4.signals.append(signal_a1b)

    signal_a2 = Signal(bs_s_berg_e4, 35, direction=SignalDirection.IN, function=SignalFunction.Ausfahr_Signal,
                       kind=SignalKind.Hauptsignal, name="A2")
    topology.add_signal(signal_a2)
    bs_s_berg_e4.signals.append(signal_a2)

    signal_a2b = Signal(bs_s_berg_e2, 20, direction=SignalDirection.IN, function=SignalFunction.Ausfahr_Signal,
                        kind=SignalKind.Hauptsignal, name="A2b")
    topology.add_signal(signal_a2b)
    bs_s_berg_e2.signals.append(signal_a2b)

    signal_e2 = Signal(bs_connection, 5, direction=SignalDirection.GEGEN, function=SignalFunction.Einfahr_Signal,
                       kind=SignalKind.Hauptsignal, name="E2")
    topology.add_signal(signal_e2)
    bs_connection.signals.append(signal_e2)

    # Signals C-Dorf
    signal_e3 = Signal(bs_connection, 25, direction=SignalDirection.IN, function=SignalFunction.Einfahr_Signal,
                       kind=SignalKind.Hauptsignal, name="E3")
    topology.add_signal(signal_e3)
    bs_connection.signals.append(signal_e3)

    signal_b1 = Signal(bs_c_stadt_e1, 15, direction=SignalDirection.GEGEN, function=SignalFunction.Ausfahr_Signal,
                       kind=SignalKind.Hauptsignal, name="B1")
    topology.add_signal(signal_b1)
    bs_c_stadt_e1.signals.append(signal_b1)

    signal_b1b = Signal(bs_c_stadt_e3, 5, direction=SignalDirection.GEGEN, function=SignalFunction.Ausfahr_Signal,
                        kind=SignalKind.Hauptsignal, name="B1b")
    topology.add_signal(signal_b1b)
    bs_c_stadt_e3.signals.append(signal_b1b)

    signal_b2 = Signal(bs_c_stadt_e3, 35, direction=SignalDirection.IN, function=SignalFunction.Ausfahr_Signal,
                       kind=SignalKind.Hauptsignal, name="B2")
    topology.add_signal(signal_b2)
    bs_c_stadt_e3.signals.append(signal_b2)

    # Weichenende
    signal_we1 = Signal(bs_c_stadt_e3, 5, direction=SignalDirection.IN, function=SignalFunction.Block_Signal,
                        kind=SignalKind.Hauptsignal, name="WE1")
    topology.add_signal(signal_we1)
    bs_c_stadt_e3.signals.append(signal_we1)

    signal_b2b = Signal(bs_c_stadt_e1, 25, direction=SignalDirection.IN, function=SignalFunction.Ausfahr_Signal,
                        kind=SignalKind.Hauptsignal, name="B2b")
    topology.add_signal(signal_b2b)
    bs_c_stadt_e1.signals.append(signal_b2b)

    signal_e4 = Signal(bs_c_stadt_e4, 5, direction=SignalDirection.GEGEN, function=SignalFunction.Einfahr_Signal,
                       kind=SignalKind.Hauptsignal, name="E4")
    topology.add_signal(signal_e4)
    bs_c_stadt_e4.signals.append(signal_e4)

    return topology


if __name__ == "__main__":
    print("Yaramo test model")
    topology = get_model()
    print(topology.nodes)
    print(topology.signals)
    route_generator = RouteGenerator(topology)
    route_generator.generate_routes()
    print(topology.routes)

    for _, route in topology.routes.items():
        print(f"{route.start_signal.name} -> {route.end_signal.name}")

    # topology is a yaramo.models.Topology object
    Generator().generate(topology, author_name="FlexiDug", organisation="HPI", filename="lausitz.ppxml")
    # print(topology.to_serializable())
