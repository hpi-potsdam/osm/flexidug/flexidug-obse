import argparse
from planpro_importer.reader import PlanProReader
from railwayroutegenerator.routegenerator import RouteGenerator

parser = argparse.ArgumentParser(description='Create model.')
parser.add_argument("-i", dest="filename", required=True, help="planpro xml input file")
args = parser.parse_args()


_topology = PlanProReader(args.filename).read_topology_from_plan_pro_file()

for key in _topology.edges:
    _topology.edges[key].maximum_speed = 30


route_generator = RouteGenerator(_topology)
route_generator.generate_routes()

print("Routes", len(_topology.routes))
for route in _topology.routes.values():
    print(f"{route.start_signal.name} -> {route.end_signal.name}")

print("Signals", len(_topology.signals))
for signal in _topology.signals.values():
    print(signal)

print("Nodes (Points)", len(_topology.nodes))
for node in _topology.nodes.values():
    if node.connected_on_left or node.connected_on_right:
        print(node)
