from src.StellprozessProxy import StellprozessProxy
from src.SetRouteResponse import SetRouteResponse
from src.FreeRouteResponse import FreeRouteResponse
from src.SetRoute import SetRoute
from src.FreeRoute import FreeRoute
from src.Route import Route


class CallbackMock:
    def __init__(self):
        self.set_route_response_calls = []
        self.free_route_response_calls = []

    def set_route_response(self, set_route_response: SetRouteResponse):
        self.set_route_response_calls.append(set_route_response)

    def free_route_response(self, free_route_response: FreeRouteResponse):
        self.free_route_response_calls.append(free_route_response)


def test_set_route_request():

    callback = CallbackMock()

    stellprozess_proxy = StellprozessProxy()
    
    stellprozess_proxy.set_SetRouteResponse_callback(
        callback=callback.set_route_response)

    routes = [
        Route(meldestelle_from="60ES1", meldestelle_to="60AS2")
    ]
    set_route = SetRoute(zugnummer=101, routes=routes)

    stellprozess_proxy.apply_SetRoute(set_route)

    assert len(callback.set_route_response_calls) == 1
    assert callback.set_route_response_calls[0] == SetRouteResponse(zugnummer=101, status=0)


def test_free_route_request():

    callback = CallbackMock()

    stellprozess_proxy = StellprozessProxy()
    stellprozess_proxy.set_FreeRouteResponse_callback(callback=callback.free_route_response)

    routes = [
        Route(meldestelle_from="60ES1", meldestelle_to="60AS2")
    ]
    free_route = FreeRoute(zugnummer=101, routes=routes)

    stellprozess_proxy.apply_FreeRoute(free_route)

    assert len(callback.free_route_response_calls) == 1
    assert callback.free_route_response_calls[0] == FreeRouteResponse(zugnummer=101, status=-1)


def test_set_and_free_route_request():

    callback = CallbackMock()

    stellprozess_proxy = StellprozessProxy()
    stellprozess_proxy.set_SetRouteResponse_callback(callback=callback.set_route_response)
    stellprozess_proxy.set_FreeRouteResponse_callback(callback=callback.free_route_response)


    routes = [
        Route(meldestelle_from="60ES1", meldestelle_to="60AS2")
    ]

    set_route = SetRoute(zugnummer=101, routes=routes)
    free_route = FreeRoute(zugnummer=101, routes=routes)


    stellprozess_proxy.apply_SetRoute(set_route)
    assert len(callback.set_route_response_calls) == 1
    assert len(callback.free_route_response_calls) == 0
    assert callback.set_route_response_calls[0] == SetRouteResponse(zugnummer=101, status=0)

    stellprozess_proxy.apply_FreeRoute(free_route)
    assert len(callback.free_route_response_calls) == 1
    assert callback.free_route_response_calls[0] == FreeRouteResponse(zugnummer=101, status=0)


def test_nachfahren():

    callback = CallbackMock()

    stellprozess_proxy = StellprozessProxy()
    stellprozess_proxy.set_SetRouteResponse_callback(callback=callback.set_route_response)
    stellprozess_proxy.set_FreeRouteResponse_callback(callback=callback.free_route_response)


    # Zug 101 steht in Schlössel
    routes = [
        Route(meldestelle_from="60ES5", meldestelle_to="60AS6")
    ]

    set_route = SetRoute(zugnummer=101, routes=routes)
    free_route = FreeRoute(zugnummer=101, routes=routes)

    stellprozess_proxy.apply_SetRoute(set_route)

    assert len(callback.set_route_response_calls) == 1
    assert len(callback.free_route_response_calls) == 0
    assert callback.set_route_response_calls[0] == SetRouteResponse(zugnummer=101, status=0)


    # Zug 102 will bis Schlössel fahren

    routes = [
        Route(meldestelle_from="60AS4", meldestelle_to="60ES5"),
        Route(meldestelle_from="60ES5", meldestelle_to="60AS6")
    ]

    set_route = SetRoute(zugnummer=102, routes=routes)

    stellprozess_proxy.apply_SetRoute(set_route)

    assert len(callback.set_route_response_calls) == 2
    assert len(callback.free_route_response_calls) == 0
    assert callback.set_route_response_calls[-1] == SetRouteResponse(zugnummer=102, status=-1)

    # Zug 101 gibt Schlössel frei

    stellprozess_proxy.apply_FreeRoute(free_route)
    assert len(callback.free_route_response_calls) == 1
    assert callback.free_route_response_calls[-1] == FreeRouteResponse(zugnummer=101, status=0)

    # Zug 102 will bis Schlössel fahren
    stellprozess_proxy.apply_SetRoute(set_route)

    assert len(callback.set_route_response_calls) == 3
    assert len(callback.free_route_response_calls) == 1
    assert callback.set_route_response_calls[-1] == SetRouteResponse(zugnummer=102, status=0)




def test_set_blocked_route_request():

    callback = CallbackMock()

    stellprozess_proxy = StellprozessProxy()
    stellprozess_proxy.set_SetRouteResponse_callback(callback=callback.set_route_response)
    stellprozess_proxy.set_FreeRouteResponse_callback(callback=callback.free_route_response)


    routes1 = [
        Route(meldestelle_from="60ES1", meldestelle_to="60AS2")
    ]

    routes2 = [
        Route(meldestelle_from="60ES11", meldestelle_to="60AS12")
    ]

    set_route1 = SetRoute(zugnummer=101, routes=routes1)
    set_route2 = SetRoute(zugnummer=102, routes=routes2)


    stellprozess_proxy.apply_SetRoute(set_route1)
    assert len(callback.set_route_response_calls) == 1
    assert len(callback.free_route_response_calls) == 0
    assert callback.set_route_response_calls[0] == SetRouteResponse(zugnummer=101, status=0)

    stellprozess_proxy.apply_SetRoute(set_route2)
    assert len(callback.set_route_response_calls) == 2
    assert len(callback.free_route_response_calls) == 0
    assert callback.set_route_response_calls[1] == SetRouteResponse(zugnummer=102, status=-1)