# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.

from abc import ABC, abstractmethod
from typing import Callable
from .SetRouteResponse import SetRouteResponse
from .FreeRouteResponse import FreeRouteResponse
from .SetRoute import SetRoute
from .FreeRoute import FreeRoute

class StellprozessProxyInterface(ABC):

    @abstractmethod
    def set_SetRouteResponse_callback(self,callback : Callable[[SetRouteResponse], None]) -> None: pass
    @abstractmethod
    def set_FreeRouteResponse_callback(self,callback : Callable[[FreeRouteResponse], None]) -> None: pass
  
    @abstractmethod
    def apply_SetRoute(self,set_route : SetRoute) -> None: pass
    @abstractmethod
    def apply_FreeRoute(self,free_route : FreeRoute) -> None: pass

    @abstractmethod
    def tick(self,cycle) -> None: pass


