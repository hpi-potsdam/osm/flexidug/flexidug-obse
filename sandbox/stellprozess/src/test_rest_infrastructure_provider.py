from .rest_infrastructure_provider import RESTInfrastructureProvider
import os
import requests
import pytest
import logging


class SignalMock:

    def __init__(self,name):
        self.name = name

class PointMock:
    def __init__(self,uuid):
        self.uuid = uuid

class MockResponse:
    def __init__(self,status_code):
        self.status_code = status_code



link_map = {
    "signals" : {
            "A1" : { 
                    "go" : {
                        "url" : "${INFRASTRUCTURE_SIMULATOR_URL}/api/v1/test",
                        "json" : {
                            "id" : "A1",
                            "cmd" : "off"
                        }
                    },
                    "halt" : {
                        "url" : "${UNKNOWN_ENV_VAR}/api/v1/test",
                        "json" : {
                            "id" : "A1",
                            "cmd" : "on"
                        }

                    },
            },
    },
    "points" : {
        "xyz-uuid-1234" : {
            "left" : {
                "url" : "http://10.10.10.2:8080/run/left",
            },
            "right" : {
                "url" : "http://10.10.10.2:8080/run/right",
            },
        }
    }
}



@pytest.mark.asyncio
async def test_signal(monkeypatch):

    def mock_post(*args, **kwargs):
        logging.info(f"{args} {kwargs}")
        assert "http://simulator:8080/api/v1/test" == args[0]
        assert ("json" in kwargs) and kwargs["json"]
        logging.info(f"{args} {kwargs}")
        return MockResponse(200)

    monkeypatch.setenv("INFRASTRUCTURE_SIMULATOR_URL", "http://simulator:8080")
    monkeypatch.setattr(requests, "post", mock_post)

    provider = RESTInfrastructureProvider(link_map)
    assert True is await provider.set_signal_state(SignalMock("A1"),"go")


@pytest.mark.asyncio
async def test_point(monkeypatch):

    def mock_post(*args, **kwargs):
        logging.info(f"{args} {kwargs}")
        assert "http://10.10.10.2:8080/run/left" == args[0]
        assert "json" not in kwargs 
        return MockResponse(200)

    monkeypatch.setattr(requests, "post", mock_post)

    provider = RESTInfrastructureProvider(link_map)
    assert True is await provider.turn_point(PointMock("xyz-uuid-1234"),"left")
       
@pytest.mark.asyncio
async def test_env_must_defined():
    provider = RESTInfrastructureProvider(link_map)
    assert False is await provider.set_signal_state(SignalMock("A1"),"go")
     
@pytest.mark.asyncio
async def test_unknown_id():    
    provider = RESTInfrastructureProvider(link_map)
    assert False is await provider.turn_point(PointMock("xxx"),None)

@pytest.mark.asyncio
async def test_unknown_command():    
    provider = RESTInfrastructureProvider(link_map)
    assert False is await provider.turn_point(PointMock("xyz-uuid-1234"),"xxx")
