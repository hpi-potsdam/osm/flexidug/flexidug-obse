import logging

from planpro_importer import PlanProVersion, import_planpro


def load_model(plan_pro_model_file):
    

    _topology = import_planpro(plan_pro_model_file, PlanProVersion.PlanPro19)
    
    for key in _topology.edges:
        _topology.edges[key].maximum_speed = 30

    for key in _topology.routes:
        route = _topology.routes[key]
        logging.info(f"{route.start_signal.name} -> {route.end_signal.name}")


    return _topology

