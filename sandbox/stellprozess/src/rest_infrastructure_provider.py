import logging
import re
import os
import requests

from interlocking.infrastructureprovider import InfrastructureProvider
from yaramo.model import Node, Signal


class RESTInfrastructureProvider(InfrastructureProvider):

    def __init__(self, link_map):

        self.link_map = link_map
        super().__init__()

    async def _control(self, key, infra_id, cmd):

        if key not in self.link_map:
            raise ValueError(f"No such key in map {key}")

        if infra_id not in self.link_map[key]:
            logging.error(f"Id '{infra_id}' not found in {key}")
            return False

        element = self.link_map[key][infra_id]
        if cmd not in element:
            logging.error(f"Command '{cmd}' not found for {infra_id} in {key}")
            return False

        command = element[cmd]

        url = command["url"]
        match = re.search(r"[$][{]([A-Z_]+)[}]", url)
        if match:
            env_name = match.group(1)
            env_val = os.environ.get(env_name)
            if not env_val:
                logging.error(f"environment {env_name} must be set.")
                return False
            url = re.sub(r"[$][{]([A-Z_]+)[}]", env_val, url)

        json_data = None
        if "json" in command:
            json_data = command["json"]
        logging.info(f"Command '{command}'")

        try:
            if json_data:
                response = requests.post(url, timeout=1.5, json=json_data)
            else:
                response = requests.post(url, timeout=1.5)
            logging.info(f"Turn Point via post {response}")
            if response.status_code == 200:
                return True
        except requests.exceptions.RequestException as e:
            logging.info(f"{e}")
        return False

    async def turn_point(self, yaramo_point: Node, target_orientation: str):
        logging.info(f"Turn Point {yaramo_point} to {target_orientation}")
        return await self._control("points", yaramo_point.uuid, target_orientation)

    async def set_signal_aspect(self, yaramo_signal: Signal, target_aspect: str):
        logging.info(f"Set Signal {yaramo_signal.name} to {target_aspect}")
        return await self._control("signals", yaramo_signal.name, target_aspect)
