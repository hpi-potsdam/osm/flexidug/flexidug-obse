import logging
class MockInterlocking():
    
    def __init__(self):
        self.routes = [
            {
                "keys" : ["60ES1:60AS2","60ES11:60AS12"],
                "train" : None
            },
            {
                "keys" : ["60AS2:60ES3","60AS10:60ES11"],
                "train" : None
            },
            {
                "keys" : ["60ES3:60AS4","60ES9:60AS10"],
                "train" : None
            },
            {
                "keys" : ["60AS4:60ES5","60AS8:60ES9"],
                "train" : None
            },
            {
                "keys" : ["60ES5:60AS6","60ES7:60AS8"],
                "train" : None
            }
        ]


    def set_route(self,_start_signal_name, _end_signal_name,_train_id):
        key = f"{_start_signal_name}:{_end_signal_name}"
        logging.info(f"Set route {key} :  {_train_id}")

        for segment in self.routes:
            if key not in segment['keys']:
                continue

            if segment['train']:
                logging.error(f"Route {key} always blocked by {segment['train']}")
                return False

            segment['train'] = _train_id
            return True
        
        logging.error(f"Route {key} not found")
        return False

    def reset_route(self,_start_signal_name, _end_signal_name,_train_id):
        key = f"{_start_signal_name}:{_end_signal_name}"
        logging.info(f"Reset route {key} :  {_train_id}")

        for segment in self.routes:
            if key not in segment['keys']:
                continue

            if _train_id is not segment['train']:
                logging.error(f"Route {key} is blocked by {segment['train']} instead {_train_id}")
                return False

            segment['train'] = None
            return True
        
        logging.error(f"Route {key} not found")
        return False
