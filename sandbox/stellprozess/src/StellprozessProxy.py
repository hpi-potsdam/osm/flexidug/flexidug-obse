import logging
import asyncio
import requests
from typing import Callable
import os
import yaml
import json

from railwayroutegenerator.routegenerator import RouteGenerator
from interlocking.interlockinginterface import Interlocking

from .StellprozessProxyInterface import StellprozessProxyInterface
from .FreeRoute import FreeRoute
from .SetRoute import SetRoute
from .FreeRouteResponse import FreeRouteResponse
from .SetRouteResponse import SetRouteResponse

from .load_model import load_model
from .init_providers import init_providers
#from .mock_interlocking import MockInterlocking


class StellprozessProxy(StellprozessProxyInterface):

    def __init__(self):
        self.set_route_response = None
        self.free_route_response = None
        # Load data model File
        providers = None
        dirname = os.path.dirname(__file__)
        data_model_file = os.path.join(dirname, '../datamodel/model.yml')
        with open(data_model_file, "r", encoding='UTF-8') as f:
            data_model = yaml.safe_load(f)
            if 'plan_pro_model_file' not in data_model:
                raise ValueError(f"plan_pro_model_file has to be defined in {data_model_file}")
            plan_pro_model_file = os.path.join(dirname, f"../{data_model['plan_pro_model_file']}")

            providers, provider_ticks = init_providers(data_model)

        self.tick_functions = provider_ticks
        self.topology = load_model(plan_pro_model_file)

        generator = RouteGenerator(self.topology)
        generator.generate_routes()

        self.interlocking = Interlocking(providers)
        self.interlocking.prepare(self.topology)
        self.interlocking.print_state()

        # self.mock_interlocking = MockInterlocking()

    def set_route(self, _start_signal_name, _end_signal_name, _train_id):

        # return self.mock_interlocking.set_route(_start_signal_name, _end_signal_name,_train_id)

        for _route_uuid in self.topology.routes:
            _route = self.topology.routes[_route_uuid]
            if _route.start_signal.name == _start_signal_name and _route.end_signal.name == _end_signal_name:
                could_be_set = asyncio.run(self.interlocking.set_route(_route,_train_id))
                self.interlocking.print_state()
                return could_be_set.success

    def reset_route(self, _start_signal_name, _end_signal_name, _train_id):

        # return self.mock_interlocking.reset_route(_start_signal_name, _end_signal_name,_train_id)

        for _route_uuid in self.topology.routes:
            _route = self.topology.routes[_route_uuid]
            if _route.start_signal.name == _start_signal_name and _route.end_signal.name == _end_signal_name:
                try:
                    asyncio.run(self.interlocking.reset_route(_route,_train_id))
                except Exception as e:
                    logging.error(e)
                    return False
                self.interlocking.print_state()
                return True

    def apply_SetRoute(self, set_route: SetRoute) -> None:
        logging.info(f"Set Route : {set_route.routes} zugnummer: {set_route.zugnummer}")

        status = 0
        l = len(set_route.routes)
        for i in range(l):
            route = set_route.routes[i]
            if not self.set_route(route.meldestelle_from, route.meldestelle_to, set_route.zugnummer):
                status -= 1
                for j in range(i):
                    free_route = set_route.routes[j]
                    if not self.reset_route(free_route.meldestelle_from, free_route.meldestelle_to, set_route.zugnummer):
                        logging.error(f"Cannot free route {free_route.meldestelle_from}:{free_route.meldestelle_to}")
                break

        self.set_route_response(SetRouteResponse(zugnummer=set_route.zugnummer, status=status))

    def apply_FreeRoute(self, free_route: FreeRoute) -> None:
        logging.info(f"Free Route : {free_route.routes} zugnummer: {free_route.zugnummer}")

        status = 0
        for route in free_route.routes:
            if not self.reset_route(route.meldestelle_from, route.meldestelle_to, free_route.zugnummer):
                status -= 1

        self.free_route_response(FreeRouteResponse(zugnummer=free_route.zugnummer, status=status))

    def set_FreeRouteResponse_callback(self, callback: Callable[[FreeRouteResponse], None]) -> None:
        self.free_route_response = callback

    def set_SetRouteResponse_callback(self, callback: Callable[[SetRouteResponse], None]) -> None:
        self.set_route_response = callback

    def tick(self, cycle) -> None:
        for f in self.tick_functions:
            f(cycle)
