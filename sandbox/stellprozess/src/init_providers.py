import logging
import traci
from interlocking.infrastructureprovider import SUMOInfrastructureProvider, LoggingInfrastructureProvider
from .rest_infrastructure_provider import RESTInfrastructureProvider

# TODO: To ensure that no code fragments of the simulators are contained in the target code,
# an adjustment is still necessary here. Possibly analogous to the mustache template technique.
# For development, however, we can leave it as it is for now.


def traci_tick(_):
    traci.simulationStep()


def init_providers(data_model):
    providers = []
    provider_tick_functions = []

    infrastructure_provider = LoggingInfrastructureProvider()
    providers.append(infrastructure_provider)

    if 'infrastructure' in data_model:
        link_map = data_model['infrastructure']
        infrastructure_provider = RESTInfrastructureProvider(link_map)
        providers.append(infrastructure_provider)

    if 'traci' in data_model:
        traci.init(host="s-u-m-o-simulation.internal", port=9090)
        traci.setOrder(2)
        logging.info("TraCi connected")
        sumo_infrastructure_provider = SUMOInfrastructureProvider(traci)
        providers.append(sumo_infrastructure_provider)
        provider_tick_functions.append(traci_tick)

    return providers, provider_tick_functions
