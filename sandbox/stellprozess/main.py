import os

from src.StellprozessProxy import StellprozessProxy
from src.SetRouteResponse import SetRouteResponse
from src.FreeRouteResponse import FreeRouteResponse
from src.SetRoute import SetRoute
from src.FreeRoute import FreeRoute
from src.Route import Route




def set_route_response_callback(response : SetRouteResponse):
    print(response)

def free_route_response_callback(response : FreeRouteResponse):
    print(response)

proxy = StellprozessProxy()
proxy.set_SetRouteResponse_callback(set_route_response_callback)
proxy.set_FreeRouteResponse_callback(free_route_response_callback)

def create_routes_from_string(route_string_line):
    routes = []
    route_strings = route_string_line.split(",")
    for route_string in route_strings:
        meldestelle_from , meldestelle_to = route_string.split(":")
        routes.append(Route(meldestelle_from=meldestelle_from.strip(), meldestelle_to=meldestelle_to.strip()))

    return routes

def parse_parameters(parameter_string):
    zugnr , routes = parameter_string.split("#",1)
    return int(zugnr) , routes

try:
    os.environ["INFRASTRUCTURE_SIMULATOR_URL"] = "http://localhost:8080"
    while True:
        input1 = input("> ")
        if input1 == "quit" or input1 == "exit":
            os._exit(0)
        if input1 == "help":
            print("use set_route 101#A1:B2,B2:A1")
            print("use free_route 101#A1:B2,B2:A1")
        if input1.startswith("set_route"):
            zugnummer , route_str = parse_parameters(input1[9:])
            routes_to_set = create_routes_from_string(route_str)
            set_route = SetRoute(zugnummer=zugnummer, routes=routes_to_set)
            print(set_route)
            proxy.apply_SetRoute(set_route)

        if input1.startswith("free_route"):
            zugnummer , route_str = parse_parameters(input1[10:])
            routes_to_free = create_routes_from_string(route_str)
            free_route = FreeRoute(zugnummer=101, routes=routes_to_free)
            print(free_route)
            proxy.apply_FreeRoute(free_route)


except Exception as e:
    print(e)
    os._exit(1)
