from typing import Callable
import threading
import logging
import requests
import yaml
import os

from .NaturalLanguageUnderstandingProxyInterface import NaturalLanguageUnderstandingProxyInterface
from .Transcribed import Transcribed
from .Fahranfrage import Fahranfrage
from .FahrbefehlWiederholung import FahrbefehlWiederholung
from .Command import Command
from .Anmeldung import Anmeldung
from .Ankunftsmeldung import Ankunftsmeldung
from .NegativeBestaetigung import NegativeBestaetigung
from .PositiveBestaetigung import PositiveBestaetigung
from .EmergencyStopRequest import EmergencyStopRequest

from .mapping_zuglaufstelle import ZuglaufStelleMapper

class NaturalLanguageUnderstandingProxy(NaturalLanguageUnderstandingProxyInterface):

    def __init__(self):

        dirname = os.path.dirname(__file__)
        data_model_file = os.path.join(dirname, '../datamodel/model.yml')
        with open(data_model_file, "r",encoding='UTF-8') as f:
            data_model = yaml.safe_load(f)
            if not 'stations' in data_model:
                raise ValueError(f"stations have to be defined in {data_model_file}")
            self.zuglauf_stelle_mapper = ZuglaufStelleMapper(data_model['stations'])

        self.anmeldung = None
        self.fahranfrage = None
        self.fahrbefehl_wdhl = None
        self.ankunftsmeldung = None
        self.nlu_fehler = None
        self.positive_bestaetigung = None
        self.negative_bestaetigung = None
        self.emergency_stop = None

    def _process_nlu(self,caller_id : int, text : str):
        logging.info(f"Process with nlu : {text}")
        
        #upload file to rasa-service
        url = 'http://127.0.0.1:5005/model/parse'
        resp = requests.post(url=url, json={ "text" : text}).json()

        logging.info(f"Response {resp}")

        intent = resp['intent']['name']
        confidence = resp['intent']['confidence']
        zugnummer = 0
        zuglaufstelle = None
        for item in resp["entities"]:
            if item['entity'] == "zugnummer":
                zugnummer = int(item['value'])
            if item['entity'] == "zuglaufstelle":
                if zuglaufstelle:
                    zuglaufstelle += " " + item['value']
                else:
                    zuglaufstelle = item['value']

        logging.info(f"Intent: {intent} confidence: {confidence} zugnummer: {zugnummer} zuglaufstelle: {zuglaufstelle}")

        model_zuglaufstelle , distance = self.zuglauf_stelle_mapper.map_zuglaufstelle(zuglaufstelle)

        logging.info(f"Mapping: {zuglaufstelle} => {model_zuglaufstelle} Levenshtein: {distance}")
        
        #Fehler
        if intent.startswith("nothalt") and confidence > 0.5:
            self.emergency_stop(EmergencyStopRequest(caller_id=caller_id))
            return

        if zuglaufstelle and not model_zuglaufstelle:
            self.nlu_fehler(Command(caller_id=caller_id,
                                    command_type="ZuglaufstelleNichtBekannt",
                                    text = f"Zuglaufstelle {zuglaufstelle} nicht bekannt. Bitte wiederholen!"))
            return
        
        if intent == "anmeldung" and confidence > 0.5 and zugnummer > 0:
            self.anmeldung(Anmeldung(caller_id=caller_id, zugnummer = zugnummer))
            return

        if intent == "fahranfrage" and confidence > 0.5 and zugnummer > 0 and model_zuglaufstelle:
            #request rasa Service Mock
            self.fahranfrage(Fahranfrage(caller_id=caller_id, zugnummer = zugnummer,meldestelle =model_zuglaufstelle))
            return
        
        if intent == "fahrerlaubnis_wiederholung" and confidence > 0.5 and zugnummer > 0 and model_zuglaufstelle:
            self.fahrbefehl_wdhl(FahrbefehlWiederholung(caller_id=caller_id,zugnummer = zugnummer,meldestelle =model_zuglaufstelle))
            return
        
        if intent == "ankunftsmeldung" and confidence > 0.5 and zugnummer > 0 and model_zuglaufstelle:
            self.ankunftsmeldung(Ankunftsmeldung(caller_id=caller_id,zugnummer = zugnummer,meldestelle =model_zuglaufstelle))
            return
        
        if intent == "positivebestaetigung" and confidence > 0.5:
            self.positive_bestaetigung(PositiveBestaetigung(caller_id=caller_id))
            return

        if intent == "negativebestaetigung" and confidence > 0.5:
            self.negative_bestaetigung(NegativeBestaetigung(caller_id=caller_id))
            return
        
        # Fehler: Anfrage wurde nicht erkannt
        self.nlu_fehler(Command(caller_id=caller_id,
                                command_type="AnfrageNichtErkannt",
                                text = "Anfrage wurde nicht erkannt. Bitte wiederholen!"))

    def set_Anmeldung_callback(self,callback : Callable[[Anmeldung], None]):
        self.anmeldung = callback

    def set_Ankunftsmeldung_callback(self,callback : Callable[[Ankunftsmeldung], None]) -> None: 
        self.ankunftsmeldung = callback

    def set_NegativeBestaetigung_callback(self,callback : Callable[[NegativeBestaetigung], None]) -> None: 
        self.negative_bestaetigung = callback

    def set_PositiveBestaetigung_callback(self,callback : Callable[[PositiveBestaetigung], None]) -> None: 
        self.positive_bestaetigung = callback

    def set_Fahranfrage_callback(self,callback : Callable[[Fahranfrage], None]) -> None: 
        self.fahranfrage = callback
    
    def set_FahrbefehlWiederholung_callback(self,callback : Callable[[FahrbefehlWiederholung], None]) -> None:
        self.fahrbefehl_wdhl = callback

    def set_NLUFehler_callback(self,callback : Callable[[Command], None]) -> None:
        self.nlu_fehler = callback

    def set_EmergencyStopRequest_callback(self,callback : Callable[[EmergencyStopRequest], None]) -> None:
        self.emergency_stop = callback


    def apply_Transcribed(self,transcribed : Transcribed) -> None: 
        threading.Thread(target=self._process_nlu, args=(transcribed.caller_id,transcribed.text)).start()

    def tick(self,_) -> None:
        pass
    