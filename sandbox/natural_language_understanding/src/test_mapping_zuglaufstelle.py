from .mapping_zuglaufstelle import ZuglaufStelleMapper


def test_mapping():

    zuglauf_stelle_mapper = ZuglaufStelleMapper(["Schwarze Pumpe","Boxberg","Cottbus","Cottbus Hauptbahnhof"])

    assert ("Schwarze Pumpe", 0) == zuglauf_stelle_mapper.map_zuglaufstelle("Schwarze Pumpe")
    assert ("Schwarze Pumpe", 1) == zuglauf_stelle_mapper.map_zuglaufstelle("Schwarze Puppe")
    assert ("Schwarze Pumpe", 2) == zuglauf_stelle_mapper.map_zuglaufstelle("Schwarzpump")
    assert ("Schwarze Pumpe", 0) == zuglauf_stelle_mapper.map_zuglaufstelle("Schwarze-Pumpe")
    
    assert ("Boxberg", 0) == zuglauf_stelle_mapper.map_zuglaufstelle("Boxberg")

    assert (None, -1) == zuglauf_stelle_mapper.map_zuglaufstelle("Schwarzer Punkt")

    assert (None, -1) == zuglauf_stelle_mapper.map_zuglaufstelle("Aschaffenburg")
    
    assert ("Cottbus", 3) == zuglauf_stelle_mapper.map_zuglaufstelle("Cottbus Hbf")
    assert ("Cottbus Hauptbahnhof", 0) == zuglauf_stelle_mapper.map_zuglaufstelle("Cottbus Hauptbahnhof")
