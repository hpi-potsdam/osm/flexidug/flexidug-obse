# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.

from abc import ABC, abstractmethod
from typing import Callable
from .Anmeldung import Anmeldung
from .Ankunftsmeldung import Ankunftsmeldung
from .Fahranfrage import Fahranfrage
from .FahrbefehlWiederholung import FahrbefehlWiederholung
from .NegativeBestaetigung import NegativeBestaetigung
from .PositiveBestaetigung import PositiveBestaetigung
from .EmergencyStopRequest import EmergencyStopRequest
from .Command import Command
from .Transcribed import Transcribed

class NaturalLanguageUnderstandingProxyInterface(ABC):

    @abstractmethod
    def set_Anmeldung_callback(self,callback : Callable[[Anmeldung], None]) -> None: pass
    @abstractmethod
    def set_Ankunftsmeldung_callback(self,callback : Callable[[Ankunftsmeldung], None]) -> None: pass
    @abstractmethod
    def set_Fahranfrage_callback(self,callback : Callable[[Fahranfrage], None]) -> None: pass
    @abstractmethod
    def set_FahrbefehlWiederholung_callback(self,callback : Callable[[FahrbefehlWiederholung], None]) -> None: pass
    @abstractmethod
    def set_NegativeBestaetigung_callback(self,callback : Callable[[NegativeBestaetigung], None]) -> None: pass
    @abstractmethod
    def set_PositiveBestaetigung_callback(self,callback : Callable[[PositiveBestaetigung], None]) -> None: pass
    @abstractmethod
    def set_EmergencyStopRequest_callback(self,callback : Callable[[EmergencyStopRequest], None]) -> None: pass
    @abstractmethod
    def set_NLUFehler_callback(self,callback : Callable[[Command], None]) -> None: pass
  
    @abstractmethod
    def apply_Transcribed(self,transcribed : Transcribed) -> None: pass

    @abstractmethod
    def tick(self,cycle) -> None: pass


