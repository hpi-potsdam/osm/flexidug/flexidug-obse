import logging

from Levenshtein import distance as levenshtein_distance


class ZuglaufStelleMapper:

    def __init__(self,candidates,max_distance = 3):
       
        self.candidates = candidates
        logging.info(f"Have {len(self.candidates)} stations")

        self.max_distance = max_distance
        logging.info(f"Maximal Levenshtein distance is {self.max_distance}")

    def _clean(self,word):
        word = word.replace("-"," ").replace(" ","")
        return word.lower()

    def map_zuglaufstelle(self,zuglaufstelle : str):


        best_distance = -1
        best_name = None
        
        if zuglaufstelle:
            for candidate in self.candidates:
                d = levenshtein_distance(self._clean(candidate),self._clean(zuglaufstelle))
                if not best_name or (d < best_distance):
                    if d <= self.max_distance:
                        best_name = candidate
                        best_distance = d
        return best_name, best_distance
               
    
