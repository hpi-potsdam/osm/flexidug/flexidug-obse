
class CallbackMock:
    def __init__(self):
        self.calls_fahranfrage = []

    def fahranfrage(self,fahranfrage):
        self.calls_fahranfrage.append(fahranfrage)

   

def test_fahranfrage_boxberg_schwarze_pumpe():
    from src.NaturalLanguageUnderstandingProxy import NaturalLanguageUnderstandingProxy
    from src.Transcribed import Transcribed
    from src.Fahranfrage import Fahranfrage

    callback = CallbackMock()

    nlu_proxy = NaturalLanguageUnderstandingProxy()
    nlu_proxy.set_Fahranfrage_callback(callback=callback.fahranfrage)



    transcribed = Transcribed(caller_id="test_caller",text = "Darf Zug 101 bis Schwarze Pumpe fahren")
    nlu_proxy.apply_Transcribed(transcribed)

    assert len(callback.calls_fahranfrage) == 1
    assert callback.calls_fahranfrage[0] == Fahranfrage(caller_id="test_caller",zugnummer="101",meldestelle="Schwarze Pumpe")
