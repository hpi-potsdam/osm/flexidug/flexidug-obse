RUN apt-get update && apt-get -y install software-properties-common
COPY data/debian.nonfree.sources /etc/apt/sources.list.d/debian.nonfree.sources
RUN apt-get update && apt-get -y install libttspico-utils
