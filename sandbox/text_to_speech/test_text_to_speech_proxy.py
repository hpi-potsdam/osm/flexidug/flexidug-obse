import time
# Start in cd proxy; python -m texttospeech.test
from src.TextToSpeechProxy import TextToSpeechProxy
from src.Command import Command

def callback(audio):
    print("audio",audio)

proxy = TextToSpeechProxy()

proxy.set_ZLiCAudio_callback(callback)
proxy.apply_Command(Command("xxx","Test","Das ist eine Testnachricht"))

while True:
    print("Sleep")
    time.sleep(1)