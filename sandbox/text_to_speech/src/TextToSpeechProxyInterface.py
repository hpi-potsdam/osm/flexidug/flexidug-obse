# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.

from abc import ABC, abstractmethod
from typing import Callable
from .Audio import Audio
from .EmergencyAudio import EmergencyAudio
from .EmergencyStop import EmergencyStop
from .Command import Command

class TextToSpeechProxyInterface(ABC):

    @abstractmethod
    def set_ZLiCAudio_callback(self,callback : Callable[[Audio], None]) -> None: pass
    @abstractmethod
    def set_ZLiCEmergencyStopAudio_callback(self,callback : Callable[[EmergencyAudio], None]) -> None: pass
  
    @abstractmethod
    def apply_EmergencyStop(self,emergency_stop : EmergencyStop) -> None: pass
    @abstractmethod
    def apply_Command(self,command : Command) -> None: pass
    @abstractmethod
    def apply_NLUFehler(self,command : Command) -> None: pass

    @abstractmethod
    def tick(self,cycle) -> None: pass


