from typing import Callable
import threading
import subprocess
import tempfile
import logging

from .TextToSpeechProxyInterface import TextToSpeechProxyInterface
from .Command import Command
from .Audio import Audio
from .EmergencyStop import EmergencyStop
from .EmergencyAudio import EmergencyAudio
class TextToSpeechProxy(TextToSpeechProxyInterface):

    def __init__(self):
        self.audio = None
        self.emergency_audio = None

    def set_ZLiCAudio_callback(self,callback : Callable[[Audio], None]) -> None:
        self.audio = callback

    def set_ZLiCEmergencyStopAudio_callback(self,callback : Callable[[EmergencyAudio], None]) -> None:
        self.emergency_audio = callback

    def _process_tts(self, caller_id : str, text : str):

        logging.debug(f"convert '{text}' to wav.")

        with tempfile.TemporaryDirectory() as tmpdir_name:
            logging.debug(f"created temporary directory {tmpdir_name}")
            filepath_wav = f"{tmpdir_name}/audio.wav"

            cmd = ['pico2wave', '--lang' , 'de-DE', '--wave' , filepath_wav, text]

            logging.info(f"Convert {text} to {filepath_wav}")
            subprocess.Popen(cmd).wait()

            with open(filepath_wav, 'rb') as f:
                audio = f.read()

                if caller_id is None: #send to all (e.g. emergency stop)
                    self.emergency_audio(EmergencyAudio(audio=audio))
                else:
                    self.audio(Audio(caller_id=caller_id,audio = audio))

    def apply_Command(self,command : Command) -> None:
        logging.info(f"Recv Command caller_id: {command.caller_id} text: '{command.text}'")
        threading.Thread(target=self._process_tts, args=(command.caller_id,command.text,)).start()

    def apply_EmergencyStop(self,emergency_stop : EmergencyStop) -> None:
        logging.info(f"Recv EmergencyStop text: '{emergency_stop.text}'")
        threading.Thread(target=self._process_tts, args=(None,emergency_stop.text,)).start()

    def apply_NLUFehler(self,command : Command) -> None:
        logging.info(f"Recv NLUFehler caller_id: {command.caller_id} text: '{command.text}'")
        threading.Thread(target=self._process_tts, args=(command.caller_id,command.text,)).start()

    def tick(self,_) -> None:
        pass
    