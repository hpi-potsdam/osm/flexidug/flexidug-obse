//webkitURL is deprecated but nevertheless
URL = window.URL || window.webkitURL;

var gumStream; 						//stream from getUserMedia()
var rec; 							//Recorder.js object
var input; 							//MediaStreamAudioSourceNode we'll be recording

// shim for AudioContext when it's not avb. 
var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext //audio context to help us record

var ws = undefined;

function uuidv4() {
	return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
	  (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	);
}

function now() {
	return new Date().toLocaleString();
}

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var caller_id = getParameterByName('caller_id')

var id = uuidv4()
if (caller_id)
	id = caller_id
console.log("Use caller_id: "+id)

function wav_callback(blob) {
	
	var audioTemplateSource = document.getElementById("audio-template").innerHTML;
	var audioTemplate = Handlebars.compile(audioTemplateSource);

	var url = URL.createObjectURL(blob);

	var audio = audioTemplate({"src" : url,  "date" : now() });
	$("#recordingsList").append(audio)
	$("#recordingsList").scrollTop($("#recordingsList")[0].scrollHeight);

	//upload File to server		
	ws.send(blob)

	
	
}


function connect() {
	ws =  new WebSocket(((window.location.protocol === "https:") ? "wss://" : "ws://") + window.location.host + "/ws/"+id);

	ws.onopen = function() {
		console.log("[open] Connection established");
	};
  
	ws.onmessage = async function(e) {
		console.log(`[message] Data received from server: ${e.data}`);
		try 
		{
			//Json
			text = await e.data.text()
			msg = JSON.parse(text)
			if (msg["type"] == "heartbeat")
				return; //ignore
			console.log(msg)
			return
		}
		catch( error ) {}
		
		//binary
		var audioTemplateSource = document.getElementById("audio-template2").innerHTML;
		var audioTemplate = Handlebars.compile(audioTemplateSource);

		var url = URL.createObjectURL(e.data);
		var audio = audioTemplate({"src" : url , "date" : now() });
		$("#recordingsList").append(audio)
		$("#recordingsList").scrollTop($("#recordingsList")[0].scrollHeight);
		
	};
  
	ws.onclose = function(e) {
		if (e.wasClean) {
			console.log(`[close] Connection closed cleanly, code=${e.code} reason=${e.reason}`);
		} else {
			// e.g. server process killed or network down
			// event.code is usually 1006 in this case
			console.log('[close] Connection died');
		}	  setTimeout(function() {
		connect();
	  }, 1000);
	};
  
	ws.onerror = function(err) {
	  console.error('[error] Socket encountered error: ', err.message, 'Closing socket');
	  ws.close();
	};
}

function set_mode(recording) {

	if(!recording) {
		$("#recordButton").find(".fa").removeClass("fa-microphone-slash")
		$("#recordButton").removeClass("red")

		$("#recordButton").find(".fa").addClass("fa-microphone")
		$("#recordButton").addClass("green")

		$("#recordButton").find(".txt").text("Aufnahme starten")

	} else {
		$("#recordButton").find(".fa").removeClass("fa-microphone")
		$("#recordButton").removeClass("green")

		$("#recordButton").find(".fa").addClass("fa-microphone-slash")
		$("#recordButton").addClass("red")

		$("#recordButton").find(".txt").text("Aufnahme stoppen")

	}

}

$( document ).ready(function() {


	//Websocket
	connect()
	
	set_mode(false)

	state = 'Init'

	$("#recordButton").click(function(ev) {
	  ev.preventDefault()
	  console.log( "click" );


	switch (state) {

		case 'Init':

			/*
			Simple constraints object, for more advanced audio features see
			https://addpipe.com/blog/audio-constraints-getusermedia/
			*/
		
			var constraints = { audio: true, video:false }

			/*
				We're using the standard promise based getUserMedia() 
				https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
			*/

			navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
				console.log("getUserMedia() success, stream created, initializing Recorder.js ...");

				/*
					create an audio context after getUserMedia is called
					sampleRate might change after getUserMedia is called, like it does on macOS when recording through AirPods
					the sampleRate defaults to the one set in your OS for your playback device

				*/
				audioContext = new AudioContext();

				//update the format 
				document.getElementById("formats").innerHTML="Format: 1 channel pcm @ "+audioContext.sampleRate/1000+"kHz"

				/*  assign to gumStream for later use  */
				gumStream = stream;
				
				/* use the stream */
				input = audioContext.createMediaStreamSource(stream);

				/* 
					Create the Recorder object and configure to record mono sound (1 channel)
					Recording 2 channels  will double the file size
				*/
				rec = new Recorder(input,{numChannels:1})

				rec.record()

				//change Button classes
				set_mode(true)
			
				state = 'Recording'

			}).catch(function(err) {
				//enable the record button if getUserMedia() fails
				alert(err);
			});

			break;

		case 'Recording':
			//tell the recorder to stop the recording
			rec.stop();

			//change Button classes
			set_mode(false)
			
			state = 'Init'

			//stop microphone access
			gumStream.getAudioTracks()[0].stop();

			//create the wav blob and pass it on to createDownloadLink
			rec.exportWAV(wav_callback);

			break;
	}

	})
  });