# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.

from abc import ABC, abstractmethod
from typing import Callable
from .Audio import Audio
from .EmergencyAudio import EmergencyAudio

class SIPServerProxyInterface(ABC):

    @abstractmethod
    def set_TFAudio_callback(self,callback : Callable[[Audio], None]) -> None: pass
  
    @abstractmethod
    def apply_ZLiCAudio(self,audio : Audio) -> None: pass
    @abstractmethod
    def apply_ZLiCEmergencyStopAudio(self,emergency_audio : EmergencyAudio) -> None: pass

    @abstractmethod
    def tick(self,cycle) -> None: pass


