
import threading
import asyncio
import queue
import logging
import uvicorn

from fastapi import APIRouter
from fastapi import File, UploadFile
from fastapi import FastAPI
from fastapi import APIRouter
from fastapi import WebSocket
from starlette.websockets import WebSocketState, WebSocketDisconnect


from fastapi.staticfiles import StaticFiles


class WebSocketHandler:

    def __init__(self,websocket,caller_id,recv_audio_callback):
        self.websocket = websocket
        self.queue = queue.Queue()
        self.caller_id = caller_id
        self.recv_audio_callback = recv_audio_callback

    async def read_from_socket(self):
        async for data in self.websocket.iter_bytes():
            logging.info(f"Binary data {len(data)}")
            self.recv_audio_callback(self.caller_id,data)

    async def get_data_and_send(self):
        try:
            while self.websocket.client_state == WebSocketState.CONNECTED:
             
                if self.queue.empty():
                    await asyncio.sleep(0.1)
                else:
                    msg = self.queue.get(block=False)
                    logging.info(f"Send Message {len(msg)} via websocket")
                    await self.websocket.send_bytes(msg)
        except WebSocketDisconnect as e:
            logging.error(f"WebSocketDisconnect {e}")


class Server(threading.Thread):

    def __init__(self,host,port,recv_audio_callback):
        super().__init__()
        self.host = host
        self.port = port
        self.router = APIRouter()
        self.router.add_api_route("/health", self._health, methods=["GET"])
        self.router.add_api_websocket_route("/ws/{caller_id}", self._notification)
        self.recv_audio_callback = recv_audio_callback
        self.handlers = set()

    def notify(self,caller_id,msg):
       
        cnt = 0
        for handler in self.handlers:
            if handler.caller_id == caller_id:
                handler.queue.put(msg)
                cnt+= 1

        if cnt == 0:
            logging.error(f"caller_id {caller_id} do not exists anymore")
    
    def notify_all(self,msg):

        for handler in self.handlers:
            handler.queue.put(msg)

    def _health(self):
        return {"Status": "UP"}

    async def _notification(self,websocket: WebSocket,caller_id : str):
        await websocket.accept()
        logging.info(f"Client {caller_id} accepted")

        handler = WebSocketHandler(websocket,caller_id,self.recv_audio_callback)
        self.handlers.add(handler)

        await asyncio.gather(handler.read_from_socket(), handler.get_data_and_send())

        try:
            await websocket.close(code=1001)
        except Exception as e:
            logging.error(f"Exception {e}")

        logging.info(f"Client {caller_id} disconnected")
        self.handlers.remove(handler)

    def run(self):
        app = FastAPI(openapi_url='/openapi.json',)
        app.include_router(self.router)
        app.mount("/", StaticFiles(directory="htdocs",html = True), name="htdocs")

        uvicorn.run(app, host=self.host, port=self.port,ws_ping_interval=5,ws_ping_timeout=5)
 