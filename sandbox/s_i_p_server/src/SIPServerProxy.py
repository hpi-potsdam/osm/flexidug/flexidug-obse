from typing import Callable
import logging
import os
from .SIPServerProxyInterface import SIPServerProxyInterface
from .server import Server
from .Audio import Audio
from .EmergencyAudio import EmergencyAudio


class SIPServerProxy(SIPServerProxyInterface):

    def __init__(self):
        self.tf_audio_callback = None

        server_port = os.environ.get("SERVER_PORT")
        if not server_port:
            logging.error("environment SERVER_PORT must be set. Using default port 8080.")
            server_port = '8080'

        self.server = Server('0.0.0.0', int(server_port), self._recv_audio_callback)
        self.server.start()

    def set_TFAudio_callback(self, callback: Callable[[Audio], None]) -> None:
        self.tf_audio_callback = callback

    def apply_ZLiCAudio(self, audio: Audio) -> None:
        self.server.notify(audio.caller_id, audio.audio)

    def apply_ZLiCEmergencyStopAudio(self,emergency_audio : EmergencyAudio) -> None: 
        self.server.notify_all(emergency_audio.audio)


    def _recv_audio_callback(self, caller_id, audio):
        logging.info(f"recv audio_data:{len(audio)} id:{caller_id}")
        self.tf_audio_callback(Audio(caller_id, audio))

    def tick(self,_) -> None:
        pass
    