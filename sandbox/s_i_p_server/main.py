import time
import os
import logging
from src.SIPServerProxy import SIPServerProxy
from src.Audio import Audio


logging.basicConfig(level=logging.INFO)

def tf_audio_callback(audio : Audio):
    print("audio",len(audio.audio))



proxy = SIPServerProxy()
proxy.set_TFAudio_callback(tf_audio_callback)




try:
    while True:
        time.sleep(10)
        
        try:
            with open("test.wav","br") as f:
                contents = f.read()
                for handler in proxy.server.handlers:
                    audio = Audio(handler.caller_id,contents)
                    proxy.apply_ZLiCAudio(audio)
        except Exception as e:
            print(e)

except:
    os._exit(1)
