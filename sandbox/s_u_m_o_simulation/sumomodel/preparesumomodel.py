from planpro_importer.reader import PlanProReader
from railwayroutegenerator.routegenerator import RouteGenerator
from sumoexporter import SUMOExporter
import os

def prepare_sumo_model():
    # Get the selected PlanPro file
    filename = None
    with open('current_planpro_file.txt') as f:
        lines = f.readlines()
        filename = lines[0].strip()

    if filename is None or not os.path.isfile(filename):
        print("PlanPro-File not found")
        exit(1)

    # Convert it to a yaramo instance
    topology = PlanProReader(filename).read_topology_from_plan_pro_file()
    topology.name = "sumo"

    # Generate routes
    RouteGenerator(topology).generate_routes()

    # Export SUMO model
    sumo_exporter = SUMOExporter(topology)
    sumo_exporter.convert()
    sumo_exporter.write_output()

if __name__ == '__main__':
    prepare_sumo_model()