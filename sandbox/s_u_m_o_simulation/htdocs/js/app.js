$( document ).ready(function() {
	$("#automatic_train_run").click(function(ev){
		ev.preventDefault();
		const config_json = $('textarea#config_json').val();
		$.ajax({
			url: "/run_automatic_trains",
			type: "post", //send it through post method
			data: config_json,
			contentType: 'application/json',
			success: function(response) {
				console.log("successful triggered!",response)
			},
			error: function(xhr) {
				alert(xhr.responseText);			}
		  });
		$(this).addClass("disabled").prop("disabled", true);
		setTimeout(function(){
			$("#automatic_train_run").removeClass("disabled").prop("disabled", false);
		}, 5000);
	})
});