import logging


class RouteManager:

    def __init__(self, datamodel):
        self.datamodel = datamodel

    def get_direction_for_start(self, meldestelle):
        for direction in self.datamodel:
            if self.datamodel[direction][0]['station'] == meldestelle:
                return direction
        
        logging.error(f"no direction reloved for {meldestelle}")
        return None

    def get_first_route_for_start(self, meldestelle: str):
        direction = self.get_direction_for_start(meldestelle)
        return self.datamodel[direction][0]['from_signal'], self.datamodel[direction][0]['to_signal']

    def get_routes_for_meldestelle(self, meldestelle: str, direction: str):
        if direction not in self.datamodel:
            logging.error(f"Direction {direction} not in datamodel.")
            return None

        for entry in self.datamodel[direction]:
            if entry['station'] == meldestelle:
                return [(entry['from_signal'], entry['to_signal'])]

        logging.error(f"Station {meldestelle} not in datamodel.")
        return None

    def get_set_routes_for_strecke(self, meldestelle_from: str, meldestelle_to: str, direction: str):
        if direction not in self.datamodel:
            logging.error(f"Direction {direction} not in datamodel.")
            return None

        routes = []
        start = False
        valid = False
        for entry in self.datamodel[direction]:

            if entry['station'] == meldestelle_from:
                start = True

            if start:
                routes.append((entry['from_signal'], entry['to_signal']))

            if entry['station'] == meldestelle_to:
                if start: 
                    valid = True
                start = False

        if valid:
            return routes
        
        logging.error(f"No valid route {meldestelle_from} -> {meldestelle_to} found in datamodel.")
        return None
