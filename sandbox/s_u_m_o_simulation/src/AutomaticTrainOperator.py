import logging

from .Command import Command
from .model import AutomaticTrain, AutomaticTrainOperation
from .Anmeldung import Anmeldung
from .Ankunftsmeldung import Ankunftsmeldung
from .PositiveBestaetigung import PositiveBestaetigung
from .Fahranfrage import Fahranfrage
from .FahrbefehlWiederholung import FahrbefehlWiederholung
from threading import Thread
from time import sleep
import json

TIME_FOR_VOICE_OUT = 6


class AutomaticTrainOperator(object):

    def __init__(self):
        self.command = None
        self.anmeldung = None
        self.ankunftsmeldung = None
        self.positive_bestaetigung = None
        self.fahranfrage = None
        self.fahrbefehl_wiederholung = None
        self.tick_counter = 0
        self.automatic_trains = []

    def load_automatic_trains_from_config_file(self, config_file: str):
        with open(config_file) as json_data:
            json_object = json.load(json_data)
            self.load_automatic_trains_from_json(json_object)

    def load_automatic_trains_from_json(self, json_object):
        logging.info(json_object)
        for train_id in json_object.keys():
            logging.info(train_id)
            logging.info(json_object[train_id]["create_after"])
            create_after: int = json_object[train_id]["create_after"] + self.tick_counter
            start_position: str = json_object[train_id]["start_position"]
            end_position: str = json_object[train_id]["end_position"]
            intermediate_stops: list = []
            if "intermediate_stops" in json_object[train_id].keys():
                intermediate_stops = json_object[train_id]["intermediate_stops"]
            minimum_stop_time: int = 30
            if "minimum_stop_time" in json_object[train_id].keys():
                minimum_stop_time = json_object[train_id]["minimum_stop_time"]
            logging.info(intermediate_stops)

            self.automatic_trains.append(AutomaticTrain(train_id, create_after, start_position, end_position,
                                                        intermediate_stops, minimum_stop_time))

    def process_register(self, automatic_train: AutomaticTrain, command: Command = None):
        if command is None:
            logging.info(f"Register automatic train {automatic_train.train_id}")
            automatic_train.operation = AutomaticTrainOperation.REGISTRATION_IN_PROGRESS
            registration = Anmeldung("sumo", int(automatic_train.train_id))
            self.voice_out("AnfrageAnmeldung", f"Wird Zug {automatic_train.train_id} angenommen?")
            sleep(TIME_FOR_VOICE_OUT)
            self.anmeldung(registration)
        elif command.command_type == "AnmeldungErfolgreich":
            sleep(TIME_FOR_VOICE_OUT)
            automatic_train.operation = AutomaticTrainOperation.REGISTERED

    def process_locate_train(self, automatic_train: AutomaticTrain, location: str, command: Command = None):
        if command is None:
            logging.info(f"Locate automatic train {automatic_train.train_id}")
            automatic_train.operation = AutomaticTrainOperation.LOCATION_IN_PROGRESS
            self.voice_out("Ankunftsmeldung", f"Zug {automatic_train.train_id} in {location}")
            sleep(TIME_FOR_VOICE_OUT)
            location = Ankunftsmeldung("sumo", int(automatic_train.train_id), location)
            self.ankunftsmeldung(location)
        elif command.command_type == "AnkunftsmeldungWiederholung":
            sleep(TIME_FOR_VOICE_OUT)
            self.voice_out("PositiveBestaetigung", f"Ja, richtig!")
            sleep(TIME_FOR_VOICE_OUT)
            confirmation = PositiveBestaetigung("sumo")
            self.positive_bestaetigung(confirmation)
            sleep(TIME_FOR_VOICE_OUT)
            if automatic_train.has_more_stops():
                if automatic_train.last_stop != automatic_train.start_position:
                    automatic_train.earliest_next_operation = self.tick_counter + automatic_train.minimum_stop_time
                automatic_train.operation = AutomaticTrainOperation.LOCATED
            else:
                logging.info(f"Train {automatic_train.train_id} has reached the end destination.")
                automatic_train.operation = AutomaticTrainOperation.ARRIVED
        elif command.command_type == "AnkunftsmeldungFalschesZiel":
            sleep(TIME_FOR_VOICE_OUT)
            automatic_train.operation = AutomaticTrainOperation.ERROR

    def process_start_train(self, automatic_train: AutomaticTrain, destination: str, command: Command = None):
        if command is None:
            logging.info(f"Ride Request for train {automatic_train.train_id}")
            automatic_train.operation = AutomaticTrainOperation.RIDE_REQUEST_IN_PROGRESS
            self.voice_out("Fahranfrage", f"Darf Zug {automatic_train.train_id} bis {destination} fahren?")
            sleep(TIME_FOR_VOICE_OUT)
            request = Fahranfrage("sumo", int(automatic_train.train_id), destination)
            self.fahranfrage(request)
        elif command.command_type == "FahranfragePositiveAntwort":
            sleep(TIME_FOR_VOICE_OUT)
            self.voice_out("FahrbefehlWiederholung", f"Ich wiederhole: Zug {automatic_train.train_id} darf bis {destination} fahren.")
            sleep(TIME_FOR_VOICE_OUT)
            repeat = FahrbefehlWiederholung("sumo", int(automatic_train.train_id), destination)
            self.fahrbefehl_wiederholung(repeat)
        elif command.command_type == "FahranfrageNegativeAntwort":
            sleep(TIME_FOR_VOICE_OUT)
            automatic_train.earliest_next_operation = (self.tick_counter +
                                                       (automatic_train.delay_next_operation_factor * 10))
            automatic_train.delay_next_operation_factor = automatic_train.delay_next_operation_factor + 1
            automatic_train.operation = AutomaticTrainOperation.LOCATED
        elif command.command_type == "FahranfrageRouteNichtGefunden":
            sleep(TIME_FOR_VOICE_OUT)
            automatic_train.operation = AutomaticTrainOperation.ERROR
        elif command.command_type == "FahranfrageRichtigeBestaetigung":
            sleep(TIME_FOR_VOICE_OUT)
            automatic_train.operation = AutomaticTrainOperation.DRIVING
        elif command.command_type == "FahranfrageFalscheBestaetigung":
            sleep(TIME_FOR_VOICE_OUT)
            automatic_train.earliest_next_operation = (self.tick_counter +
                                                       (automatic_train.delay_next_operation_factor * 10))
            automatic_train.delay_next_operation_factor = automatic_train.delay_next_operation_factor + 1
            automatic_train.operation = AutomaticTrainOperation.LOCATED


    def process_command(self, cmd: Command):
        automatic_train = None
        for auto_train in self.automatic_trains:
            if AutomaticTrainOperation.is_blocking_operation(auto_train.operation):
                automatic_train = auto_train
        if automatic_train is None:
            # Manual train caused this command
            return

        cmd_type = cmd.command_type
        if cmd_type == "AnmeldungErfolgreich":
            Thread(target=lambda: self.process_register(automatic_train, command=cmd)).start()
        elif cmd_type == "AnkunftsmeldungWiederholung":
            Thread(target=lambda: self.process_locate_train(automatic_train,
                                                            automatic_train.last_stop,
                                                            command=cmd)).start()
        elif cmd_type == "FahranfragePositiveAntwort" or cmd_type == "FahranfrageRichtigeBestaetigung":
            Thread(target=lambda: self.process_start_train(automatic_train,
                                                           automatic_train.get_next_stop(),
                                                           command=cmd)).start()

    def voice_out(self, command_type, text):
        cmd = Command(caller_id="sumo", command_type=command_type, text=text)
        self.command(cmd)

    def tick(self):
        self.tick_counter = self.tick_counter + 1

        new_request_possible = True
        for auto_train in self.automatic_trains:
            if AutomaticTrainOperation.is_blocking_operation(auto_train.operation):
                new_request_possible = False
                break

        if new_request_possible:
            for auto_train in self.automatic_trains:
                if auto_train.earliest_next_operation <= self.tick_counter:
                    if auto_train.operation == AutomaticTrainOperation.NOT_REGISTERED:
                        # Create Train
                        Thread(target=self.process_register, args=[auto_train]).start()
                        break
                    if auto_train.operation == AutomaticTrainOperation.REGISTERED:
                        # Locate Train
                        Thread(target=self.process_locate_train, args=[auto_train, auto_train.start_position]).start()
                        break
                    if auto_train.operation == AutomaticTrainOperation.LOCATED:
                        # Ride Request
                        Thread(target=self.process_start_train, args=[auto_train, auto_train.get_next_stop()]).start()
                        break
                    if auto_train.operation == AutomaticTrainOperation.DRIVING and auto_train.is_arrived_at_location():
                        # Locate at end
                        auto_train.reach_next_stop()
                        Thread(target=self.process_locate_train, args=[auto_train, auto_train.last_stop]).start()
                        break


