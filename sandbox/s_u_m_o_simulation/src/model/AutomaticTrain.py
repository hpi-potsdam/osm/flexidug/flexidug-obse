from .Train import Train
from .AutomaticTrainOperation import AutomaticTrainOperation


class AutomaticTrain(Train):

    def __init__(self, train_id: str, earliest_next_operation: int, start_position: str, end_position: str,
                 intermediate_stops: list, minimum_stop_time: int):
        super().__init__(train_id)
        self.earliest_next_operation = earliest_next_operation
        self.delay_next_operation_factor = 1
        self.operation = AutomaticTrainOperation.NOT_REGISTERED
        self.start_position = start_position
        self.end_position = end_position
        self.last_stop = start_position
        self.intermediate_stops = intermediate_stops
        self.next_stop_counter = 0
        self.minimum_stop_time = minimum_stop_time

    def is_automatic_train(self):
        return True

    def get_next_stop(self):
        if self.next_stop_counter < len(self.intermediate_stops):
            return self.intermediate_stops[0]
        return self.end_position

    def reach_next_stop(self):
        if self.next_stop_counter < len(self.intermediate_stops):
            self.last_stop = self.intermediate_stops[self.next_stop_counter]
        else:
            self.last_stop = self.end_position
        self.next_stop_counter = self.next_stop_counter + 1

    def has_more_stops(self):
        return self.next_stop_counter < len(self.intermediate_stops) + 1  # +1 for the end position