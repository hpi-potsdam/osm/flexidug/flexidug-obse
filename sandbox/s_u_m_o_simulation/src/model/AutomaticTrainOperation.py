from enum import Enum


class AutomaticTrainOperation(Enum):
    NOT_REGISTERED = 0
    REGISTRATION_IN_PROGRESS = 1
    REGISTERED = 2
    LOCATION_IN_PROGRESS = 10
    LOCATED = 11
    ARRIVED = 12
    RIDE_REQUEST_IN_PROGRESS = 20
    DRIVING = 30
    ERROR = 90

    @classmethod
    def is_blocking_operation(cls, operation):
        return (operation == cls.REGISTRATION_IN_PROGRESS or operation == cls.LOCATION_IN_PROGRESS or
                operation == cls.RIDE_REQUEST_IN_PROGRESS)

