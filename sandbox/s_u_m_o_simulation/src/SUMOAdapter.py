import traci
import logging
import re
import os
import yaml
from .route_manager import RouteManager
from .model import Train, AutomaticTrain
from .AutomaticTrainOperator import AutomaticTrainOperator


class SUMOAdapter(object):

    def __init__(self, automatic_train_operator: AutomaticTrainOperator):
        self.registered_trains: list[Train] = []
        self.automatic_train_operator = automatic_train_operator

        dirname = os.path.dirname(__file__)
        data_model_file = os.path.join(dirname, '../datamodel/model.yml')
        with open(data_model_file, "r", encoding='UTF-8') as f:
            data_model = yaml.safe_load(f)
            if 'tracks' not in data_model:
                raise ValueError(f"routes have to be defined in {data_model_file}")
            if len(data_model['tracks']) != 1:
                raise ValueError(f"implementation only can handle one (not {len(data_model['tracks'])}) tracks")
            track = data_model['tracks'][0]
            self.route_manager = RouteManager(track['routes'])

    def connect(self):
        traci.init(port=9090)
        traci.setOrder(1)
        logging.info("TraCi Connected")

    def register_train(self, train_id: str):
        # If the train is already registered, do nothing
        if train_id in map(lambda t: t.train_id, self.registered_trains):
            logging.warning(f"Train {train_id} was already created.")
            return

        train_obj = None
        for auto_train in self.automatic_train_operator.automatic_trains:
            if auto_train.train_id == train_id:
                train_obj = auto_train
        if train_obj is None:
            train_obj = Train(train_id)

        self.registered_trains.append(train_obj)

    def set_current_train_position(self, train_id: str, current_position: str):
        train = next((t for t in self.registered_trains if t.train_id == train_id), None)
        if train is None:
            logging.error(f"Train with the id {train_id} not found!")

        train.next_start_position = current_position

        if not train.created_in_sumo:
            self._add_train(train, current_position)

    def _add_train(self, train: Train, position: str):
        # Convert position to SUMO edge id, to get the route
        logging.info(f"Create train {train.train_id} at position {position}")
        train.direction = self.route_manager.get_direction_for_start(position)
        signal_tuple = self.route_manager.get_first_route_for_start(position)

        # Create train in SUMO
        traci.vehicle.add(train.train_id, f"route_{signal_tuple[0]}-{signal_tuple[1]}", "regio")
        train.created_in_sumo = True
        train.stop_train()
        traci.vehicle.setSpeedMode(train.train_id, 32)

    def set_route_for_train(self, train_id: str, target_position: str):
        # Get route on basis of the current and the target position
        train = next((t for t in self.registered_trains if t.train_id == train_id), None)
        routes = self.route_manager.get_set_routes_for_strecke(train.next_start_position, target_position,
                                                               train.direction)

        train.set_sumo_route(routes[0])
        train.current_route = routes
        train.last_start_position = train.next_start_position
        train.next_start_position = target_position
        train.set_speed(13.944)  # 6.944

    def emergency_stop(self):
        for train in self.registered_trains:
            if train.created_in_sumo:
                train.stop_train()

    def tick(self):
        traci.simulationStep()

        for train in self.registered_trains:
            if train.created_in_sumo:
                if train.is_on_last_edge_of_route():
                    matcher = re.match(r'route_(.+)-(.+)', train.get_current_sumo_route())
                    current_signal_tuple = (matcher.group(1), matcher.group(2))
                    routes = train.current_route
                    index_of_tuple = routes.index(current_signal_tuple)
                    if index_of_tuple < len(routes)-1:
                        train.set_sumo_route(routes[index_of_tuple+1])
                        train.set_speed(13.944)  # 6.944
