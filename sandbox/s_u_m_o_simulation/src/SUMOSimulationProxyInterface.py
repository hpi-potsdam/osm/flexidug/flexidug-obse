# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.

from abc import ABC, abstractmethod
from typing import Callable
from .Anmeldung import Anmeldung
from .Ankunftsmeldung import Ankunftsmeldung
from .Fahranfrage import Fahranfrage
from .FahrbefehlWiederholung import FahrbefehlWiederholung
from .NegativeBestaetigung import NegativeBestaetigung
from .PositiveBestaetigung import PositiveBestaetigung
from .EmergencyStopControl import EmergencyStopControl
from .Command import Command

class SUMOSimulationProxyInterface(ABC):

    @abstractmethod
    def set_Anmeldung_callback(self,callback : Callable[[Anmeldung], None]) -> None: pass
    @abstractmethod
    def set_Ankunftsmeldung_callback(self,callback : Callable[[Ankunftsmeldung], None]) -> None: pass
    @abstractmethod
    def set_Fahranfrage_callback(self,callback : Callable[[Fahranfrage], None]) -> None: pass
    @abstractmethod
    def set_FahrbefehlWiederholung_callback(self,callback : Callable[[FahrbefehlWiederholung], None]) -> None: pass
    @abstractmethod
    def set_NegativeBestaetigung_callback(self,callback : Callable[[NegativeBestaetigung], None]) -> None: pass
    @abstractmethod
    def set_PositiveBestaetigung_callback(self,callback : Callable[[PositiveBestaetigung], None]) -> None: pass
    @abstractmethod
    def set_EmergencyStopControl_callback(self,callback : Callable[[EmergencyStopControl], None]) -> None: pass
    @abstractmethod
    def set_Command_callback(self,callback : Callable[[Command], None]) -> None: pass
  
    @abstractmethod
    def apply_Anmeldung(self,anmeldung : Anmeldung) -> None: pass
    @abstractmethod
    def apply_Ankunftsmeldung(self,ankunftsmeldung : Ankunftsmeldung) -> None: pass
    @abstractmethod
    def apply_Fahranfrage(self,fahranfrage : Fahranfrage) -> None: pass
    @abstractmethod
    def apply_FahrbefehlWiederholung(self,fahrbefehl_wiederholung : FahrbefehlWiederholung) -> None: pass
    @abstractmethod
    def apply_NegativeBestaetigung(self,negative_bestaetigung : NegativeBestaetigung) -> None: pass
    @abstractmethod
    def apply_PositiveBestaetigung(self,positive_bestaetigung : PositiveBestaetigung) -> None: pass
    @abstractmethod
    def apply_EmergencyStopControl(self,emergency_stop_control : EmergencyStopControl) -> None: pass
    @abstractmethod
    def apply_Command(self,command : Command) -> None: pass

    @abstractmethod
    def tick(self,cycle) -> None: pass


