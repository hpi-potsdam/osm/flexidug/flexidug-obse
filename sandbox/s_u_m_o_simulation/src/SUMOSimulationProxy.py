# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.
import logging
import os
from typing import Callable
import yaml

from .Anmeldung import Anmeldung
from .Ankunftsmeldung import Ankunftsmeldung
from .EmergencyStopControl import EmergencyStopControl
from .Fahranfrage import Fahranfrage
from .FahrbefehlWiederholung import FahrbefehlWiederholung
from .NegativeBestaetigung import NegativeBestaetigung
from .PositiveBestaetigung import PositiveBestaetigung
from .Command import Command
from .SUMOSimulationProxyInterface import SUMOSimulationProxyInterface
from .SUMOAdapter import SUMOAdapter
from .AutomaticTrainOperator import AutomaticTrainOperator
from zlicutils.server import Server
from time import sleep
from threading import Thread


def str2bool(v):
    return v.lower() in ("yes", "true", "1")


class SUMOSimulationProxy(SUMOSimulationProxyInterface):

    def __init__(self):
        self.anmeldung = None
        self.ankunftsmeldung = None
        self.fahranfrage = None
        self.fahrbefehlwiederholung = None
        self.positivebestaetigung = None
        self.negativebestaetigung = None
        self.command = None
        self.emergencystopcontrol = None

        self.last_Ankunftsmeldung = None
        self.last_FahrbefehlWiederholung = None

        automatic_trains_config_file = None
        dirname = os.path.dirname(__file__)
        data_model_file = os.path.join(dirname, '../datamodel/model.yml')
        with open(data_model_file, "r", encoding='UTF-8') as f:
            data_model = yaml.safe_load(f)
            if 'automatic_trains_model_file' not in data_model:
                raise ValueError(f"automatic_trains_model_file has to be defined in {data_model_file}")
            automatic_trains_config_file = os.path.join(dirname, f"../{data_model['automatic_trains_model_file']}")
            if 'automatic_trains_autoplay' not in data_model:
                raise ValueError(f"automatic_trains_autoplay has to be defined in {data_model_file}")
            automatic_trains_autoplay = str2bool(data_model['automatic_trains_autoplay'])

        self.automatic_train_operator: AutomaticTrainOperator = AutomaticTrainOperator()
        self.automatic_train_operator.anmeldung = self.apply_Anmeldung
        self.automatic_train_operator.ankunftsmeldung = self.apply_Ankunftsmeldung
        self.automatic_train_operator.positive_bestaetigung = self.apply_PositiveBestaetigung
        self.automatic_train_operator.fahranfrage = self.apply_Fahranfrage
        self.automatic_train_operator.fahrbefehl_wiederholung = self.apply_FahrbefehlWiederholung
        if automatic_trains_autoplay:
            logging.info(f"Load automatic trains from {automatic_trains_config_file}")
            if not os.path.isfile(automatic_trains_config_file):
                raise ValueError(f"automatic trains file not found {automatic_trains_config_file}")
            self.automatic_train_operator.load_automatic_trains_from_config_file(automatic_trains_config_file)

        server_port = os.environ.get("WEBSERVER_PORT")
        if not server_port:
            logging.error("environment WEBSERVER_PORT must be set. Using default port 8080.")
            server_port = '8080'

        self.server = Server(host='0.0.0.0', port=int(server_port))
        self.server.router.add_api_route("/run_automatic_trains", self._run_automatic_trains, methods=["POST"])
        self.server.start()

        self.sumo_adapter = SUMOAdapter(self.automatic_train_operator)
        self.sumo_adapter.connect()

    def set_Anmeldung_callback(self, callback: Callable[[Anmeldung], None]) -> None:
        self.anmeldung = callback

    def set_Ankunftsmeldung_callback(self, callback: Callable[[Ankunftsmeldung], None]) -> None:
        self.ankunftsmeldung = callback

    def set_Fahranfrage_callback(self, callback: Callable[[Fahranfrage], None]) -> None:
        self.fahranfrage = callback
    
    def set_FahrbefehlWiederholung_callback(self, callback: Callable[[FahrbefehlWiederholung], None]) -> None:
        self.fahrbefehlwiederholung = callback

    def set_NegativeBestaetigung_callback(self, callback: Callable[[NegativeBestaetigung], None]) -> None:
        self.negativebestaetigung = callback

    def set_PositiveBestaetigung_callback(self,callback : Callable[[PositiveBestaetigung], None]) -> None:
        self.positivebestaetigung = callback

    def set_Command_callback(self,callback : Callable[[Command], None]) -> None:
        self.command = callback
        self.automatic_train_operator.command = callback

    def set_EmergencyStopControl_callback(self, callback: Callable[[EmergencyStopControl], None]) -> None:
        self.emergencystopcontrol = callback

    def apply_Anmeldung(self,anmeldung : Anmeldung) -> None:
        logging.info(anmeldung)
        self.anmeldung(anmeldung)
        self.sumo_adapter.register_train(str(anmeldung.zugnummer))

    def apply_Ankunftsmeldung(self,ankunftsmeldung : Ankunftsmeldung) -> None:
        logging.info(ankunftsmeldung)
        self.ankunftsmeldung(ankunftsmeldung)
        self.last_Ankunftsmeldung = ankunftsmeldung

    def apply_Fahranfrage(self,fahranfrage : Fahranfrage) -> None:
        logging.info(fahranfrage)
        self.fahranfrage(fahranfrage)
    
    def apply_FahrbefehlWiederholung(self,fahrbefehl_wiederholung : FahrbefehlWiederholung) -> None:
        logging.info(fahrbefehl_wiederholung)
        self.fahrbefehlwiederholung(fahrbefehl_wiederholung)
        self.last_FahrbefehlWiederholung = fahrbefehl_wiederholung

    def apply_NegativeBestaetigung(self,negative_bestaetigung : NegativeBestaetigung) -> None:
        logging.info(negative_bestaetigung)
        self.negativebestaetigung(negative_bestaetigung)
    
    def apply_PositiveBestaetigung(self,positive_bestaetigung : PositiveBestaetigung) -> None:
        logging.info(positive_bestaetigung)
        self.positivebestaetigung(positive_bestaetigung)
        self.sumo_adapter.set_current_train_position(str(self.last_Ankunftsmeldung.zugnummer), self.last_Ankunftsmeldung.meldestelle)

    def apply_Command(self, command: Command) -> None:
        logging.info(f"COMMAND: {command}")
        self.command(command)
        self.automatic_train_operator.process_command(command)

        if command.command_type == "FahranfrageRichtigeBestaetigung":
            def drive_train():
                sleep(5)
                self.sumo_adapter.set_route_for_train(str(self.last_FahrbefehlWiederholung.zugnummer),
                                                      self.last_FahrbefehlWiederholung.meldestelle)
                self.last_FahrbefehlWiederholung = None
            Thread(target=lambda: drive_train()).start()

    def apply_EmergencyStopControl(self, emergency_stop_control: EmergencyStopControl) -> None:
        logging.info(emergency_stop_control)
        self.sumo_adapter.emergency_stop()
        self.emergencystopcontrol(emergency_stop_control)

    def _run_automatic_trains(self, json_str: dict):
        logging.info(f"JSON STR {json_str}")
        self.automatic_train_operator.load_automatic_trains_from_json(json_str)
    
    def tick(self, cycle) -> None:
        self.sumo_adapter.tick()
        self.automatic_train_operator.tick()
