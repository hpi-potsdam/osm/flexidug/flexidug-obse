# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.

from abc import ABC, abstractmethod
from typing import Callable
from .Audio import Audio
from .Transcribed import Transcribed
from .EmergencyStopControl import EmergencyStopControl
from .SetRoute import SetRoute
from .FreeRoute import FreeRoute
from .SetRouteResponse import SetRouteResponse
from .FreeRouteResponse import FreeRouteResponse
from .Command import Command
from .EmergencyAudio import EmergencyAudio
from .Anmeldung import Anmeldung
from .Ankunftsmeldung import Ankunftsmeldung
from .Fahranfrage import Fahranfrage
from .FahrbefehlWiederholung import FahrbefehlWiederholung
from .NegativeBestaetigung import NegativeBestaetigung
from .PositiveBestaetigung import PositiveBestaetigung
from .EmergencyStopRequest import EmergencyStopRequest
from .EmergencyStop import EmergencyStop

class TracingProxyInterface(ABC):

  
    @abstractmethod
    def apply_TFAudio(self,audio : Audio) -> None: pass
    @abstractmethod
    def apply_Transcribed(self,transcribed : Transcribed) -> None: pass
    @abstractmethod
    def apply_EmergencyStopControl(self,emergency_stop_control : EmergencyStopControl) -> None: pass
    @abstractmethod
    def apply_SetRoute(self,set_route : SetRoute) -> None: pass
    @abstractmethod
    def apply_FreeRoute(self,free_route : FreeRoute) -> None: pass
    @abstractmethod
    def apply_SetRouteResponse(self,set_route_response : SetRouteResponse) -> None: pass
    @abstractmethod
    def apply_FreeRouteResponse(self,free_route_response : FreeRouteResponse) -> None: pass
    @abstractmethod
    def apply_Command(self,command : Command) -> None: pass
    @abstractmethod
    def apply_NLUFehler(self,command : Command) -> None: pass
    @abstractmethod
    def apply_ZLiCAudio(self,audio : Audio) -> None: pass
    @abstractmethod
    def apply_ZLiCEmergencyStopAudio(self,emergency_audio : EmergencyAudio) -> None: pass
    @abstractmethod
    def apply_Anmeldung(self,anmeldung : Anmeldung) -> None: pass
    @abstractmethod
    def apply_Ankunftsmeldung(self,ankunftsmeldung : Ankunftsmeldung) -> None: pass
    @abstractmethod
    def apply_Fahranfrage(self,fahranfrage : Fahranfrage) -> None: pass
    @abstractmethod
    def apply_FahrbefehlWiederholung(self,fahrbefehl_wiederholung : FahrbefehlWiederholung) -> None: pass
    @abstractmethod
    def apply_NegativeBestaetigung(self,negative_bestaetigung : NegativeBestaetigung) -> None: pass
    @abstractmethod
    def apply_PositiveBestaetigung(self,positive_bestaetigung : PositiveBestaetigung) -> None: pass
    @abstractmethod
    def apply_EmergencyStopRequest(self,emergency_stop_request : EmergencyStopRequest) -> None: pass
    @abstractmethod
    def apply_EmergencyStop(self,emergency_stop : EmergencyStop) -> None: pass

    @abstractmethod
    def tick(self,cycle) -> None: pass


