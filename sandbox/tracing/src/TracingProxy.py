# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.
import logging
import os
import uuid
import tempfile
from datetime import datetime
from zlicutils.server import Server

from fastapi.responses import StreamingResponse
from io import BytesIO

from .Audio import Audio
from .Transcribed import Transcribed
from .EmergencyStopControl import EmergencyStopControl
from .SetRoute import SetRoute
from .FreeRoute import FreeRoute
from .SetRouteResponse import SetRouteResponse
from .FreeRouteResponse import FreeRouteResponse
from .Command import Command
from .EmergencyAudio import EmergencyAudio
from .Anmeldung import Anmeldung
from .Ankunftsmeldung import Ankunftsmeldung
from .Fahranfrage import Fahranfrage
from .FahrbefehlWiederholung import FahrbefehlWiederholung
from .NegativeBestaetigung import NegativeBestaetigung
from .PositiveBestaetigung import PositiveBestaetigung
from .EmergencyStopRequest import EmergencyStopRequest
from .EmergencyStop import EmergencyStop



from .TracingProxyInterface import TracingProxyInterface


class TracingProxy(TracingProxyInterface):

    def __init__(self, with_server=True):

        self.protocol = []
        self.tempfolder = tempfile.TemporaryDirectory()
        if with_server:
            server_port = os.environ.get("SERVER_PORT")
            if not server_port:
                logging.error("environment SERVER_PORT must be set. Using default port 8080.")
                server_port = '8080'

            self.server = Server(host='0.0.0.0', port=int(server_port))
            self.server.router.add_api_route("/protocol", self._protocol, methods=["GET"])
            self.server.router.add_api_route("/audio/{audio_file}", self._audio, methods=["GET"])

            self.server.start()
        
    def __del__(self):
        self.tempfolder.cleanup()

    async def _protocol(self):
        return self.protocol
    
    async def _audio(self, audio_file):

        with open(f"{self.tempfolder.name}/{audio_file}", "rb") as bf:
            return StreamingResponse(BytesIO(bf.read()), media_type="audio/wav")

    def _add_message(self, message_name: str, caller_id: str = None, text: str = None, meldestelle: str = None, zugnummer: int = 0, command_type=None, command: str = None, audio: bytes = None) -> None:
        logging.info(f"{message_name} caller_id={caller_id} text={text} meldestelle={meldestelle} zugnummer={zugnummer} command_type={command_type} command={command} audio={len(audio or '')}")

        protocol_entry = {
            "message_name": message_name,
            "summary": f"{message_name} caller_id={caller_id} text={text} meldestelle={meldestelle} zugnummer={zugnummer} command_type={command_type} command={command} audio={len(audio or '')}",
            "date": datetime.now()
        }

        if caller_id:
            protocol_entry["caller_id"] = caller_id
        if text:
            protocol_entry["text"] = text
        if meldestelle:
            protocol_entry["meldestelle"] = meldestelle
        if zugnummer > 0:
            protocol_entry["zugnummer"] = zugnummer
        if command:
            protocol_entry["command"] = command
        # persist Audio to deliver at API
        if audio:
            audio_file = f"{uuid.uuid4()}.wav"
            protocol_entry["audio"] = audio_file

            with open(f"{self.tempfolder.name}/{audio_file}", "wb") as bf:
                bf.write(audio)

        self.protocol.append(protocol_entry)

    def apply_TFAudio(self, audio: Audio) -> None:
        self._add_message("TFAudio", caller_id=audio.caller_id, audio=audio.audio)

    def apply_Transcribed(self, transcribed: Transcribed) -> None:
        self._add_message("Transcribed", caller_id=transcribed.caller_id, text=transcribed.text)

    def apply_EmergencyStopControl(self, emergency_stop_control: EmergencyStopControl) -> None:
        self._add_message("EmergencyStopControl", command=emergency_stop_control.command)

    def apply_SetRoute(self, set_route: SetRoute) -> None:
        self._add_message("SetRoute", zugnummer=set_route.zugnummer, text=f"{set_route.routes}")

    def apply_FreeRoute(self, free_route: FreeRoute) -> None:
        self._add_message("FreeRoute", zugnummer=free_route.zugnummer, text=f"{free_route.routes}")

    def apply_SetRouteResponse(self, set_route_response: SetRouteResponse) -> None:
        self._add_message("SetRouteResponse", zugnummer=set_route_response.zugnummer, text=f"Status {set_route_response.status}")

    def apply_FreeRouteResponse(self, free_route_response: FreeRouteResponse) -> None:
        self._add_message("FreeRouteResponse", zugnummer=free_route_response.zugnummer, text=f"Status {free_route_response.status}")

    def apply_Command(self, command: Command) -> None:
        self._add_message("Command", caller_id=command.caller_id, command_type=command.command_type, text=command.text)

    def apply_NLUFehler(self, command: Command) -> None:
        self._add_message("NLUFehler", caller_id=command.caller_id, text=command.text)

    def apply_ZLiCAudio(self, audio: Audio) -> None:
        self._add_message("ZLiCAudio", caller_id=audio.caller_id, audio=audio.audio)

    def apply_ZLiCEmergencyStopAudio(self, emergency_audio: EmergencyAudio) -> None:
        self._add_message("ZLiCEmergencyStopAudio", audio=emergency_audio.audio)

    def apply_Anmeldung(self, anmeldung: Anmeldung) -> None:
        self._add_message("Anmeldung", caller_id=anmeldung.caller_id, zugnummer=anmeldung.zugnummer)

    def apply_Ankunftsmeldung(self, ankunftsmeldung: Ankunftsmeldung) -> None:
        self._add_message("Ankunftsmeldung", caller_id=ankunftsmeldung.caller_id, zugnummer=ankunftsmeldung.zugnummer, meldestelle=ankunftsmeldung.meldestelle)

    def apply_Fahranfrage(self, fahranfrage: Fahranfrage) -> None:
        self._add_message("Fahranfrage", caller_id=fahranfrage.caller_id, zugnummer=fahranfrage.zugnummer, meldestelle=fahranfrage.meldestelle)

    def apply_FahrbefehlWiederholung(self, fahrbefehl_wiederholung: FahrbefehlWiederholung) -> None:
        self._add_message("FahrbefehlWiederholung", caller_id=fahrbefehl_wiederholung.caller_id, zugnummer=fahrbefehl_wiederholung.zugnummer, meldestelle=fahrbefehl_wiederholung.meldestelle)

    def apply_NegativeBestaetigung(self, negative_bestaetigung: NegativeBestaetigung) -> None:
        self._add_message("NegativeBestaetigung", caller_id=negative_bestaetigung.caller_id)

    def apply_PositiveBestaetigung(self, positive_bestaetigung: PositiveBestaetigung) -> None:
        self._add_message("PositiveBestaetigung", caller_id=positive_bestaetigung.caller_id)

    def apply_EmergencyStopRequest(self, emergency_stop_request: EmergencyStopRequest) -> None:
        self._add_message("EmergencyStopRequest", caller_id=emergency_stop_request.caller_id)

    def apply_EmergencyStop(self, _emergency_stop: EmergencyStop) -> None:
        self._add_message("EmergencyStop")

    def tick(self, cycle) -> None:
        pass
