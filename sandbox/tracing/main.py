import logging
import time
import os
from datetime import datetime, timedelta

from src.TracingProxy import TracingProxy
from src.Audio import Audio
from src.Ankunftsmeldung import Ankunftsmeldung
from src.Command import Command
from src.EmergencyStopControl import EmergencyStopControl

logging.basicConfig(level=logging.INFO)


proxy = TracingProxy()

next_request = datetime.now() + timedelta(seconds=10)
try:
    while True:
        time.sleep(1)
        proxy.tick(1)

        if next_request < datetime.now():
            next_request = datetime.now() + timedelta(seconds=60)
            proxy.apply_TFAudio(Audio(caller_id="xxx", audio=b'hello world'))
            proxy.apply_Ankunftsmeldung(Ankunftsmeldung(caller_id="xxx", meldestelle="Jöhstadt", zugnummer=101))
            proxy.apply_Command(Command(caller_id="xxx",
                                        command_type="AlleFahrtenAnhalten",
                                        text="Alle Fahrten anhalten"))
            proxy.apply_EmergencyStopControl(EmergencyStopControl(command="disable"))

except Exception as e:
    print(e)
    os._exit(1)
