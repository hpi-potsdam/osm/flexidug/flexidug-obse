


function get_protocol() {

	$.get( "/protocol", function( data ) {
		

		console.log(data)

		let entry_template = document.querySelector("#entry-template").innerHTML;
		let template= Handlebars.compile(entry_template);

		for(var i = 0;i < data.length;i++) {
			entry_id = 'entry'+i

			//Check if exists
			if($("#" + entry_id).length == 0) {

				//it doesn't exist
				template_data = {
					'id': entry_id,
					'date': moment(data[i].date).format("DD.MM.YYYY HH:mm:ss"),
					'message_name': data[i].message_name,
					'summary': data[i].summary
				}
				
				if ('audio' in data[i])
					template_data['audio'] = data[i].audio

				if ('text' in data[i])
					template_data['text'] = data[i].text

				if ('command' in data[i])
					template_data['command'] = data[i].command

				if ('zugnummer' in data[i])
					template_data['zugnummer'] = data[i].zugnummer

				if ('meldestelle' in data[i])
					template_data['meldestelle'] = data[i].meldestelle


				entry = template(template_data)

				$("#protocol_table").find('tbody').append(entry)
			}

			
		}


	});
	setTimeout(function() {
		get_protocol();
	}, 1000);

}




$( document ).ready(function() {

	get_protocol()
});