# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.

from abc import ABC, abstractmethod
from typing import Callable
from .Transcribed import Transcribed
from .Audio import Audio

class SpeechToTextProxyInterface(ABC):

    @abstractmethod
    def set_Transcribed_callback(self,callback : Callable[[Transcribed], None]) -> None: pass
  
    @abstractmethod
    def apply_TFAudio(self,audio : Audio) -> None: pass

    @abstractmethod
    def tick(self,cycle) -> None: pass


