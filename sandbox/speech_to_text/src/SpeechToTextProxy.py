from typing import Callable
from .SpeechToTextProxyInterface import SpeechToTextProxyInterface
import whisper
import threading
import tempfile
import logging
from .Audio import Audio
from .Transcribed import Transcribed

class SpeechToTextProxy(SpeechToTextProxyInterface):

    def __init__(self):
        self.model = whisper.load_model("small",download_root="model")
        self.transcribed = None

    def set_Transcribed_callback(self,callback : Callable[[Transcribed], None]) -> None: 
        self.transcribed = callback

    def apply_TFAudio(self,audio : Audio) -> None:
        self._process_stt(audio.caller_id, audio.audio)
        # Remove threading at the moment to run at macOS
        # threading.Thread(target=self._process_stt, args=(audio.caller_id,audio.audio,)).start()

    def _process_stt(self, caller_id : str, audio : bytes):
        
        with tempfile.NamedTemporaryFile(suffix='.wav') as t:
            filepath = t.name
           
            with open(filepath, 'wb') as f:
                f.write(audio)
            
            result = self.model.transcribe(filepath)
            logging.info(result)
            text = result['text']
            self.transcribed(Transcribed(caller_id=caller_id,text = text))

    def tick(self,_) -> None:
        pass
