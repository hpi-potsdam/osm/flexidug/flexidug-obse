from typing import Callable
from .TextToSpeechProxyInterface import TextToSpeechProxyInterface
import threading
import boto3
import os
import tempfile
from pydub import AudioSegment
from .Command import Command
from .Audio import Audio

class TextToSpeechProxy(TextToSpeechProxyInterface):

    def __init__(self):
        self.audio = None

    def set_ZLiCAudio_callback(self,callback : Callable[[Audio], None]) -> None: 
        self.audio = callback

    def _process_tts(self, caller_id : str, text : str):

        polly_client = boto3.client('polly')

        print("request")
        response = polly_client.synthesize_speech(VoiceId='Vicki', #Marlene, Vicki, Hans
                    OutputFormat='mp3', 
                    Text = text,
                    TextType = 'text',
                    Engine = 'neural')
        print("response")

        contents = response['AudioStream'].read()

        #convert mp3 to wav
     
        with tempfile.TemporaryDirectory() as tmpdir_name:
            print('created temporary directory', tmpdir_name)
            filepath_mp3 = os.path.join(tmpdir_name,"audio.mp3")
            filepath_wav = f"{tmpdir_name}/audio.wav"
            
            with open(filepath_mp3, 'wb') as f:
                f.write(contents)

            # convert mp3 to wav 
            sound = AudioSegment.from_mp3(filepath_mp3)
            sound.export(filepath_wav, format="wav")
        
            with open(filepath_wav, 'rb') as f:
                audio = f.read()
                self.audio(Audio(caller_id=caller_id,audio = audio))

    def apply_Command(self,command : Command) -> None: 
        threading.Thread(target=self._process_tts, args=(command.caller_id,command.text,)).start()

    def apply_NLUFehler(self,command : Command) -> None: 
        threading.Thread(target=self._process_tts, args=(command.caller_id,command.text,)).start()

    def tick(self,_) -> None:
        pass
