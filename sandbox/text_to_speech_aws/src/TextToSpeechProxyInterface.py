# This file has been automatically generated. Any changes will be overwritten on the next generation of the file.

from abc import ABC, abstractmethod
from typing import Callable
from .Command import Command
from .Audio import Audio

class TextToSpeechProxyInterface(ABC):

    @abstractmethod
    def set_ZLiCAudio_callback(self,callback : Callable[[Audio], None]) -> None: pass
  
    @abstractmethod
    def apply_Command(self,command : Command) -> None: pass
    @abstractmethod
    def apply_NLUFehler(self,command : Command) -> None: pass



