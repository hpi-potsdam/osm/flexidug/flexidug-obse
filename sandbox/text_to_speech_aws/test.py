import time
# Start in cd proxy; python -m texttospeech.test
from src.TextToSpeechProxy import TextToSpeechProxy

def callback(id,audio):
    print("id",id,"audio",audio)

proxy = TextToSpeechProxy()

proxy.set_ZLiCAudio_callback(callback)
proxy.apply_Command("Das ist eine Testnachricht")

while True:
    print("Sleep")
    time.sleep(1)