# Flexidug Ontology Based System Engineering


## Requirements

https://github.com/dfriedenberger/obse
https://github.com/dfriedenberger/rdf2puml
https://github.com/dfriedenberger/generators

Abhänigkeiten können mit `pip` installiert werden.
Ein virtual environment ist zu empfehlen.
```bash
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```
Für PowerShell/CMD (Windows) oder csh/fish: https://docs.python.org/3/library/venv.html#how-venvs-work

### Plantuml

Der create model process erzeugt puml files. Diese könne mit plantuml in png's ode svg's umgewandelt werden. Dafür wird eine Java Installation und das aktuelle jar file benötigt.

https://plantuml.com/de/download

wget -O docs/plantuml.jar https://github.com/plantuml/plantuml/releases/download/v1.2023.13/plantuml-1.2023.13.jar


### Graphiz

Der create model process erzeugt dot files. Diese könne mit plantuml in png's ode svg's umgewandelt werden. Dafür wird das dot Kommando aus der graphviz Installtion benötigt.

### Fault Tree Icons

Download tar file for png from https://gitlab.com/lpirl/fault-tree-icons

Extract inside sandbox/icons
```
tar xvzf fault-tree-icons-pngs.tar.gz
```

## Steps

- Model (erzeugen)
- Interfaces und Messages generieren in Sandbox generieren
- individuell Programmieren + testen
- Projekt vollständig generieren inkl. Kopie aus Sandbox



## Create Model
Recrates flexidug.ttl 
```
python create_model.py
```

### Select the railway network model to be used by argument
 > **_NOTE:_** If no argument is used, `Lausitz` is the default network model and the component model is generated without simulation components.

```
python create_model.py [-h] [--config-network-model [{lausitz,joehstadt}]] [--simulation [{none,sumo,infrastructure}]] [--automatic_trains_autoplay] [--automatic_train_registration] [--automatic_train_deregistration]
```

**Network model**
| Network model | Description |
| -------- | ------- |
| lausitz  | Single line between the two railway stations S-Berg and C-Dorf. |
| joehstadt | Single line between the two railway stations Jöhstadt and Schlössel with a stopover in Fahrzeughalle. |

**Simulation**:
| Simulation mode | Description |
| -------- | ------- |
| none  | No simulation. Is the default. |
| infrastructure | Simulate the infrastructure such as signals and switches using a REST service. The elements are visualized via a web interface. See https://github.com/dfriedenberger/infrastructure-simulator |
| sumo | SUMO Simulation (see https://eclipse.dev/sumo/) The simulation components are integrated into the model so that the corresponding modules are injected into the overall system during subsequent generation. The message streams are channelled via the simulation component accordingly.

| SUMO-Simulation Parameter | Description |
| -------- | ------- |
| automatic_trains_autoplay  | The trains from the automatic train configuration are started automatically. |


If you are facing this issue during start-up of the simulation:
```
peer-s-u-m-o-simulation-1 | Authorization required, but no authorization protocol specified
peer-s-u-m-o-simulation-1 |
peer-s-u-m-o-simulation-1 | FXApp::openDisplay: unable to open display host.docker.internal:0
```

run: `xhost +` and make sure, some x-server is running.

**Voice protocol options**
| Option | Description |
| -------- | ------- |
| automatic_train_registration  | Automatic register of train during first position message. |
| automatic_train_deregistration | Automatic deregister of train when reaching last station. |

#### Railway network model for `lausitz`
![Railway network model for lausitz](./docs/network-model-lausitz.png)

#### Digital occupation sheet for `joehstadt` network model
![Digital occupation sheet for joehstadt network model](./docs/digitales_belegblatt_joehstadt.png)


### Validation of Model

#### TurtleValidator

See: https://github.com/IDLabResearch/TurtleValidator
```
$ ttl flexidug-dds.ttl
Validator finished with 0 warnings and 0 errors.
```


## Create System
> **_NOTE:_** There is currently still a shortcoming. The generation process also generates new files in the sandbox folder. To ensure that these also end up in the generated project, currently the following generation command has to be called twice.


```
gen --model flexidug-dds.ttl --path ../generated-flexidug-zlic-implementation/ --sandbox sandbox/
```

### Generate svg images for generated puml Files
```
java -Dplantuml.include.path=docs -DPLANTUML_LIMIT_SIZE=16384 -jar docs/plantuml.jar -svg ../generated-flexidug-zlic-implementation/docs/*.puml
```
> **_NOTE:_** Use Argument -png for PNG Images

### Generate svg images for generated dot Files
```
python convert_dot_to_optimized_svg.py --path ../generated-flexidug-zlic-implementation/docs/
```

### Generate Pdf from documentation

```
docker run -it -u $(id -u):$(id -g) -v "..\generated-flexidug-zlic-implementation\docs:/documents/" asciidoctor/docker-asciidoctor
> 5240124a534b:/documents$ asciidoctor-pdf README.adoc
```
## Some Tests
### Test component proxy interfaces
```
pytest sandbox/
```
### Signal tests
Test Digitales Belegblatt Proxy
```
pytest -v proxy/digitales_belegblatt/
```


### IntegrationTest
```
curl -L -F "audio_data=@fahranfrage_cottbus.wav" http://127.0.0.1:5007/transcribe
curl http://127.0.0.1:5007/messages
curl http://127.0.0.1:5007/message/5b6ff0fb-3eec-4304-8df3-f97ca4de1154 --output test.wav
```

