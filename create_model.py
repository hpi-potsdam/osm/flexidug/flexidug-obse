import os
import argparse
from rdflib import Graph, URIRef, RDFS


from obse.graphwrapper import GraphWrapper
from obse.sparql_queries import SparQLWrapper

from util.name_case_style import kebab_case, snake_case, pascal_case
from util.json_to_rdf_mapper import JsonToRdfMapper
from util.namespaces import SNS
from util.flexidug.namespace import FNS
from util.telegram.namespace import TNS

from util.configuration.namespace import CNS
from util.input_output_process import InputOutputProcess
from util.statemachine_impl import StateMachine

from util.read_lines import read_lines
from util.asset.namespace import ANS
from util.statemachine.namespace import SMNS
from util.asset.directory_builder import DirectoryBuilder
from util.asset.project_asset_builder import ProjectAssetBuilder, IgnoreFilesAndTmpFolder

from transform.system_definition import SystemDefinition, SimulationMode
from transform.data_model_definition import DataModelDefinition, RailwayNetworkModel
from transform.statemachine_definition import StatemachineDefinition
from transform.configuration_definition import ConfigurationDefinition
from transform.fta_definition import FaultTreeAnalysisDefinition
from transform.documentation_generation import DocumentationGeneration

# Create Rdf-Model
graph = Graph()

# bind a user-declared namespace to a prefix
graph.bind("sys", SNS)
graph.bind("fldg", FNS)
graph.bind("cnf", CNS)
graph.bind("sm", SMNS)
graph.bind("tel", TNS)
graph.bind("asset", ANS)


class SimulationAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")
        super().__init__(option_strings, dest, nargs=0, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        if namespace.simulation != SimulationMode.SUMO:
            parser.error('Only permitted with simulation mode "sumo"')
        else:
            setattr(namespace, self.dest, True)


parser = argparse.ArgumentParser(description='Create model.')
parser.add_argument('--config-network-model', nargs='?', default=RailwayNetworkModel._default,
                    choices=RailwayNetworkModel._choices, help="use specific railway network model")
parser.add_argument('--simulation', nargs='?', default=SimulationMode._default,
                    choices=SimulationMode._choices, help="use specific simulation mode")
parser.add_argument('--automatic_trains_autoplay', action=SimulationAction, default=False, help="start automatic trains in simulation")
parser.add_argument('--automatic_train_registration', action='store_true', help="automatic register of train during first position message")
parser.add_argument('--automatic_train_deregistration', action='store_true', help="automatic deregister of train when reaching last station")

args = parser.parse_args()


wrapper = GraphWrapper(graph)


###################################################################################################
# Hier werden die grundlegenden Basis Definitionen für System und Datenmodell definiert/generiert #
###################################################################################################

# Transformation/Creation of System Definition
print("Create system definition from ideas in excel")

rdf_references = SystemDefinition(wrapper, SparQLWrapper(graph)).apply(args.simulation)

# State Machine
print("Create state machine")

rdf_statemachines = StatemachineDefinition(wrapper).apply()
wrapper.add_reference(SNS.isImplementedBy, rdf_references['belegblatt'], rdf_statemachines['train_supervision_statemachine'])

# Data Model
print("Create data model definition")

rdf_models = DataModelDefinition(wrapper).apply(args)

wrapper.add_reference(FNS.hasDataModel, rdf_references['interlocking'], rdf_models['plan_pro_model'])
wrapper.add_reference(FNS.hasDataModel, rdf_references['belegblatt'], rdf_models['track_model'])
wrapper.add_reference(FNS.hasDataModel, rdf_references['belegblatt'], rdf_models['voice_protocol_options_model'])


if args.simulation == SimulationMode.INFRASTRUCTURE:
    wrapper.add_reference(FNS.hasDataModel, rdf_references['interlocking'], rdf_models['infrastructure_model'])
    wrapper.add_reference(FNS.hasDataModel, rdf_references['infrastructure-simulator'], rdf_models['simulator_model'])

if args.simulation == SimulationMode.SUMO:
    wrapper.add_reference(FNS.hasDataModel, rdf_references['interlocking'], rdf_models['traci_model'])
    wrapper.add_reference(FNS.hasDataModel, rdf_references['sumo_simulation'], rdf_models['track_model'])
    wrapper.add_reference(FNS.hasDataModel, rdf_references['sumo_simulation'], rdf_models['automatic-trains-model'])

wrapper.add_reference(FNS.hasDataModel, rdf_references['nlu'], rdf_models['station_model'])

# FTA
rdf_fta = FaultTreeAnalysisDefinition(wrapper, SparQLWrapper(graph)).apply()

graph.serialize(destination="flexidug.ttl", format='turtle')


#############################
# PIM to PSM Transformation #
#############################
print("Create PSM from PIM")

# helping control structure
all_components = ['interlocking', 'belegblatt', 'nlu', 'sip', 'tts', 'stt', 'nothalt', 'tracing']
dds_components = ['interlocking', 'belegblatt', 'nlu', 'sip', 'tts', 'stt', 'nothalt', 'tracing']

if args.simulation == SimulationMode.INFRASTRUCTURE:
    all_components.append('infrastructure-simulator')

if args.simulation == SimulationMode.SUMO:
    all_components.append('sumo_simulation')
    dds_components.append('sumo_simulation')

# Implementierung über DDS


# DDS Modul
rdf_dds = wrapper.add_labeled_instance(SNS.Component,"Data Distribution System",unique_name="dds")
wrapper.add_reference(SNS.hasComponent,rdf_references['zlic'],rdf_dds)

# Interfaces für DDS
rdf_dds_publish = wrapper.add_labeled_instance(SNS.Interface,"Publish",unique_name="dds_publish")
rdf_dds_subscribe = wrapper.add_labeled_instance(SNS.Interface,"Subscribe",unique_name="dds_subscribe")
wrapper.add_reference(SNS.provideInterface,rdf_dds,rdf_dds_publish)
wrapper.add_reference(SNS.provideInterface,rdf_dds,rdf_dds_subscribe)

# Die folgenden Komponenten kommunizieren über DDS
for component_key in dds_components:
    rdf_dds_notify = wrapper.add_labeled_instance(SNS.Interface, "Notify", unique_name=f"{component_key}_notify")
    rdf_component = rdf_references[component_key]

    wrapper.add_reference(SNS.provideInterface,rdf_component,rdf_dds_notify)
    wrapper.add_reference(SNS.requireInterface,rdf_component,rdf_dds_publish)
    wrapper.add_reference(SNS.requireInterface,rdf_component,rdf_dds_subscribe)
    wrapper.add_reference(SNS.requireInterface,rdf_dds,rdf_dds_notify)


# Webinterface for Digitales Belegblatt
rdf_belegblatt_web = wrapper.add_labeled_instance(SNS.Interface,"Web",unique_name="belegblatt_web")
wrapper.add_reference(SNS.provideInterface,rdf_references['belegblatt'],rdf_belegblatt_web)

# Webinterface for Nothalt
rdf_nothalt_web = wrapper.add_labeled_instance(SNS.Interface,"Web",unique_name="nothalt_web")
wrapper.add_reference(SNS.provideInterface,rdf_references['nothalt'],rdf_nothalt_web)

# Infrastructure imulator-Interface for interlocking
if args.simulation == SimulationMode.INFRASTRUCTURE:
    rdf_interlocking_infrastructure_simulator = wrapper.add_labeled_instance(SNS.Interface, "Web", unique_name="interlocking_infrastructure_simulator")
    wrapper.add_reference(SNS.provideInterface, rdf_references['infrastructure-simulator'], rdf_interlocking_infrastructure_simulator)
    wrapper.add_reference(SNS.requireInterface, rdf_references['interlocking'], rdf_interlocking_infrastructure_simulator)

# SUMO Interface for SUMO Simulation
if args.simulation == SimulationMode.SUMO:
    rdf_sumo_sim = wrapper.add_labeled_instance(SNS.Interface, "Simulation", unique_name="sumo_sim")
    wrapper.add_reference(SNS.provideInterface, rdf_references['sumo_simulation'], rdf_sumo_sim)
    # TODO requireInterface fehlt

    rdf_automatic_train = wrapper.add_labeled_instance(SNS.Interface, "Automatic Trains Web", unique_name="simulation_automatic_trains_web")
    wrapper.add_reference(SNS.provideInterface, rdf_references['sumo_simulation'], rdf_automatic_train)
    # TODO requireInterface fehlt

# Webinterface for Tracing
rdf_tracing_web = wrapper.add_labeled_instance(SNS.Interface, "Web", unique_name="tracing_web")
wrapper.add_reference(SNS.provideInterface, rdf_references['tracing'], rdf_tracing_web)

# Webinterface for SIP-Interface
rdf_sip_web = wrapper.add_labeled_instance(SNS.Interface,"Web",unique_name="sip_web")
wrapper.add_reference(SNS.provideInterface,rdf_references['sip'],rdf_sip_web)
wrapper.add_reference(SNS.requireInterface,rdf_references['radioio'],rdf_sip_web)

# Interface between RadioIo and Funkgerät
# TODO who provides, who requires

# Der Tf und das ZLiC System kommunizieren über Sprechfunk
# Radio
rdf_radio1 = wrapper.add_labeled_instance(SNS.Interface,"Recv",unique_name="radio_rcv_1")
wrapper.add_reference(SNS.provideInterface,rdf_references['radio_zlic'],rdf_radio1)
wrapper.add_reference(SNS.requireInterface,rdf_references['radio_tf'],rdf_radio1)

rdf_radio2 = wrapper.add_labeled_instance(SNS.Interface,"Recv",unique_name="radio_rcv_2")
wrapper.add_reference(SNS.provideInterface,rdf_references['radio_tf'],rdf_radio2)
wrapper.add_reference(SNS.requireInterface,rdf_references['radio_zlic'],rdf_radio2)

# DATA Interface between Zug and ZLic (balisen id's)
#rdf_train_zlic_lpwan = wrapper.add_named_instance(MBA.Interface,"LPWan")
#wrapper.add_reference(MBA.has,rdf_train,rdf_train_zlic_lpwan)
#wrapper.add_reference(MBA.has,rdf_zlic,rdf_train_zlic_lpwan)


# DATA Interface between Zug and Balise (NFC)
#rdf_train_balise = wrapper.add_named_instance(MBA.Interface,"NFC")
#wrapper.add_reference(MBA.has,rdf_train,rdf_train_balise)
#wrapper.add_reference(MBA.has,rdf_balise,rdf_train_balise)

#rdf_file = wrapper.add_named_instance(MBA.Interface,"File") 
#wrapper.add_reference(MBA.has,rdf_dlst,rdf_file)
#wrapper.add_reference(MBA.has,rdf_dw,rdf_file)


# Rasta-Interface for interlocking
rdf_interlocking_rasta = wrapper.add_labeled_instance(SNS.Interface,"EULYNXoverRasta",unique_name="interlocking_rasta")
wrapper.add_reference(SNS.provideInterface,rdf_references['interlocking'],rdf_interlocking_rasta)
wrapper.add_reference(SNS.requireInterface,rdf_references['objectcontroller'],rdf_interlocking_rasta)


###########################################################
# Technical Configuration (Port numbers, and credentials) #
###########################################################
rdf_configuration = ConfigurationDefinition(wrapper).apply(args.simulation)

wrapper.add_reference(CNS.hasConfiguration, rdf_references['tts'], rdf_configuration['polly_credentials'])
wrapper.add_reference(CNS.hasConfiguration, rdf_sip_web, rdf_configuration['sip_web_endpoint'])
wrapper.add_reference(CNS.hasConfiguration, rdf_belegblatt_web, rdf_configuration['belegblatt_web_endpoint'])
wrapper.add_reference(CNS.hasConfiguration, rdf_nothalt_web, rdf_configuration['nothalt_web_endpoint'])
wrapper.add_reference(CNS.hasConfiguration, rdf_tracing_web, rdf_configuration['tracing_web_endpoint'])


if args.simulation == SimulationMode.INFRASTRUCTURE:
    wrapper.add_reference(CNS.hasConfiguration, rdf_interlocking_infrastructure_simulator, rdf_configuration['infrastructure_simulator_web_endpoint'])

if args.simulation == SimulationMode.SUMO:
    wrapper.add_reference(CNS.hasConfiguration, rdf_sumo_sim, rdf_configuration['sumo_simulation_endpoint'])
    wrapper.add_reference(CNS.hasConfiguration, rdf_automatic_train, rdf_configuration['sumo_automatic_trains_web_endpoint'])



######################################################
#   Implementation über Asset's                      #
######################################################

sparql_wrapper = SparQLWrapper(graph)


def get_id_key(ref : URIRef):
    return str(ref).split("-")[-1]



# Generation targets
# In die Sandbox werden Interfaces und Messages generiert
# Im ROOT Verzeichnis liegt das generierte Projekt

rdf_asset_root_dir = wrapper.add_labeled_instance(ANS.Directory,"Root")
wrapper.add_str_property(ANS.path,rdf_asset_root_dir,"$ROOT_DIRECTORY")

rdf_asset_sandbox_dir = wrapper.add_labeled_instance(ANS.Directory,"Sandbox")
wrapper.add_str_property(ANS.path,rdf_asset_sandbox_dir,"$SANDBOX_DIRECTORY")

directory_builder = DirectoryBuilder(wrapper)


#################
# Documentation #
#################

DocumentationGeneration(graph).apply(documentation_path="docs/", rdf_asset_root_dir=rdf_asset_root_dir, rdf_asset_sandbox_dir=rdf_asset_sandbox_dir)


#############
# Templates #
#############


rdf_readme_template = wrapper.add_labeled_instance(ANS.Template,"README template")
wrapper.add_str_property(ANS.filename,rdf_readme_template,"README.md") #TODO mustache

rdf_cyclonedds_template = wrapper.add_labeled_instance(ANS.Template,"cyclonedds config template")
wrapper.add_str_property(ANS.filename,rdf_cyclonedds_template,"cyclonedds.xml") #TODO mustache

rdf_model_config_template = wrapper.add_labeled_instance(ANS.Template,"Configuration template")
wrapper.add_str_property(ANS.filename,rdf_model_config_template,"model.yml.mustache")

rdf_proxy_template = wrapper.add_labeled_instance(ANS.Template,"proxy interface template")
wrapper.add_str_property(ANS.filename,rdf_proxy_template,"proxy.py.mustache")

rdf_message_template = wrapper.add_labeled_instance(ANS.Template,"Message template")
wrapper.add_str_property(ANS.filename,rdf_message_template,"message.py.mustache")

rdf_dds_message_template = wrapper.add_labeled_instance(ANS.Template,"DDS-Message template")
wrapper.add_str_property(ANS.filename,rdf_dds_message_template,"dds_message.py.mustache")

rdf_mapper_template = wrapper.add_labeled_instance(ANS.Template,"Mapper template")
wrapper.add_str_property(ANS.filename,rdf_mapper_template,"mapper.py.mustache")

rdf_subscriber_template = wrapper.add_labeled_instance(ANS.Template,"Subscriber template")
wrapper.add_str_property(ANS.filename,rdf_subscriber_template,"subscriber.py.mustache")


rdf_statemachine_template = wrapper.add_labeled_instance(ANS.Template,"StateMachine template")
wrapper.add_str_property(ANS.filename,rdf_statemachine_template,"statemachine.py.mustache")

rdf_wrapper_sh_template = wrapper.add_labeled_instance(ANS.Template,"Wrapper Shell Script template")
wrapper.add_str_property(ANS.filename,rdf_wrapper_sh_template,"wrapper.sh.mustache")

rdf_docker_template = wrapper.add_labeled_instance(ANS.Template,"Dockerfile template")
wrapper.add_str_property(ANS.filename,rdf_docker_template,"Dockerfile.mustache")

# DDS-Komponenten
for component_key in all_components:
    rdf_component = rdf_references[component_key]

    is_dds_component = component_key in dds_components

    name = sparql_wrapper.get_single_object_property(rdf_component,RDFS.label)
    project_name = kebab_case(name)
    project_path = snake_case(name)

    #Reader
    input_output_process = InputOutputProcess(sparql_wrapper,name)
    
    #Writer
    json_to_rdf_mapper = JsonToRdfMapper(wrapper)
    
    builder = ProjectAssetBuilder(wrapper,directory_builder,name)
   
    #Folder Structure project and project / src
    rdf_asset_project_dir = directory_builder.get_directory(rdf_asset_root_dir,project_path,key="Generated")
    rdf_asset_project_source_dir = directory_builder.get_directory(rdf_asset_project_dir,f"{project_path}/src",key="Generated")

    #Sandbox Folder 'proxy'
    proxy_path = os.path.join("sandbox",project_path)

    #Folder Structure sandbox / project and sandbox / project / src and sandbox / project / datamodel
    rdf_asset_sandbox_project_dir = directory_builder.get_directory(rdf_asset_sandbox_dir,project_path,key="Sandbox")
    rdf_asset_sandbox_project_source_dir = directory_builder.get_directory(rdf_asset_sandbox_project_dir,f"{project_path}/src",key="Sandbox")
    rdf_asset_sandbox_project_datamodel_dir = directory_builder.get_directory(rdf_asset_sandbox_project_dir,f"{project_path}/datamodel",key="Sandbox")

    #README.md
    rdf_readme_config = json_to_rdf_mapper.apply({},f"{project_name}-readme")
    builder.add_asset(rdf_readme_template,rdf_readme_config,"README",rdf_asset_project_dir,"README.md")


    if is_dds_component:
        # Create dds-config.xml
        rdf_cyclonedds_config = json_to_rdf_mapper.apply({},f"{project_name}-cyclonedds")
        builder.add_asset(rdf_cyclonedds_template,rdf_cyclonedds_config,"cyclonedds",rdf_asset_project_dir,"cyclonedds.xml")

    data_model_config = {
        "properties" : [],
        "stations" : [],
        "tracks" : [],
        "infrastructure" : [],
        "simulation" : [],
        "traci" : []
    }


    # Create Data Model Files
    for rdf_model in sparql_wrapper.get_out_references(rdf_component,FNS.hasDataModel):
        model_type = sparql_wrapper.get_type(rdf_model)
        print(name,"has model",model_type)
    
        if model_type == FNS.PlanProModel:
            model_filename = sparql_wrapper.get_single_object_property(rdf_model,FNS.filename)
            data_model_config["properties"].append({"key" : "plan_pro_model_file" , "value" : model_filename})

        if model_type == FNS.AutomaticTrainsModel:
            model_filename = sparql_wrapper.get_single_object_property(rdf_model, FNS.filename)
            data_model_config["properties"].append({"key": "automatic_trains_model_file", "value": model_filename})
            model_autoplay = sparql_wrapper.get_single_object_property(rdf_model, FNS.autoplay)
            data_model_config["properties"].append({"key": "automatic_trains_autoplay", "value": bool(model_autoplay)})

        if model_type == FNS.VoiceProtocolOptionsModel:
            automatic_train_registration = sparql_wrapper.get_single_object_property(rdf_model, FNS.automaticTrainRegistration)
            data_model_config["properties"].append({"key": "automatic_train_registration", "value": bool(automatic_train_registration)})
            automatic_train_deregistration = sparql_wrapper.get_single_object_property(rdf_model, FNS.automaticTrainDeregistration)
            data_model_config["properties"].append({"key": "automatic_train_deregistration", "value": bool(automatic_train_deregistration)})

        if model_type == FNS.StationModel:
            for rdf_station in sparql_wrapper.get_out_references(rdf_model,FNS.hasStation):
                station_name = sparql_wrapper.get_single_object_property(rdf_station,FNS.stationName)
                data_model_config["stations"].append({"name" : station_name})
        
        if model_type == FNS.TrackModel:
            for rdf_track in sparql_wrapper.get_out_references(rdf_model,FNS.hasTrack):
                track = {
                    "stop_over_stations": [],
                    "routes" : {
                        "out" : [],
                        "back" : []
                    }
                }
                for rdf_station in sparql_wrapper.get_out_references(rdf_track,FNS.hasStopOverStation):
                    station_name = sparql_wrapper.get_single_object_property(rdf_station,FNS.stationName)
                    track["stop_over_stations"].append({"name" : station_name})

                if sparql_wrapper.has_out_reference(rdf_track, FNS.hasRouteSection):
                    p_blanc = sparql_wrapper.get_single_out_reference(rdf_track, FNS.hasRouteSection)

                    for rdf_route_section in sparql_wrapper.get_sequence(p_blanc):

                        from_signal = sparql_wrapper.get_single_object_property(rdf_route_section, FNS.fromSignal)
                        to_signal = sparql_wrapper.get_single_object_property(rdf_route_section, FNS.toSignal)
                        direction = sparql_wrapper.get_single_object_property(rdf_route_section, FNS.direction)

                        route_section = {
                            "from_signal": from_signal,
                            "to_signal": to_signal
                        }

                        if sparql_wrapper.has_out_reference(rdf_route_section, FNS.inStation):
                            rdf_station = sparql_wrapper.get_single_out_reference(rdf_route_section, FNS.inStation)
                            route_section['station'] = sparql_wrapper.get_single_object_property(rdf_station, FNS.stationName)

                        track['routes'][direction].append(route_section)

                data_model_config["tracks"].append(track)

        if model_type == FNS.InfrastructureModel:
            infrastructure = { "signals" : [] , "points" : [] }
            for rdf_infrastructure in sparql_wrapper.get_out_references(rdf_model,FNS.hasInfrastructure):
                rdf_type = sparql_wrapper.get_type(rdf_infrastructure)
                identifier = sparql_wrapper.get_single_object_property(rdf_infrastructure,FNS.identifier)
                infrastructure_config = {"id" : identifier}
                #if simulated
                infrastructure_config["is_simulated"] = True
                infrastructure_config["url"] = "${INFRASTRUCTURE_SIMULATOR_URL}" #TODO (find provider and generate Enviroment)
                infrastructure_config["board"] = "flexidug" #TODO identisch zur Board-Konfiguration
                #else
                #infrastructure_config["is_connected_via_rest"] = True

                if rdf_type == FNS.SignalNe1 or rdf_type == FNS.SignalNe5:
                    infrastructure["signals"].append(infrastructure_config)
                if rdf_type == FNS.Point:
                    infrastructure["points"].append(infrastructure_config)
            data_model_config["infrastructure"].append(infrastructure)

        if model_type == FNS.TraciModel:
            data_model_config["traci"].append({})

        if model_type == FNS.SimulatorModel:
            infrastructure = { "board" : "flexidug" , "items" : [] }
            for rdf_infrastructure in sparql_wrapper.get_out_references(rdf_model,FNS.hasInfrastructure):
                rdf_type = sparql_wrapper.get_type(rdf_infrastructure)
                label = sparql_wrapper.get_single_object_property(rdf_infrastructure,RDFS.label)
                identifier = sparql_wrapper.get_single_object_property(rdf_infrastructure,FNS.identifier)
               
                if rdf_type == FNS.SignalNe1:
                    infrastructure["items"].append({"id" : identifier , "type" :"Trapeztafel2000" , "name" : label })
                if rdf_type == FNS.SignalNe5:
                    infrastructure["items"].append({"id" : identifier , "type" :"Haltetafel2000" , "name" : label })
                if rdf_type == FNS.Point:
                    infrastructure["items"].append({"id" : identifier , "type" : "Weiche" , "name" : label })
            data_model_config["simulation"].append(infrastructure)





    rdf_model_config = json_to_rdf_mapper.apply(data_model_config,f"{project_name}-data-model")
    builder.add_asset(rdf_model_config_template,rdf_model_config,"Model Configuration",rdf_asset_sandbox_project_datamodel_dir,"model.yml")



    # Create __init__.py (nötig)

    if is_dds_component:

        # Datenstrukturen
        for p_out_mesg in sparql_wrapper.get_in_references(rdf_component,SNS.hasSender):
            input_output_process.add_output_message(p_out_mesg)

        for p_in_mesg in sparql_wrapper.get_in_references(rdf_component,SNS.hasReceiver):
            input_output_process.add_input_message(p_in_mesg)

        #Messages
        for msg_conf in input_output_process.get_message_configs():
            msg_name = msg_conf['MessageClassName']
            rdf_message_config = json_to_rdf_mapper.apply(msg_conf,f"{project_name}-{msg_name}")
            builder.add_asset(rdf_message_template,rdf_message_config,f"Message {msg_name}",rdf_asset_sandbox_project_source_dir,f"{msg_name}.py")

        #DDS Messages
        for msg_conf in input_output_process.get_dds_message_configs():
            msg_name = msg_conf['MessageClassName']
            rdf_message_config = json_to_rdf_mapper.apply(msg_conf,f"{project_name}-{msg_name}")
            builder.add_asset(rdf_dds_message_template,rdf_message_config,f"Message {msg_name}",rdf_asset_project_source_dir,f"{msg_name}.py")

        # Proxy-Interface
        rdf_proxy_config = json_to_rdf_mapper.apply(input_output_process.get_proxy_config(),f"{project_name}-proxy")
        builder.add_asset(rdf_proxy_template,rdf_proxy_config,"ProxyInterface",rdf_asset_sandbox_project_source_dir,f"{pascal_case(name)}ProxyInterface.py")


        # Mapper
        rdf_mapper_config = json_to_rdf_mapper.apply(input_output_process.get_mapper_config(),f"{project_name}-mapper")
        builder.add_asset(rdf_mapper_template,rdf_mapper_config,"Mapper",rdf_asset_project_source_dir,"mapper.py")
    
        # Subscriber
        rdf_subscriber_config = json_to_rdf_mapper.apply(input_output_process.get_subscriber_config(),f"{project_name}-subscriber")
        builder.add_asset(rdf_subscriber_template,rdf_subscriber_config,"Subscriber",rdf_asset_project_dir,"subscriber.py")

    # Other Components (like statemachines)
    for rdf_implementation in sparql_wrapper.get_out_references(rdf_component,SNS.isImplementedBy):
        impl_type = sparql_wrapper.get_type(rdf_implementation)
        if impl_type == SMNS.StateMachine:
            statemachine = StateMachine(sparql_wrapper,rdf_implementation)
            rdf_statemachine_config = json_to_rdf_mapper.apply(statemachine.get_config(),f"{project_name}-{statemachine.get_classname()}")
            builder.add_asset(rdf_statemachine_template,rdf_statemachine_config,statemachine.get_classname(),rdf_asset_sandbox_project_source_dir,f"{statemachine.get_classname()}.py")

    # Initialization of Project
            
    if not os.path.exists(proxy_path):

        # create project path  
        print(f"Create Projektpath {proxy_path}")
        os.mkdir(proxy_path)

        # Init project with default files
        with open(f"{proxy_path}/docker-from.txt", 'w') as f:
            f.write("FROM reg.osmhpi.de/flexidug/cyclonedds/3.11-slim-bookworm:1.0.0\n")
            # TODO read default image from config


    # wrapper.sh 
    # TODO (auch über Template lösen und rasa prozess modellieren)
    wrapper_txt = read_lines(proxy_path,"wrapper.txt",must_exists=False)

    if is_dds_component:
        wrapper_txt.append("#Start subscriber")
        wrapper_txt.append("python subscriber.py &")
    wrapper_config = { "include" : '\n'.join(wrapper_txt)}

    rdf_wrapper_sh_config = json_to_rdf_mapper.apply(wrapper_config,f"{project_name}-wrapper.sh")
    builder.add_asset(rdf_wrapper_sh_template,rdf_wrapper_sh_config,"Wrapper",rdf_asset_project_dir,"wrapper.sh")

    # Dockerfile
    docker_from_txt = read_lines(proxy_path,"docker-from.txt",must_exists=True)
    docker_txt = read_lines(proxy_path,"docker.txt",must_exists=False)
    docker_config = {
        "from" : '\n'.join(docker_from_txt) ,
        "include" : '\n'.join(docker_txt)
    }

    if is_dds_component:
        docker_config["dds"] = True

    rdf_docker_config = json_to_rdf_mapper.apply(docker_config,f"{project_name}-Dockerfile")
    builder.add_asset(rdf_docker_template,rdf_docker_config,"Dockerfile",rdf_asset_project_dir,"Dockerfile")

    # Copy all Assets from sandbox to project dir #
    builder.copy_folder(proxy_path, rdf_asset_sandbox_project_dir, rdf_asset_project_dir, ignore = IgnoreFilesAndTmpFolder())


###########################################
# docker-compose, system.yml und pipeline #
###########################################


config = {
    "kubernetes_configuration" : {
        "service" : [],
        "pod" : []
    },
    "docker_compose_configuration" : {
        "service" : []
    },
    "gitlab_ci_configuration" : {
        "modul" : [],
        "generic_secrets" : []
    }
}


# Komponenten die als Docker-Image gebaut werden
for component_key in all_components:
    rdf_component = rdf_references[component_key]
    
    name = sparql_wrapper.get_single_object_property(rdf_component,RDFS.label)
    project_name = kebab_case(name)
    project_path = snake_case(name)

    docker_image_prefix = "reg.osmhpi.de/flexidug"

    kubernetes_pod_configuration = dict()
    kubernetes_service_configuration = dict()

    kubernetes_pod_configuration['name'] =  project_name #TODO name ist nicht speziell genug
    kubernetes_pod_configuration['port'] =  []
    kubernetes_pod_configuration['volume'] =  []
    kubernetes_pod_configuration['volume_mount'] =  []
    kubernetes_pod_configuration['environment'] = []
    kubernetes_pod_configuration['build'] =  project_path
    kubernetes_pod_configuration['image'] =  f"{docker_image_prefix}/{project_name}"

    docker_compose_configuration = dict()
    docker_compose_configuration['port'] =  []
    docker_compose_configuration['environment'] =  []
    docker_compose_configuration['volume'] =  []
    docker_compose_configuration['name'] =  project_name #TODO name ist nicht speziell genug
    docker_compose_configuration['build'] =  project_path
    docker_compose_configuration['image'] =  f"{docker_image_prefix}/{project_name}"

    gitlab_ci_configuration = dict()
    gitlab_ci_configuration['name'] =  project_name #TODO name ist nicht speziell genug
    gitlab_ci_configuration['build'] =  project_path
    gitlab_ci_configuration['image'] =  f"{docker_image_prefix}/{project_name}"
    
    #Sind die Komponenten über Ports erreichbar
    for rdf_interface in sparql_wrapper.get_out_references(rdf_component, SNS.provideInterface):
        for rdf_configuration in sparql_wrapper.get_out_references(rdf_interface, CNS.hasConfiguration):
            rdf_type = sparql_wrapper.get_type(rdf_configuration)
            if rdf_type == CNS.EndpointConfiguration:
                port = sparql_wrapper.get_single_object_property(rdf_configuration, CNS.portNumber)
                host_name = sparql_wrapper.get_single_object_property(rdf_configuration, CNS.hostName)
                env_name = sparql_wrapper.get_single_object_property(rdf_configuration, CNS.environmentNamePortNumber)
                print(name,"Endpoint",env_name,port,host_name)
                #TODO Endpoint to system_lab.yml
                kubernetes_pod_configuration['port'].append({
                    "container_port" : port,
                    "container_port_name" : "web",
                })
                kubernetes_pod_configuration['environment'].append({
                    "name" : env_name,
                    "value" : str(port) # Environment has to be a string
                })

                # needs Modeling as Service
                kubernetes_service_configuration['name'] = project_name
                kubernetes_service_configuration['service_name'] = f"{project_name}-service"
                
                if 'port' not in kubernetes_service_configuration:
                    kubernetes_service_configuration['port'] = []

                kubernetes_service_configuration['port'].append({
                    "container_port_name" : "web",
                    "number" : port
                })

                docker_compose_configuration['port'].append({
                    "container_port" : port,
                    "host_port" : port
                })
                docker_compose_configuration['hostname'] = f"{project_name}.internal"

                docker_compose_configuration['environment'].append({
                    "name" : env_name,
                    "value" : str(port) # Environment has to be a string
                })

    #Werde andere Services über Ports erreicht
    for rdf_interface in sparql_wrapper.get_out_references(rdf_component, SNS.requireInterface):

        rdf_provider_components = sparql_wrapper.get_in_references(rdf_interface, SNS.provideInterface)
        if len(rdf_provider_components) != 1:
            raise ValueError("Provided Interface has no/multiple provider {rdf_provider_components}")
        provider = sparql_wrapper.get_single_object_property(rdf_provider_components[0],RDFS.label)
       
        for rdf_configuration in sparql_wrapper.get_out_references(rdf_interface, CNS.hasConfiguration):
            rdf_type = sparql_wrapper.get_type(rdf_configuration)
            if rdf_type == CNS.EndpointConfiguration:
                port = sparql_wrapper.get_single_object_property(rdf_configuration, CNS.portNumber)
                host_name = sparql_wrapper.get_single_object_property(rdf_configuration, CNS.hostName)

                print(name,"Require Endpoint",port,host_name,"from",f"{kebab_case(provider)}.internal")

                docker_compose_configuration['environment'].append({
                    "name" : f"{snake_case(provider).upper()}_URL",
                    "value" : f"http://{kebab_case(provider)}.internal:{port}" 
                })

                kubernetes_pod_configuration['environment'].append({
                    "name" : f"{snake_case(provider).upper()}_URL",
                    "value" : f"http://{kebab_case(provider)}-service:{port}" # Environment has to be a string
                })

    #Configuration
    for rdf_configuration in sparql_wrapper.get_out_references(rdf_component,CNS.hasConfiguration):
        use_name = sparql_wrapper.get_single_object_property(rdf_configuration,RDFS.label)
        rdf_type = sparql_wrapper.get_type(rdf_configuration)
        print(rdf_component,"has",use_name,rdf_type)
        if rdf_type == CNS.CredentialConfiguration:
            credentials_filename = sparql_wrapper.get_single_object_property(rdf_configuration,CNS.credentialFilename)
            #TODO target_path from model
            target_path = "/root/.aws/credentials"
            secret_name = snake_case(use_name).replace("_","")
            print(secret_name,"map",credentials_filename,"to",target_path)

            #TODO add "kubectl create secret generic ..." to Readme 

            config["gitlab_ci_configuration"]["generic_secrets"].append({
                "name" : secret_name,
                "credential_file" : credentials_filename
            })



            # Variante 1 Kubernetes
            kubernetes_pod_configuration['volume'].append({
                "name" : secret_name
            })

            path , name = os.path.split(target_path)
            kubernetes_pod_configuration['volume_mount'].append({
                "name" : secret_name,
                "mount_path" : path,
                "mount_name" : name
            })

            # Variante 2 Docker-Compose
            docker_compose_configuration['volume'].append({
                "name" : secret_name,
                "mount_from" : credentials_filename,
                "mount_to" : target_path
            })

    config["kubernetes_configuration"]["pod"].append(kubernetes_pod_configuration)
    if bool(kubernetes_service_configuration): # not empty
        config["kubernetes_configuration"]["service"].append(kubernetes_service_configuration)
    config["docker_compose_configuration"]["service"].append(docker_compose_configuration)
    config["gitlab_ci_configuration"]["modul"].append(gitlab_ci_configuration)

    # Add Credentials for Docker Registry
    config["gitlab_ci_configuration"]["kubernetes_namespace"] = "zlic"
    config["gitlab_ci_configuration"]["environment_production_url"] = "https://zlic.lab.osmhpi.de"
    config["gitlab_ci_configuration"]["docker_registry"] = {
        "username" : "$CI_REGISTRY_USER",
        "password" : "$CI_REGISTRY_PASSWORD",
        "server" : "$CI_REGISTRY"
    }
    



# Templates
rdf_kubernetes_spec_template = wrapper.add_labeled_instance(ANS.Template,"Object spec template")
wrapper.add_str_property(ANS.filename,rdf_kubernetes_spec_template,"kubernetes-object-spec.yml.mustache")

rdf_docker_compose_template = wrapper.add_labeled_instance(ANS.Template,"Docker Compose template")
wrapper.add_str_property(ANS.filename,rdf_docker_compose_template,"docker-compose.yml.mustache")

rdf_gitlab_ci_template = wrapper.add_labeled_instance(ANS.Template,"GitLab CI template")
wrapper.add_str_property(ANS.filename,rdf_gitlab_ci_template,"gitlab-ci.yml.mustache")

# Configuration
#print("config",json.dumps(config,indent=4))
json_to_rdf_mapper = JsonToRdfMapper(wrapper)
rdf_kubernetes_configuration = json_to_rdf_mapper.apply(config["kubernetes_configuration"],"kubernetes")
rdf_docker_compose_configuration = json_to_rdf_mapper.apply(config["docker_compose_configuration"],"docker_compose")
rdf_gitlab_ci_configuration = json_to_rdf_mapper.apply(config["gitlab_ci_configuration"],"gitlab_ci")


# Asset - YAML File with Kubernetes Resources
rdf_asset_kubernetes = wrapper.add_labeled_instance(ANS.Asset,"Kubernetes Resources")

rdf_target_kubernetes = wrapper.add_labeled_instance(ANS.Target,f"Kubernetes")
wrapper.add_str_property(ANS.filename,rdf_target_kubernetes,"system.yml")
wrapper.add_reference(ANS.hasDirectory,rdf_target_kubernetes,rdf_asset_root_dir)
wrapper.add_reference(ANS.hasTarget,rdf_asset_kubernetes,rdf_target_kubernetes)



wrapper.add_reference(ANS.hasTemplate,rdf_asset_kubernetes,rdf_kubernetes_spec_template)
wrapper.add_reference(ANS.hasConfiguration,rdf_asset_kubernetes,rdf_kubernetes_configuration)

# Asset - Docker Compose file for local testing
rdf_asset_docker_compose = wrapper.add_labeled_instance(ANS.Asset,"Docker Compose file")
wrapper.add_str_property(ANS.filename,rdf_asset_docker_compose,"docker-compose.yml")
wrapper.add_str_property(ANS.path,rdf_asset_docker_compose,".")


rdf_target_docker_compose = wrapper.add_labeled_instance(ANS.Target,f"Docker Compose file")
wrapper.add_str_property(ANS.filename,rdf_target_docker_compose,"docker-compose.yml")
wrapper.add_reference(ANS.hasDirectory,rdf_target_docker_compose,rdf_asset_root_dir)
wrapper.add_reference(ANS.hasTarget,rdf_asset_docker_compose,rdf_target_docker_compose)

wrapper.add_reference(ANS.hasTemplate,rdf_asset_docker_compose,rdf_docker_compose_template)
wrapper.add_reference(ANS.hasConfiguration,rdf_asset_docker_compose,rdf_docker_compose_configuration)

# Asset - Pipeline (gitlab ci)
rdf_asset_gitlab_ci = wrapper.add_labeled_instance(ANS.Asset,"GitLab pipeline")

rdf_target_gitlab_ci = wrapper.add_labeled_instance(ANS.Target,f"Gitlab Pipeline")
wrapper.add_str_property(ANS.filename,rdf_target_gitlab_ci,".gitlab-ci.yml")
wrapper.add_reference(ANS.hasDirectory,rdf_target_gitlab_ci,rdf_asset_root_dir)
wrapper.add_reference(ANS.hasTarget,rdf_asset_gitlab_ci,rdf_target_gitlab_ci)


wrapper.add_reference(ANS.hasTemplate,rdf_asset_gitlab_ci,rdf_gitlab_ci_template)
wrapper.add_reference(ANS.hasConfiguration,rdf_asset_gitlab_ci,rdf_gitlab_ci_configuration)

graph.serialize(destination=f"flexidug-dds.ttl",format='turtle')

