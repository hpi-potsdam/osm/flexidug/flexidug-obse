import os
from typing import Dict

from rdflib import Graph, RDFS, URIRef
from rdf2puml.pumlmodel import PumlModel
from rdf2puml.constants import DIRECTION, POSITION
from rdf2puml.rdf2puml import rdf2puml

from obse.sparql_queries import SparQLWrapper
from obse.graphwrapper import GraphWrapper

from util.namespaces import SNS
from util.statemachine.namespace import SMNS
from util.flexidug.namespace import FNS
from util.telegram.namespace import TNS
from util.fta.namespace import FTANS
from util.dot_model import DotModel
from util.name_case_style import kebab_case, snake_case


from util.asset.namespace import ANS
from util.asset.directory_builder import DirectoryBuilder

from util.json_to_rdf_mapper import JsonToRdfMapper


def get_name(wrapper, instance):
    names = wrapper.get_object_properties(instance, RDFS.label)
    return "/".join(names)


def get_type(wrapper, instance):
    return wrapper.get_type(instance).split("#")[1]


def create_unique_id(o):
    s = str(o).split('#')[-1]
    return s.replace("/", "_").replace("-", "_").replace(":", "_").replace("[", "_").replace("]", "_").replace(" ", "_").replace("(", "_").replace(")", "_")


def get_id(instance):
    return str(instance).split("#")[1].replace("-", "").replace("/","_").replace(".","_").replace("(","_").replace(")","_")


def packages2json(graph: Graph):
    config = {
        "title": "Zugleiter in the Cloud",
        "description": "System Dokumentation des ZLiC",
        "image": "packages.svg",
        "layout": "LAYOUT_LEFT_RIGHT",
        "packages": [],
        "actors": [],
        "relations": [],
        "faulttrees": []
    }
    relations = dict()
    wrapper = SparQLWrapper(graph)

    # Subsysteme und Komponenten
    for package in wrapper.get_instances_of_type(SNS.Subsystem):
        package_name = get_name(wrapper, package)
        package_config = {
            "name": package_name,
            "id": create_unique_id(package),
            "components": [],
        }

        for component in wrapper.get_out_references(package, SNS.hasComponent):
            component_name = get_name(wrapper, component)
            component_config = {
                "name": component_name,
                "id": create_unique_id(component),
                "link": f"component-{kebab_case(component_name)}.adoc",
                "functions": []
            }
            for function in wrapper.get_out_references(component, SNS.hasFunction):
                function_name = get_name(wrapper, function)
                component_config["functions"].append({"name": function_name})

            package_config['components'].append(component_config)

        config['packages'].append(package_config)

    # Aktoren
    for actor in wrapper.get_instances_of_type(SNS.Actor):
        actor_name = get_name(wrapper, actor)
        actor_type = get_type(wrapper, actor)
        config['actors'].append({
            "name": actor_name,
            "id": create_unique_id(actor),
            "type": actor_type,
        })

        for uses in wrapper.get_out_references(actor, SNS.usesComponent):
            relations[f"{actor}-{uses}"] = {
                "name": "use",
                "from": create_unique_id(actor),
                "to": create_unique_id(uses),
            }

    # Relations
    for message in wrapper.get_instances_of_type(SNS.Message):
        for sender in wrapper.get_out_references(message, SNS.hasSender):
            for receiver in wrapper.get_out_references(message, SNS.hasReceiver):
                relations[f"{sender}-{receiver}"] = {
                    "name": "",
                    "from": create_unique_id(sender),
                    "to": create_unique_id(receiver),
                }

    config['relations'] = list(relations.values())

    # Fault Trees

    for p_top_event in wrapper.get_instances_of_type(FTANS.TopEvent):
        name = wrapper.get_single_object_property(p_top_event, RDFS.label)
        config['faulttrees'].append({
            "name": name,
            "link": f"faulttree-{kebab_case(name)}.adoc"
        })

    return config


def component2json(graph: Graph) -> dict:
    components = dict()
    wrapper = SparQLWrapper(graph)

    for component in wrapper.get_instances_of_type(SNS.Component):
        component_name = get_name(wrapper, component)

        component_json = {
            "title": component_name,
            "description": f"Komponenten Dokumentation von {component_name}",
            "image": f"component-{kebab_case(component_name)}.svg",
            "layout": "LAYOUT_TOP_DOWN",
            "components": [
                {
                    "name": component_name,
                    "id": create_unique_id(component),
                }
            ],
            "functions": [],
            "interfaces": [],
            "messages": [],
            "implementations": [],
            "models": [],
            "relations": [],
            "relations_undirected": [],
            "relations_right": [],
            "relations_up": []
        }

        # Funktionalitäten
        for function in wrapper.get_out_references(component, SNS.hasFunction):
            function_name = get_name(wrapper, function)

            component_json["functions"].append({
                "name": function_name,
                "id": create_unique_id(function),
            })
            component_json["relations_undirected"].append({
                "name": "",  # message_name,
                "from": create_unique_id(function),
                "to": create_unique_id(component),
            })

        # Schnittstellen
        for interface in wrapper.get_out_references(component, SNS.provideInterface):
            interface_name = get_name(wrapper, interface)
            component_json["interfaces"].append({
                "name": interface_name,
                "direction": "provide",
                "id": create_unique_id(interface),
            })
            component_json["relations"].append({
                "name": "provides",  # message_name,
                "from": create_unique_id(interface),
                "to": create_unique_id(component),
            })

        for interface in wrapper.get_out_references(component, SNS.requireInterface):
            interface_name = get_name(wrapper, interface)
            component_json["interfaces"].append({
                "name": interface_name,
                "direction": "require",
                "id": create_unique_id(interface),
            })
            component_json["relations"].append({
                "name": "require",
                "from": create_unique_id(component),
                "to": create_unique_id(interface),
            })

        # Messages
        for message in wrapper.get_in_references(component, SNS.hasSender):
            message_name = wrapper.get_single_object_property(message, SNS.messageName)
            message_info = get_name(wrapper, message)
            component_json["messages"].append({
                "name": message_name,
                "info": message_info,
                "link": f"message-{kebab_case(message_name)}.adoc",
                "direction": "send",
                "id": create_unique_id(message),
            })
            component_json["relations_right"].append({
                "name": " ",
                "from": create_unique_id(component),
                "to": create_unique_id(message),
            })

        for message in wrapper.get_in_references(component, SNS.hasReceiver):
            message_name = wrapper.get_single_object_property(message, SNS.messageName)
            message_info = get_name(wrapper, message)
            component_json["messages"].append({
                "name": message_name,
                "info": message_info,
                "link": f"message-{kebab_case(message_name)}.adoc",
                "direction": "receive",
                "id": create_unique_id(message),
            })
            component_json["relations_right"].append({
                "name": " ",
                "from": create_unique_id(message),
                "to": create_unique_id(component),
            })

        # Implementations
        for impl in wrapper.get_out_references(component, SNS.isImplementedBy):
            impl_name = get_name(wrapper, impl)
            impl_type = get_type(wrapper, impl)

            component_json["implementations"].append({
                "name": impl_name,
                "type": impl_type,
                "link": f"implementation-{kebab_case(impl_name)}.adoc",
                "id": create_unique_id(impl),
            })
    
            component_json["relations_up"].append({
                "name": "implemented by",
                "from": create_unique_id(component),
                "to": create_unique_id(impl),
            })

        # Datenmodelle
        for model in wrapper.get_out_references(component, FNS.hasDataModel):
            model_name = get_name(wrapper, model)
            model_type = get_type(wrapper, model)
            component_json["models"].append({
                "name": model_name,
                "type": model_type,
                "id": create_unique_id(model),
            })
            component_json["relations_up"].append({
                "name": " ",
                "from": create_unique_id(component),
                "to": create_unique_id(model),
            })

        components[component_name] = component_json
    return components


def statemachines2json(graph: Graph) -> Dict[str, object]:
    puml_models = {}
    wrapper = SparQLWrapper(graph)

    for state_machine in wrapper.get_instances_of_type(SMNS.StateMachine):
        state_machine_name = get_name(wrapper, state_machine)

        statemachine_json = {
            "title": state_machine_name,
            "description": f"ImplementierungsDokumentation der State Machine {state_machine_name}",
            "image": f"implementation-{kebab_case(state_machine_name)}.svg",
            "layout": "LAYOUT_TOP_DOWN",
            "transitions": [],
            "states": [],
        }

        for element in wrapper.get_out_references(state_machine, SMNS.contains):
            name = get_name(wrapper, element)
            element_type = wrapper.get_type(element)

            if element_type == SMNS.Transition:
                source_state = wrapper.get_out_references(element, SMNS.source)[0]
                target_state = wrapper.get_out_references(element, SMNS.target)[0]

                source_state_name = get_name(wrapper, source_state)
                target_state_name = get_name(wrapper, target_state)

                transition = {
                    "source": source_state_name,
                    "target": target_state_name,
                    "name": name,
                }

                # Event
                event_name = ""
                events = wrapper.get_out_references(element, SMNS.transitionEvent)
                if len(events) > 1:
                    raise ValueError("more than one event is not allowed for transition => {events}")
                if len(events) == 1:
                    event_name = get_name(wrapper, events[0])
                    transition["event"] = event_name

                # Guard
                guard_name = "True"
                guards = wrapper.get_out_references(element, SMNS.transitionGuard)
                if len(guards) > 1:
                    raise ValueError("more than one guard is not allowed for transition => {guards}")
                if len(guards) == 1:
                    guard_name = get_name(wrapper, guards[0])
                    transition["guard"] = guard_name

                # Action
                action_name = "Nop"
                actions = wrapper.get_out_references(element, SMNS.transitionAction)
                if len(actions) > 1: 
                    raise ValueError(f"more than one action is not allowed for transition => {actions}")
                if len(actions) == 1:
                    action_name = get_name(wrapper, actions[0])
                    transition["action"] = action_name

                transition["label"] = f"{event_name} [{guard_name}] / {action_name}"

                statemachine_json['transitions'].append(transition)

            elif element_type == SMNS.FinalState:
                statemachine_json['states'].append({
                    "name": name,
                    "type": "end"
                })
            elif element_type == SMNS.InitialState:
                statemachine_json['states'].append({
                    "name": name,
                    "type": "start"
                })
            elif element_type == SMNS.Junction:
                statemachine_json['states'].append({
                    "name": name,
                    "type": "choice"
                })
            elif element_type == SMNS.State:  # default
                statemachine_json['states'].append({
                    "name": name
                })
        puml_models[state_machine_name] = statemachine_json

    return puml_models


def create_telegram(wrapper, message_json, telegram):

    telegram_name = get_name(wrapper, telegram)
    # telegram_type = get_type(wrapper, telegram)

    properties = []
    array_properties = []
    for data_field in wrapper.get_out_references(telegram, TNS.hasDataField):
        var_name = wrapper.get_single_object_property(data_field, TNS.elementName)
        rdf_type = wrapper.get_single_out_reference(data_field, TNS.hasDataType)
        var_type = wrapper.get_type(rdf_type)
        if var_type == TNS.Array:
            rdf_item_type = wrapper.get_single_out_reference(data_field, TNS.hasArrayItemDataType)
            var_item_type = wrapper.get_type(rdf_item_type)
            if var_item_type == TNS.Structure:
                array_properties.append({"name": var_name, "to": create_unique_id(rdf_item_type)})
                create_telegram(wrapper, message_json, rdf_item_type)
            else:
                raise ValueError(f"Not implemented for type {var_item_type}")
        elif var_type == TNS.Structure:
            raise ValueError(f"TNS.Structure not implemented add root level for {telegram_name}")
        else:
            type_str = get_type(wrapper, rdf_type)
            properties.append({"name": var_name, "type": type_str})

    message_json["telegrams"].append({
        "name": telegram_name,
        "id": create_unique_id(telegram),
        "properties": properties,
        "array_properties": array_properties
    })


def messages2json(graph: Graph) -> Dict[str, object]:
    puml_models = {}
    wrapper = SparQLWrapper(graph)

    for message in wrapper.get_instances_of_type(SNS.Message):

        message_name = wrapper.get_single_object_property(message, SNS.messageName)
        message_info = get_name(wrapper, message)
        # message_type = get_type(wrapper, message)

        message_json = {
            "title": message_name,
            "description": f"Nachrichten Dokumentation von {message_name}",
            "image": f"message-{kebab_case(message_name)}.svg",
            "layout": "LAYOUT_LEFT_RIGHT",
            "message": {
                    "name": message_name,
                    "id": create_unique_id(message),
                    "info": message_info  # create_note(message, message_info, position=POSITION.TOP)
            },
            "telegrams": [],
            "relations": [],
        }

        for telegram in wrapper.get_out_references(message, TNS.hasStructure):
            create_telegram(wrapper, message_json, telegram)
            message_json["relations"].append({
                "name": " ",
                "from": create_unique_id(message),
                "to": create_unique_id(telegram),
            })

        puml_models[message_name] = message_json
    return puml_models


def resolve_fault_tree(wrapper: SparQLWrapper, dot_model: object, p_event: URIRef) -> str:
    name = wrapper.get_single_object_property(p_event, RDFS.label)
    gates = wrapper.get_out_references(p_event, FTANS.hasGate)

    node = "ellipses"
    if len(gates) > 0:
        node = "rectangles"

    dot_model[node].append({
        "id": get_id(p_event),
        "label": name
    })

    for p_gate in gates:
        gate_type = wrapper.get_type(p_gate)

        if gate_type == FTANS.OrGate:
            dot_model["orgates"].append({
                "id": get_id(p_gate)
            })
        elif gate_type == FTANS.AndGate:
            dot_model["andgates"].append({
                "id": get_id(p_gate)
            })
        else:
            raise ValueError(f"Unknown gate type {gate_type}")

        dot_model["relations"].append({
            "from": get_id(p_event),
            "to": get_id(p_gate)
        })

        for p_child_event in wrapper.get_out_references(p_gate, FTANS.hasEvent):
            resolve_fault_tree(wrapper, dot_model, p_child_event)
            dot_model["relations"].append({
                "from": get_id(p_gate),
                "to": get_id(p_child_event)
            })


def fta2json(graph: Graph) -> Dict[str, DotModel]:
    dot_fta = {}

    wrapper = SparQLWrapper(graph)

    for p_top_event in wrapper.get_instances_of_type(FTANS.TopEvent):
        name = wrapper.get_single_object_property(p_top_event, RDFS.label)

        dot_model = {
            "title": name,
            "description": f"Fehlerbaum {name}",
            "image": f"faulttree-{kebab_case(name)}.svg",
            "or_gate_icon": "or_gate-64p.png",
            "and_gate_icon": "and_gate-64p.png",
            "rectangles": [],
            "ellipses": [],
            "andgates": [],
            "orgates": [],
            "relations": []
        }
        resolve_fault_tree(wrapper, dot_model, p_top_event)
        dot_fta[name] = dot_model

    return dot_fta


class DocumentationGeneration:

    def __init__(self, graph: Graph):
        self.graph = graph
        self.wrapper = GraphWrapper(graph)
        self.json_to_rdf_mapper = JsonToRdfMapper(self.wrapper)

    def _create_target_asset(self, rdf_directory, rdf_template, name, filename, context):
        rdf_asset = self.wrapper.add_labeled_instance(ANS.Asset, name)
         
        rdf_target = self.wrapper.add_labeled_instance(ANS.Target, name)
        self.wrapper.add_str_property(ANS.filename, rdf_target, filename)
        self.wrapper.add_reference(ANS.hasDirectory, rdf_target, rdf_directory)

        self.wrapper.add_reference(ANS.hasTarget, rdf_asset, rdf_target)
        self.wrapper.add_reference(ANS.hasTemplate, rdf_asset, rdf_template)

        rdf_configuration = self.json_to_rdf_mapper.apply(context, name)
        self.wrapper.add_reference(ANS.hasConfiguration, rdf_asset, rdf_configuration)

    def apply(self, rdf_asset_root_dir, rdf_asset_sandbox_dir, documentation_path):
        print(f'Create documentation from system and data model definition into folder "{documentation_path}"')

        puml = rdf2puml(self.graph)
        puml.serialize(os.path.join(documentation_path, "flexidug.puml"))

        # === Directory ===
        directory_builder = DirectoryBuilder(self.wrapper)
        rdf_asset_documentation_dir = directory_builder.get_directory(rdf_asset_root_dir, "docs", key="Generated")

        # Documentation Templates
        rdf_index_template = self.wrapper.add_labeled_instance(ANS.Template, "index template")
        self.wrapper.add_str_property(ANS.filename, rdf_index_template, "adoc/index.adoc.mustache")

        rdf_package_puml_template = self.wrapper.add_labeled_instance(ANS.Template, "packages puml template")
        self.wrapper.add_str_property(ANS.filename, rdf_package_puml_template, "adoc/packages.puml.mustache")

        rdf_component_template = self.wrapper.add_labeled_instance(ANS.Template, "component template")
        self.wrapper.add_str_property(ANS.filename, rdf_component_template, "adoc/component.adoc.mustache")

        rdf_component_puml_template = self.wrapper.add_labeled_instance(ANS.Template, "component puml template")
        self.wrapper.add_str_property(ANS.filename, rdf_component_puml_template, "adoc/component.puml.mustache")

        rdf_message_template = self.wrapper.add_labeled_instance(ANS.Template, "message template")
        self.wrapper.add_str_property(ANS.filename, rdf_message_template, "adoc/message.adoc.mustache")

        rdf_message_puml_template = self.wrapper.add_labeled_instance(ANS.Template, "message puml template")
        self.wrapper.add_str_property(ANS.filename, rdf_message_puml_template, "adoc/message.puml.mustache")

        rdf_statemachine_template = self.wrapper.add_labeled_instance(ANS.Template, "statemachine template")
        self.wrapper.add_str_property(ANS.filename, rdf_statemachine_template, "adoc/statemachine.adoc.mustache")

        rdf_statemachine_puml_template = self.wrapper.add_labeled_instance(ANS.Template, "statemachine puml template")
        self.wrapper.add_str_property(ANS.filename, rdf_statemachine_puml_template, "adoc/statemachine.puml.mustache")

        rdf_faulttree_template = self.wrapper.add_labeled_instance(ANS.Template, "faulttree template")
        self.wrapper.add_str_property(ANS.filename, rdf_faulttree_template, "adoc/faulttree.adoc.mustache")

        rdf_faulttree_dot_template = self.wrapper.add_labeled_instance(ANS.Template, "faulttree dot template")
        self.wrapper.add_str_property(ANS.filename, rdf_faulttree_dot_template, "adoc/faulttree.dot.mustache")

        # Index adoc-File und Packages puml-File

        package_json = packages2json(self.graph)
        self._create_target_asset(rdf_asset_documentation_dir, rdf_index_template, "Documentation Index", filename="README.adoc", context=package_json)
        self._create_target_asset(rdf_asset_documentation_dir, rdf_package_puml_template, "Packages", filename="packages.puml", context=package_json)

        # Komponenten adoc + svg
        for component_name, component_json in component2json(self.graph).items():
            self._create_target_asset(rdf_asset_documentation_dir, rdf_component_template, f"{component_name} ReadMe", filename=f"component-{kebab_case(component_name)}.adoc", context=component_json)
            self._create_target_asset(rdf_asset_documentation_dir, rdf_component_puml_template, f"{component_name} Puml", filename=f"component-{kebab_case(component_name)}.puml", context=component_json)

        # Messages
        for message_name, message_json in messages2json(self.graph).items():
            self._create_target_asset(rdf_asset_documentation_dir, rdf_message_template, f"{message_name} ReadMe", filename=f"message-{kebab_case(message_name)}.adoc", context=message_json)
            self._create_target_asset(rdf_asset_documentation_dir, rdf_message_puml_template, f"{message_name} Puml", filename=f"message-{kebab_case(message_name)}.puml", context=message_json)
       
        # State machines
        for statemachine_name, statemachine_json in statemachines2json(self.graph).items():
            self._create_target_asset(rdf_asset_documentation_dir, rdf_statemachine_template, f"{statemachine_name} ReadMe", filename=f"implementation-{kebab_case(statemachine_name)}.adoc", context=statemachine_json)
            self._create_target_asset(rdf_asset_documentation_dir, rdf_statemachine_puml_template, f"{statemachine_name} Puml", filename=f"implementation-{kebab_case(statemachine_name)}.puml", context=statemachine_json)

        # Fault trees
        icon_images = []
        for faulttree_name, faulttree_json in fta2json(self.graph).items():
            for k in ['or_gate_icon', 'and_gate_icon']:
                if faulttree_json[k] not in icon_images: 
                    icon_images.append(faulttree_json[k])
            self._create_target_asset(rdf_asset_documentation_dir, rdf_faulttree_template, f"{faulttree_name} ReadMe", filename=f"faulttree-{kebab_case(faulttree_name)}.adoc", context=faulttree_json)
            self._create_target_asset(rdf_asset_documentation_dir, rdf_faulttree_dot_template, f"{faulttree_name} dot", filename=f"faulttree-{kebab_case(faulttree_name)}.dot", context=faulttree_json)

        # Copy png's
        # Asset that is copied from source
        rdf_asset_icon_dir = directory_builder.get_directory(rdf_asset_documentation_dir, "icons", key="Generated")
        rdf_source_icon_directory = directory_builder.get_directory(rdf_asset_sandbox_dir, "icons", key="Sandbox")

        for image in icon_images:
            rdf_asset = self.wrapper.add_labeled_instance(ANS.Asset, f"Copied {image}")

            rdf_target = self.wrapper.add_labeled_instance(ANS.Target, f"Copied {image}")
            self.wrapper.add_str_property(ANS.filename, rdf_target, image)
            self.wrapper.add_reference(ANS.hasDirectory, rdf_target, rdf_asset_icon_dir)
            self.wrapper.add_reference(ANS.hasTarget, rdf_asset, rdf_target)

            rdf_source = self.wrapper.add_labeled_instance(ANS.Source, f"Copied {image}")
            self.wrapper.add_str_property(ANS.filename, rdf_source, image)
            self.wrapper.add_reference(ANS.hasDirectory, rdf_source, rdf_source_icon_directory)
            self.wrapper.add_reference(ANS.hasSource, rdf_asset, rdf_source)
