from typing import List
from rdflib import URIRef, RDFS
from obse.graphwrapper import GraphWrapper
from util.statemachine.namespace import SMNS


class StatemachineDefinition:

    def __init__(self,wrapper : GraphWrapper):
        self.wrapper = wrapper

    def _add_state(self,rdf_statemachine : URIRef,state : str,init_state = False):
        if init_state:
            p_state = self.wrapper.add_labeled_instance(SMNS.InitialState,state)
        else:
            p_state = self.wrapper.add_labeled_instance(SMNS.State,state)

        self.wrapper.add_reference(SMNS.contains,rdf_statemachine,p_state)
        return p_state

    def _add_transition(self,rdf_statemachine : URIRef,transition : str,p_state_source : URIRef, p_state_target : URIRef,):
        p_transition = self.wrapper.add_labeled_instance(SMNS.Transition,transition)
        self.wrapper.add_reference(SMNS.contains,rdf_statemachine,p_transition)
        self.wrapper.add_reference(SMNS.source,p_transition,p_state_source)
        self.wrapper.add_reference(SMNS.target,p_transition,p_state_target)
        return p_transition

    def _create_event(self,rdf_statemachine : URIRef,event : str):
        p_event = self.wrapper.add_labeled_instance(SMNS.Event,event)
        self.wrapper.add_reference(SMNS.contains,rdf_statemachine,p_event)
        return p_event

    def _add_event_ref(self,p_event : URIRef,p_transition : URIRef):
        self.wrapper.add_reference(SMNS.transitionEvent,p_transition,p_event)
        return p_event
  
    def _add_action(self,rdf_statemachine : URIRef,action : str,p_transition : URIRef):
        p_action = self.wrapper.add_labeled_instance(SMNS.Action,action)
        self.wrapper.add_reference(SMNS.contains,rdf_statemachine,p_action)
        self.wrapper.add_reference(SMNS.transitionAction,p_transition,p_action)
        return p_action

    def _set_action_steps(self,p_action : URIRef,steps : List[str]):
        text = "\n".join(steps)
        self.wrapper.add_str_property(RDFS.comment,p_action,text)

    def _add_guard(self,rdf_statemachine : URIRef,guard : str,p_transition : URIRef):
        p_guard = self.wrapper.add_labeled_instance(SMNS.Guard,guard)
        self.wrapper.add_reference(SMNS.contains,rdf_statemachine,p_guard)
        self.wrapper.add_reference(SMNS.transitionGuard,p_transition,p_guard)
        return p_guard

    def apply(self):

        rdf = {}

        rdf['train_supervision_statemachine'] = self.wrapper.add_labeled_instance(SMNS.StateMachine,"Train Supervision")


        #########################
        # States                #
        #########################


        rdf_state_init = self._add_state(rdf['train_supervision_statemachine'],"Init",init_state= True)
        
        rdf_state_error = self._add_state(rdf['train_supervision_statemachine'],"Error")

        
        rdf_state_movement = self._add_state(rdf['train_supervision_statemachine'],"InMovement")
        # Conditions
        # meldestelle_from, meldestelle_to sind gesetzt
        # meldestelle, caller_id sind None

        
        rdf_state_arrival= self._add_state(rdf['train_supervision_statemachine'],"Arrival")
        # Conditions
        # caller_id, arrival timeout sind gesetzt

        rdf_state_position = self._add_state(rdf['train_supervision_statemachine'],"ConfirmedPosition")
        # Conditions
        # meldestelle ist gesetzt
        # caller_id sind None
        # meldestelle_from, meldestelle_to sind gesetzt oder führen zu FreeRoute Request

        rdf_state_free_route = self._add_state(rdf['train_supervision_statemachine'],"FreeRoute")

        rdf_state_driving_request = self._add_state(rdf['train_supervision_statemachine'],"DrivingRequest")
        rdf_state_driving_permission = self._add_state(rdf['train_supervision_statemachine'],"DrivingPermission")


        rdf_state_set_position = self._add_state(rdf['train_supervision_statemachine'],"SetPosition")
        rdf_state_delete_position = self._add_state(rdf['train_supervision_statemachine'],"DeletePosition")
        rdf_state_deregistered = self._add_state(rdf['train_supervision_statemachine'],"Deregistered")



        #########################
        # Events                #
        #########################

        ## Timer
        p_event_timer = self._create_event(rdf['train_supervision_statemachine'],"Timer")
        ## Messages
        p_event_registration_msg = self._create_event(rdf['train_supervision_statemachine'],"RegistrationMessage")
        p_event_arrival_msg = self._create_event(rdf['train_supervision_statemachine'],"ArrivalMessage")
        p_event_positive_confirmation_msg = self._create_event(rdf['train_supervision_statemachine'],"PositiveConfirmationMessage")
        p_event_negative_confirmation_msg = self._create_event(rdf['train_supervision_statemachine'],"NegativeConfirmationMessage")
        p_event_set_route_response_msg = self._create_event(rdf['train_supervision_statemachine'],"SetRouteResponseMessage")
        p_event_free_route_response_msg = self._create_event(rdf['train_supervision_statemachine'],"FreeRouteResponseMessage")
        p_event_driving_request_msg = self._create_event(rdf['train_supervision_statemachine'],"DrivingRequestMessage")
        p_event_driving_permission_replay_msg = self._create_event(rdf['train_supervision_statemachine'],"DrivingPermissionReplayMessage")

        ## Others - System or Manual Events
        p_event_deregister = self._create_event(rdf['train_supervision_statemachine'],"Deregister")

        # Anmeldung / Registration
        # Init -> inMovement

        # Wird Zug 4711 angenommen?
        # Zug 4711 ja.


        rdf_transition_registration = self._add_transition(rdf['train_supervision_statemachine'],"Registration",rdf_state_init,rdf_state_movement)
        self._add_event_ref(p_event_registration_msg,rdf_transition_registration)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"Registration",rdf_transition_registration) #Bestätigung
        self._set_action_steps(p_action,["Send RegistrationConfirmation message","Set train number"])

        # Ankunftsmeldung
        # InMovement -> Arrival -> ConfirmedPosition
        # Dialog:
        # ZF:   Zug 4711 in Jänschwalde
        # ZL: Ich wiederhole. Zug 4711 in Jänschwalde
        # ZF:   Ja. Richtig.

        rdf_transition_arrival = self._add_transition(rdf['train_supervision_statemachine'],"Arrival",rdf_state_movement,rdf_state_arrival)
        self._add_event_ref(p_event_arrival_msg,rdf_transition_arrival)
        self._add_guard(rdf['train_supervision_statemachine'],"ValidArrivalMessage",rdf_transition_arrival)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"Arrival",rdf_transition_arrival)
        self._set_action_steps(p_action,["Send ArrivalReplay message","set Meldestelle","set ArrivalTimeout","set CallerId"])

    	#Timeout for ArrivalReplayMessage
        rdf_transition_arrival_timeout = self._add_transition(rdf['train_supervision_statemachine'],"ArrivalTimeout",rdf_state_arrival,rdf_state_movement)
        self._add_event_ref(p_event_timer,rdf_transition_arrival_timeout)
        self._add_guard(rdf['train_supervision_statemachine'],"ArrivalTimeout",rdf_transition_arrival_timeout)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"ArrivalTimeout",rdf_transition_arrival_timeout)
        self._set_action_steps(p_action,["reset ArrivalTimeout","reset CallerId","reset Meldestelle"])

        rdf_transition_arrival_confirmed = self._add_transition(rdf['train_supervision_statemachine'],"ArrivalConfirmed",rdf_state_arrival,rdf_state_position)
        self._add_event_ref(p_event_positive_confirmation_msg,rdf_transition_arrival_confirmed)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"PositivArrivalConfirmation",rdf_transition_arrival_confirmed)
        self._set_action_steps(p_action,["reset ArrivalTimeout","reset CallerId","set Meldestelle in DigitalBelegblatt"])

        rdf_transition_arrival_deviated = self._add_transition(rdf['train_supervision_statemachine'],"ArrivalDeviated",rdf_state_arrival,rdf_state_movement)
        self._add_event_ref(p_event_negative_confirmation_msg,rdf_transition_arrival_deviated)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"NegativArrivalConfirmation",rdf_transition_arrival_deviated)
        self._set_action_steps(p_action,["reset ArrivalTimeout","reset CallerId","reset Meldestelle"])


        
        # Route löschen falls gesetzt mit Positive Bestätigung, Negative Bestätigung oder Timeout
        # ConfirmedPosition -> FreeRoute -> ConfirmedPosition

        rdf_transition_free_route = self._add_transition(rdf['train_supervision_statemachine'],"FreeRoute",rdf_state_position,rdf_state_free_route)
        self._add_event_ref(p_event_timer,rdf_transition_free_route)
        self._add_guard(rdf['train_supervision_statemachine'],"ValidRoute",rdf_transition_free_route)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"FreeRoute",rdf_transition_free_route)
        self._set_action_steps(p_action,["set FreeRouteTimeout","send FreeRoute"])

        # positive Bestätigung
        rdf_transition_free_route_response_pos = self._add_transition(rdf['train_supervision_statemachine'],"PositiveFreeRouteResponse",rdf_state_free_route,rdf_state_position)
        self._add_event_ref(p_event_free_route_response_msg,rdf_transition_free_route_response_pos)
        p_guard_positive_free_route_response = self._add_guard(rdf['train_supervision_statemachine'],"PositiveFreeRouteResponse",rdf_transition_free_route_response_pos)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"PositiveFreeRouteResponse",rdf_transition_free_route_response_pos)
        self._set_action_steps(p_action,["reset FreeRouteTimeout","reset meldestelle_from and meldestelle_to"])

        # negative Bestätigung (System-Fehler)
        # Guard NegativeFreeRouteResponse => Not PositiveFreeRouteResponse
        rdf_transition_free_route_response_neg = self._add_transition(rdf['train_supervision_statemachine'],"NegativeFreeRouteResponse",rdf_state_free_route,rdf_state_position)
        self._add_event_ref(p_event_free_route_response_msg,rdf_transition_free_route_response_neg)
        p_guard_negative_free_route_response = self._add_guard(rdf['train_supervision_statemachine'],"NegativeFreeRouteResponse",rdf_transition_free_route_response_neg)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"NegativeFreeRouteResponse",rdf_transition_free_route_response_neg)
        self._set_action_steps(p_action,["reset FreeRouteTimeout","reset meldestelle_from and meldestelle_to","Send System Fehler"])


        #Timeout for FreeRoute (System-Fehler)
        rdf_transition_free_route_timeout = self._add_transition(rdf['train_supervision_statemachine'],"FreeRouteTimeout",rdf_state_free_route,rdf_state_position)
        self._add_event_ref(p_event_timer,rdf_transition_free_route_timeout)
        p_guard_free_route_timeout = self._add_guard(rdf['train_supervision_statemachine'],"FreeRouteTimeout",rdf_transition_free_route_timeout)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"FreeRouteTimeout",rdf_transition_free_route_timeout)
        self._set_action_steps(p_action,["reset FreeRouteTimeout","reset meldestelle_from and meldestelle_to","Send System Fehler"])

        # Fahranfrage
        # ConfirmedPosition -> DrivingRequest -> DrivingPosition -> InMovement
        # Dialog:
        # ZF:  Darf Zug 4711 bis Schwarze Pumpe fahren?
        # ZL: 4711 darf bis Schwarze Pumpe fahren.
        # ZF: Ich wiederhole. Zug 4711 darf bis schwarze Pumpe fahren
        # ZL: Ja. Richtig

        rdf_transition_driving_request = self._add_transition(rdf['train_supervision_statemachine'],"DrivingRequest",rdf_state_position,rdf_state_driving_request)
        self._add_event_ref(p_event_driving_request_msg,rdf_transition_driving_request)
        self._add_guard(rdf['train_supervision_statemachine'],"ValidDrivingRequest",rdf_transition_driving_request)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"SetRoute",rdf_transition_driving_request)
        self._set_action_steps(p_action,["send SetRouteRequest","set SetRoute Timeout","set meldestelle_from and meldestelle_to","set CallerId"])

        # positive Bestätigung
        rdf_transition_driving_permission = self._add_transition(rdf['train_supervision_statemachine'],"DrivingPermission",rdf_state_driving_request,rdf_state_driving_permission)
        self._add_event_ref(p_event_set_route_response_msg,rdf_transition_driving_permission)
        p_guard_positive_set_route_response = self._add_guard(rdf['train_supervision_statemachine'],"PositiveSetRouteResponse",rdf_transition_driving_permission)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"PositiveSetRouteResponse",rdf_transition_driving_permission)
        self._set_action_steps(p_action,["reset SetRouteTimeout","send DrivingPermission","set DrivingPermissionTimeout"])

        # negative Bestätigung (Bitte Warten)
        rdf_transition_try_again_later = self._add_transition(rdf['train_supervision_statemachine'],"TryAgainLater",rdf_state_driving_request,rdf_state_position)
        self._add_event_ref(p_event_set_route_response_msg,rdf_transition_try_again_later)
        p_guard_negative_set_route_response = self._add_guard(rdf['train_supervision_statemachine'],"NegativeSetRouteResponse",rdf_transition_try_again_later)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"NegativeSetRouteResponse",rdf_transition_try_again_later)
        self._set_action_steps(p_action,["reset SetRouteTimeout","send WaitingNotification","reset meldestelle_from and meldestelle_to","reset CallerId"])


        #Timeout for DrivingRequest
        rdf_transition_driving_request_timeout = self._add_transition(rdf['train_supervision_statemachine'],"DrivingRequestTimeout",rdf_state_driving_request,rdf_state_position)
        self._add_event_ref(p_event_timer,rdf_transition_driving_request_timeout)
        p_guard_set_route_timeout = self._add_guard(rdf['train_supervision_statemachine'],"SetRouteTimeout",rdf_transition_driving_request_timeout)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"ClearDrivingRequest",rdf_transition_driving_request_timeout)
        self._set_action_steps(p_action,["reset SetRouteTimeout","reset meldestelle_from and meldestelle_to","reset CallerId"])


        # Fahrbefehl Wiederholung (richtig)
        rdf_transition_driving_permission_confirmed = self._add_transition(rdf['train_supervision_statemachine'],"CorrectDrivingPermissionReplay",rdf_state_driving_permission,rdf_state_movement)
        self._add_guard(rdf['train_supervision_statemachine'],"CorrectDrivingPermission",rdf_transition_driving_permission_confirmed)
        self._add_event_ref(p_event_driving_permission_replay_msg,rdf_transition_driving_permission_confirmed)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"CorrectDrivingPermissionReplay",rdf_transition_driving_permission_confirmed)
        self._set_action_steps(p_action,["reset DrivingPermissionTimeout","Send PositivConfirmation","reset CallerId"])

        # Fahrbefehl Wiederholung (falsch)
        rdf_transition_driving_permission_deviated = self._add_transition(rdf['train_supervision_statemachine'],"WrongDrivingPermissionReplay",rdf_state_driving_permission,rdf_state_free_route)
        self._add_guard(rdf['train_supervision_statemachine'],"WrongDrivingPermission",rdf_transition_driving_permission_deviated)
        self._add_event_ref(p_event_driving_permission_replay_msg,rdf_transition_driving_permission_deviated)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"WrongDrivingPermissionReplay",rdf_transition_driving_permission_deviated)
        self._set_action_steps(p_action,["reset DrivingPermissionTimeout","Send NegativConfirmation","reset CallerId","send free route","set free route timeout"])

        #Timeout for DrivingPermission
        rdf_transition_driving_permission_timeout = self._add_transition(rdf['train_supervision_statemachine'],"DrivingPermissionTimeout",rdf_state_driving_permission,rdf_state_free_route)
        self._add_event_ref(p_event_timer,rdf_transition_driving_permission_timeout)
        self._add_guard(rdf['train_supervision_statemachine'],"DrivingPermissionTimeout",rdf_transition_driving_permission_timeout)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"DrivingPermissionTimeout",rdf_transition_driving_permission_timeout)
        self._set_action_steps(p_action,["reset DrivingPermissionTimeout","reset CallerId"])


        # Route setzen falls erste Ankunft
        # ConfirmedPosition -> SetPosition -> ConfirmedPosition

        rdf_transition_set_position = self._add_transition(rdf['train_supervision_statemachine'],"SetPosition",rdf_state_position,rdf_state_set_position)
        self._add_event_ref(p_event_timer,rdf_transition_set_position)
        self._add_guard(rdf['train_supervision_statemachine'],"FirstArrival",rdf_transition_set_position)   
        p_action = self._add_action(rdf['train_supervision_statemachine'],"SetPosition",rdf_transition_set_position)
        self._set_action_steps(p_action,["set SetRouteTimeout","send SetRoute"])

        # positive Bestätigung
        rdf_transition_set_position_response_pos = self._add_transition(rdf['train_supervision_statemachine'],"PositiveSetPosition",rdf_state_set_position,rdf_state_position)
        self._add_event_ref(p_event_set_route_response_msg,rdf_transition_set_position_response_pos)
        self.wrapper.add_reference(SMNS.transitionGuard,rdf_transition_set_position_response_pos,p_guard_positive_set_route_response)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"PositiveSetPosition",rdf_transition_set_position_response_pos)
        self._set_action_steps(p_action,["reset SetRouteTimeout","set first arrival false"])

        # negative Bestätigung (System-Fehler)
        # Guard NegativeFreeRouteResponse => Not PositiveFreeRouteResponse
        rdf_transition_set_position_response_neg = self._add_transition(rdf['train_supervision_statemachine'],"NegativeSetPosition",rdf_state_set_position,rdf_state_error)
        self._add_event_ref(p_event_set_route_response_msg,rdf_transition_set_position_response_neg)
        self.wrapper.add_reference(SMNS.transitionGuard,rdf_transition_set_position_response_neg,p_guard_negative_set_route_response)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"NegativeSetPosition",rdf_transition_set_position_response_neg)
        self._set_action_steps(p_action,["reset SetRouteTimeout","Send System Fehler"])


        #Timeout for SetRoute (System-Fehler)
        rdf_transition_set_position_timeout = self._add_transition(rdf['train_supervision_statemachine'],"SetPositionTimeout",rdf_state_set_position,rdf_state_error)
        self._add_event_ref(p_event_timer,rdf_transition_set_position_timeout)
        self.wrapper.add_reference(SMNS.transitionGuard,rdf_transition_set_position_timeout,p_guard_set_route_timeout)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"SetPositionTimeout",rdf_transition_set_position_timeout)
        self._set_action_steps(p_action,["reset SetRouteTimeout","Send System Fehler"])



        # Abmeldung Route löschen und Zug deregistrieren
        # ConfirmedPosition -> DeletePosition -> Deregistered

        rdf_transition_delete_position = self._add_transition(rdf['train_supervision_statemachine'],"DeletePosition",rdf_state_position,rdf_state_delete_position)
        self._add_event_ref(p_event_deregister,rdf_transition_delete_position)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"DeletePosition",rdf_transition_delete_position)
        self._set_action_steps(p_action,["set FreeRouteTimeout","send FreeRoute"])

        # Automatische Abmeldung in Endstation
        rdf_transition_automatic_delete_position = self._add_transition(rdf['train_supervision_statemachine'],"AutomaticDeletePosition",rdf_state_position,rdf_state_delete_position)
        self._add_event_ref(p_event_timer, rdf_transition_automatic_delete_position)
        self._add_guard(rdf['train_supervision_statemachine'],"LastArrival",rdf_transition_automatic_delete_position)   
        p_action = self._add_action(rdf['train_supervision_statemachine'],"AutomaticDeletePosition",rdf_transition_automatic_delete_position)
        self._set_action_steps(p_action,["set FreeRouteTimeout","send FreeRoute"])

        # positive Bestätigung
        rdf_transition_delete_position_response_pos = self._add_transition(rdf['train_supervision_statemachine'],"PositiveDeletePosition",rdf_state_delete_position,rdf_state_deregistered)
        self._add_event_ref(p_event_free_route_response_msg,rdf_transition_delete_position_response_pos)
        self.wrapper.add_reference(SMNS.transitionGuard,rdf_transition_delete_position_response_pos,p_guard_positive_free_route_response)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"PositiveDeletePosition",rdf_transition_delete_position_response_pos)
        self._set_action_steps(p_action,["reset FreeRouteTimeout","reset meldestelle_from and meldestelle_to"])

        # negative Bestätigung (System-Fehler)
        # Guard NegativeFreeRouteResponse => Not PositiveFreeRouteResponse
        rdf_transition_delete_position_response_neg = self._add_transition(rdf['train_supervision_statemachine'],"NegativeDeletePosition",rdf_state_delete_position,rdf_state_error)
        self._add_event_ref(p_event_free_route_response_msg,rdf_transition_delete_position_response_neg)
        self.wrapper.add_reference(SMNS.transitionGuard,rdf_transition_delete_position_response_neg,p_guard_negative_free_route_response)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"NegativeDeletePosition",rdf_transition_delete_position_response_neg)
        self._set_action_steps(p_action,["reset FreeRouteTimeout","Send System Fehler"])


        #Timeout for FreeRoute (System-Fehler)
        rdf_transition_delete_position_timeout = self._add_transition(rdf['train_supervision_statemachine'],"DeletePositionTimeout",rdf_state_delete_position,rdf_state_error)
        self._add_event_ref(p_event_timer,rdf_transition_delete_position_timeout)
        self.wrapper.add_reference(SMNS.transitionGuard,rdf_transition_delete_position_timeout,p_guard_free_route_timeout)
        p_action = self._add_action(rdf['train_supervision_statemachine'],"DeletePositionTimeout",rdf_transition_delete_position_timeout)
        self._set_action_steps(p_action,["reset FreeRouteTimeout","Send System Fehler"])


        return rdf