from rdflib import URIRef
from obse.graphwrapper import GraphWrapper
from util.configuration.namespace import CNS
from .system_definition import SimulationMode

class ConfigurationDefinition:

    def __init__(self, wrapper: GraphWrapper):
        self.wrapper = wrapper

    def apply(self, simulation_mode):

        rdf = {}

        # kubernetes Configuration
        rdf['kubernetes'] = self.wrapper.add_labeled_instance(CNS.KubernetesConfiguration, "Kubernetes Configuration")
        self.wrapper.add_str_property(CNS.namespace, rdf['kubernetes'], "zlic")

        # AWS Credentials
        rdf['polly_credentials'] = self.wrapper.add_labeled_instance(CNS.CredentialConfiguration, "Polly User")
        self.wrapper.add_str_property(CNS.credentialFilename, rdf['polly_credentials'], ".config/credentials.polly_user")

        # Endpoint Configuration
        rdf['sip_web_endpoint'] = self.wrapper.add_labeled_instance(CNS.EndpointConfiguration, "SIP Web Interface")
        self.wrapper.add_integer_property(CNS.portNumber, rdf['sip_web_endpoint'], 5007)
        self.wrapper.add_str_property(CNS.hostName, rdf['sip_web_endpoint'], "sip.zlic.lab.osmhpi.de")
        self.wrapper.add_str_property(CNS.environmentNamePortNumber, rdf['sip_web_endpoint'], "SERVER_PORT")

        rdf['belegblatt_web_endpoint'] = self.wrapper.add_labeled_instance(CNS.EndpointConfiguration, "Belegblatt Web Interface")
        self.wrapper.add_integer_property(CNS.portNumber, rdf['belegblatt_web_endpoint'], 5008)
        self.wrapper.add_str_property(CNS.hostName, rdf['belegblatt_web_endpoint'], "zlic.lab.osmhpi.de")
        self.wrapper.add_str_property(CNS.environmentNamePortNumber, rdf['belegblatt_web_endpoint'], "SERVER_PORT")

        rdf['nothalt_web_endpoint'] = self.wrapper.add_labeled_instance(CNS.EndpointConfiguration,
                                                                        "Nothalt Web Interface")
        self.wrapper.add_integer_property(CNS.portNumber, rdf['nothalt_web_endpoint'], 5010)
        self.wrapper.add_str_property(CNS.hostName, rdf['nothalt_web_endpoint'], "emergency.lab.osmhpi.de")
        self.wrapper.add_str_property(CNS.environmentNamePortNumber, rdf['nothalt_web_endpoint'], "SERVER_PORT")

        if simulation_mode == SimulationMode.INFRASTRUCTURE:
            rdf['infrastructure_simulator_web_endpoint'] = self.wrapper.add_labeled_instance(CNS.EndpointConfiguration, "Infrastructure Simulator Web Interface")
            self.wrapper.add_integer_property(CNS.portNumber, rdf['infrastructure_simulator_web_endpoint'], 5009)
            self.wrapper.add_str_property(CNS.hostName, rdf['infrastructure_simulator_web_endpoint'], "sim.zlic.lab.osmhpi.de")
            self.wrapper.add_str_property(CNS.environmentNamePortNumber, rdf['infrastructure_simulator_web_endpoint'], "SERVER_PORT")

        if simulation_mode == SimulationMode.SUMO:
            rdf['sumo_simulation_endpoint'] = self.wrapper.add_labeled_instance(CNS.EndpointConfiguration, "SUMO Simulation")
            self.wrapper.add_integer_property(CNS.portNumber, rdf['sumo_simulation_endpoint'], 9090)
            self.wrapper.add_str_property(CNS.hostName, rdf['sumo_simulation_endpoint'], "sumo.lab.osmhpi.de")
            self.wrapper.add_str_property(CNS.environmentNamePortNumber, rdf['sumo_simulation_endpoint'], "SERVER_PORT")

            rdf['sumo_automatic_trains_web_endpoint'] = self.wrapper.add_labeled_instance(CNS.EndpointConfiguration,
                                                                                          "SUMO Automatic Trains Web Interface")
            self.wrapper.add_integer_property(CNS.portNumber, rdf['sumo_automatic_trains_web_endpoint'], 5012)
            self.wrapper.add_str_property(CNS.hostName, rdf['sumo_automatic_trains_web_endpoint'], "sumo-automatic-trains.lab.osmhpi.de")
            self.wrapper.add_str_property(CNS.environmentNamePortNumber, rdf['sumo_automatic_trains_web_endpoint'], "WEBSERVER_PORT")

        rdf['tracing_web_endpoint'] = self.wrapper.add_labeled_instance(CNS.EndpointConfiguration, "Tracing Web Interface")
        self.wrapper.add_integer_property(CNS.portNumber, rdf['tracing_web_endpoint'], 5011)
        self.wrapper.add_str_property(CNS.hostName, rdf['tracing_web_endpoint'], "tracing.lab.osmhpi.de")
        self.wrapper.add_str_property(CNS.environmentNamePortNumber, rdf['tracing_web_endpoint'], "SERVER_PORT")

        return rdf
