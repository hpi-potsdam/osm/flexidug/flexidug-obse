from rdflib import URIRef
from obse.graphwrapper import GraphWrapper
from obse.sparql_queries import SparQLWrapper

# lokal imports
from util.namespaces import SNS
from util.telegram.namespace import TNS

from util.telegram.telegram_structure_builder import TelegramStructureBuilder


def cut_name(rdf_type: URIRef) -> str:
    s = str(rdf_type)
    return s.split("#")[1]


# Available simulation modes
class SimulationMode:
    NONE = 'none'
    SUMO = 'sumo'
    INFRASTRUCTURE = 'infrastructure'
    _choices = [NONE, SUMO, INFRASTRUCTURE]
    _default = NONE


class SystemDefinition:

    def __init__(self, wrapper: GraphWrapper, sparql_wrapper: SparQLWrapper):
        self.wrapper = wrapper
        self.sparql_wrapper = sparql_wrapper

    # Helper functions
    def _add_component(self, rdf_parent: URIRef, label, function) -> URIRef:
        rdf_elm = self.wrapper.add_labeled_instance(SNS.Component, label)
        self.wrapper.add_reference(SNS.hasComponent, rdf_parent, rdf_elm)

        rdf_function = self.wrapper.add_labeled_instance(SNS.Function, function)
        self.wrapper.add_reference(SNS.hasFunction, rdf_elm, rdf_function)

        return rdf_elm

    def _new_sequence(self, label: str):
        pass

    def _add_message(self, rdf_sender: URIRef, rdf_receiver: URIRef, label: str, message_name: str,
                     rdf_telegram: URIRef = None) -> URIRef:
        unique_name = f"{cut_name(rdf_sender)}-{cut_name(rdf_receiver)}-{message_name}"
        rdf_message = self.wrapper.add_labeled_instance(SNS.Message, label, unique_name)
        self.wrapper.add_str_property(SNS.messageName, rdf_message, message_name)
        self.wrapper.add_reference(SNS.hasSender, rdf_message, rdf_sender)
        self.wrapper.add_reference(SNS.hasReceiver, rdf_message, rdf_receiver)
        if rdf_telegram:
            self.wrapper.add_reference(TNS.hasStructure, rdf_message, rdf_telegram)

        return rdf_message

    def apply(self, simulation_mode):

        print(f"Simulation mode: '{simulation_mode}'")

        rdf = {}
        # Subsystem "Zug"
        rdf['train'] = self.wrapper.add_labeled_instance(SNS.Subsystem, "Zug", unique_name="train")

        rdf['radio_tf'] = self._add_component(rdf['train'], "TF-Funkgerät", function="Erlaubt die Kommunikation mit dem Zugleiter in the Cloud.")

        rdf_tablet = self._add_component(rdf['train'], "TF-Tablet", function="Übernimmt die Visualisierung des Digitalen Belegblatts.")

        # add Actor "Zugführer"
        rdf_traindriver = self.wrapper.add_labeled_instance(SNS.Actor, "Zugführer (TF)", unique_name="traindriver")
        self.wrapper.add_reference(SNS.usesComponent, rdf_traindriver, rdf['radio_tf'])
        self.wrapper.add_reference(SNS.usesComponent, rdf_traindriver, rdf_tablet)

        # Subsystem "Balise"
        rdf['balise'] = self.wrapper.add_labeled_instance(SNS.Subsystem, "Festdatenbalise",unique_name="balise")

        # Subsystem "FeldElement/Weiche"
        rdf['weiche'] = self.wrapper.add_labeled_instance(SNS.Subsystem, "FeldElement/Weiche")

        rdf['objectcontroller'] = self._add_component(rdf['weiche'], "ObjectController", function="Übernimmt die Kommunikation zu den Feldelementen.")
        rdf['weichenlagemelder'] = self._add_component(rdf['weiche'], "Weichenlagemelder", function="Weichenlage wird angezeigt.")

        # Subsystem "Zugleiter in the Cloud"
        rdf['zlic'] = self.wrapper.add_labeled_instance(SNS.Subsystem, "Zugleiter in the Cloud")

        rdf['radio_zlic'] = self._add_component(rdf['zlic'], "ZLIC-Funkgerät", function="Erlaubt die Kommunikation mit dem TF.")

        rdf['radioio'] = self._add_component(rdf['zlic'], "RadioIO", function="Konvertiert die Kommunikation zwischen Funkgerät und ZLic-API")

        rdf['sip'] = self._add_component(rdf['zlic'], "SIP Server", function="Stellt einen Websocket für den Austausch von Wav-Dateien zur Verfügung.")

        rdf['stt'] = self._add_component(rdf['zlic'], "SpeechToText", function="Konvertiert WAV-Dateien in Text.")

        rdf['tts'] = self._add_component(rdf['zlic'], "TextToSpeech", function="Konvertiert Text in WAV-Dateien.")
        
        rdf['nlu'] = self._add_component(rdf['zlic'], "NaturalLanguageUnderstanding", function="Erkennt Absichten (intends) im Text.")

        rdf['interlocking'] = self._add_component(rdf['zlic'], "Stellprozess", function="Prüfung und Reservierung der Strecke.")
        rdf['belegblatt'] = self._add_component(rdf['zlic'], "Digitales Belegblatt", function="Visualisiert die Belegung der Strecke.")

        rdf['nothalt'] = self._add_component(rdf['zlic'], "Nothalt", function="Gibt einen Nothaltauftrag ab.")

        rdf['tracing'] = self._add_component(rdf['zlic'], "Tracing", function="Loggt den Nachrichtenverkehr innerhalb des ZLiC")

        # Subsystem Simulation
        rdf['simulator'] = self.wrapper.add_labeled_instance(SNS.Subsystem, "Simulator")

        # Infrastuktursimulator
        if simulation_mode == SimulationMode.INFRASTRUCTURE:
            rdf['infrastructure-simulator'] = self._add_component(rdf['simulator'], "Infrastructure-Simulator", function="Simuliert die Infrastruktur über ein REST-Interface.")

        # SUMO Simulation
        if simulation_mode == SimulationMode.SUMO:
            rdf['sumo_simulation'] = self._add_component(rdf['simulator'], "SUMO Simulation", function="Fügt SUMO-simulierte Züge dem ZliC hinzu.")

        # Subsystem "Notfunk-System"
        # rdf_notfunk = self.wrapper.add_labeled_instance(SNS.Subsystem,"Notfunk-System")
        # Subsystem "Digitaler Zwilling"
        # rdf_dw = self.wrapper.add_named_instance(MBA.Subsystem,"DigitalTwin",unique_name="digitaltwin")

        # Telegramm's
        rdf_builder = TelegramStructureBuilder(self.wrapper, "Audio")
        rdf_builder.add_string_data_field("caller_id")
        rdf_builder.add_bytes_data_field("audio")
        rdf_audio_telegram = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "EmergencyAudio")
        rdf_builder.add_bytes_data_field("audio")
        rdf_nothalt_audio_telegram = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "Transcribed")
        rdf_builder.add_string_data_field("caller_id")
        rdf_builder.add_string_data_field("text")
        rdf_transcribed_telegram = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "Anmeldung")
        rdf_builder.add_string_data_field("caller_id")
        rdf_builder.add_integer_data_field("zugnummer")
        rdf_telegram_anmeldung = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "Fahranfrage")
        rdf_builder.add_string_data_field("caller_id")
        rdf_builder.add_integer_data_field("zugnummer")
        rdf_builder.add_string_data_field("meldestelle")
        rdf_telegram_fahranfrage = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "FahrbefehlWiederholung")
        rdf_builder.add_string_data_field("caller_id")
        rdf_builder.add_integer_data_field("zugnummer")
        rdf_builder.add_string_data_field("meldestelle")
        rdf_telegram_fahrbefehl_wdhl = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "Ankunftsmeldung")
        rdf_builder.add_string_data_field("caller_id")
        rdf_builder.add_integer_data_field("zugnummer")
        rdf_builder.add_string_data_field("meldestelle")
        rdf_telegram_ankunftsmeldung = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "PositiveBestaetigung")
        rdf_builder.add_string_data_field("caller_id")
        rdf_telegram_pos_bestaetigung = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "NegativeBestaetigung")
        rdf_builder.add_string_data_field("caller_id")
        rdf_telegram_neg_bestaetigung = rdf_builder.to_rdf()

        # Fahrbefehl / Fahrbefehl Bestätigung
        rdf_builder = TelegramStructureBuilder(self.wrapper, "Command")
        rdf_builder.add_string_data_field("caller_id")
        rdf_builder.add_string_data_field("command_type")
        rdf_builder.add_string_data_field("text")
        rdf_telegram_command = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "Route")
        rdf_builder.add_string_data_field("meldestelle_from")
        rdf_builder.add_string_data_field("meldestelle_to")
        rdf_telegram_route = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "SetRoute")
        rdf_builder.add_integer_data_field("zugnummer")
        rdf_builder.add_array_of_structure("routes", rdf_telegram_route)
        rdf_telegram_set_route = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "FreeRoute")
        rdf_builder.add_integer_data_field("zugnummer")
        rdf_builder.add_array_of_structure("routes", rdf_telegram_route)
        rdf_telegram_free_route = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "SetRouteResponse")
        rdf_builder.add_integer_data_field("zugnummer")
        rdf_builder.add_integer_data_field("status")
        rdf_telegram_set_route_response = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "FreeRouteResponse")
        rdf_builder.add_integer_data_field("zugnummer")
        rdf_builder.add_integer_data_field("status")
        rdf_telegram_free_route_response = rdf_builder.to_rdf()

        # Nothalt
        rdf_builder = TelegramStructureBuilder(self.wrapper, "EmergencyStopRequest")
        rdf_builder.add_string_data_field("caller_id")
        rdf_telegram_emergency_stop_request = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "EmergencyStop")
        rdf_builder.add_string_data_field("text")
        rdf_telegram_emergency_stop = rdf_builder.to_rdf()

        rdf_builder = TelegramStructureBuilder(self.wrapper, "EmergencyStopControl")
        rdf_builder.add_string_data_field("command")  # TODO enum
        rdf_telegram_emergency_stop_control = rdf_builder.to_rdf()

        # Messages / Sequenz

        # Funkgerät TF <--> Funkgerät ZL 
        self._add_message(rdf['radio_tf'],rdf['radio_zlic'],"Funkspruch vom TF",message_name="radio_tf")
        self._add_message(rdf['radio_zlic'],rdf['radio_tf'],"Funkspruch vom ZL",message_name="radio_zlic")

        # Funkgerät ZL <--> Glue
        self._add_message(rdf['radio_zlic'],rdf['radioio'],"Akustische Nachricht vom TF",message_name="sound_tf")
        self._add_message(rdf['radioio'],rdf['radio_zlic'],"Akustische Nachricht vom ZL",message_name="sound_zlic")

        # Funkgerät Glue <--> SIP
        self._add_message(rdf['radioio'],rdf['sip'],"WAV-File vom TF",message_name="wav_tf")
        self._add_message(rdf['sip'],rdf['radioio'],"WAV-File vom ZL",message_name="wav_zl")

        # SIP --> STT
        self._add_message(rdf['sip'],rdf['stt'],"WAV-File für Konvertierung in Text",message_name="TFAudio",rdf_telegram=rdf_audio_telegram)

        # SIP --> NLU
        self._add_message(rdf['stt'],rdf['nlu'],"Text-Nachricht vom TF",message_name="Transcribed",rdf_telegram=rdf_transcribed_telegram)

        if simulation_mode == SimulationMode.SUMO:
            # NLU --> SUMO-Simulation
            self._add_message(rdf['nlu'], rdf['sumo_simulation'], "Anmeldung", message_name="Anmeldung",
                              rdf_telegram=rdf_telegram_anmeldung)
            self._add_message(rdf['nlu'], rdf['sumo_simulation'], "Ankunftsmeldung", message_name="Ankunftsmeldung",
                              rdf_telegram=rdf_telegram_ankunftsmeldung)
            self._add_message(rdf['nlu'], rdf['sumo_simulation'], "Fahranfrage", message_name="Fahranfrage",
                              rdf_telegram=rdf_telegram_fahranfrage)
            self._add_message(rdf['nlu'], rdf['sumo_simulation'], "Fahrbefehlwiederholung",
                              message_name="FahrbefehlWiederholung", rdf_telegram=rdf_telegram_fahrbefehl_wdhl)
            self._add_message(rdf['nlu'], rdf['sumo_simulation'], "Negative Bestätigung", message_name="NegativeBestaetigung",
                              rdf_telegram=rdf_telegram_neg_bestaetigung)
            self._add_message(rdf['nlu'], rdf['sumo_simulation'], "Positive Bestätigung", message_name="PositiveBestaetigung",
                              rdf_telegram=rdf_telegram_pos_bestaetigung)

            # SUMO-Simulation --> Belegblatt
            self._add_message(rdf['sumo_simulation'], rdf['belegblatt'], "Anmeldung", message_name="Anmeldung",
                              rdf_telegram=rdf_telegram_anmeldung)
            self._add_message(rdf['sumo_simulation'], rdf['belegblatt'], "Ankunftsmeldung", message_name="Ankunftsmeldung",
                              rdf_telegram=rdf_telegram_ankunftsmeldung)
            self._add_message(rdf['sumo_simulation'], rdf['belegblatt'], "Fahranfrage", message_name="Fahranfrage",
                              rdf_telegram=rdf_telegram_fahranfrage)
            self._add_message(rdf['sumo_simulation'], rdf['belegblatt'], "Fahrbefehlwiederholung", message_name="FahrbefehlWiederholung",
                              rdf_telegram=rdf_telegram_fahrbefehl_wdhl)
            self._add_message(rdf['sumo_simulation'], rdf['belegblatt'], "Negative Bestätigung", message_name="NegativeBestaetigung",
                              rdf_telegram=rdf_telegram_neg_bestaetigung)
            self._add_message(rdf['sumo_simulation'], rdf['belegblatt'], "Positive Bestätigung", message_name="PositiveBestaetigung",
                              rdf_telegram=rdf_telegram_pos_bestaetigung)
        else:
            # NLU --> Belegblatt
            self._add_message(rdf['nlu'],rdf['belegblatt'],"Anmeldung",message_name="Anmeldung",rdf_telegram=rdf_telegram_anmeldung)
            self._add_message(rdf['nlu'],rdf['belegblatt'],"Ankunftsmeldung",message_name="Ankunftsmeldung",rdf_telegram=rdf_telegram_ankunftsmeldung)
            self._add_message(rdf['nlu'],rdf['belegblatt'],"Fahranfrage",message_name="Fahranfrage",rdf_telegram=rdf_telegram_fahranfrage)
            self._add_message(rdf['nlu'],rdf['belegblatt'],"Fahrbefehlwiederholung",message_name="FahrbefehlWiederholung",rdf_telegram=rdf_telegram_fahrbefehl_wdhl)
            self._add_message(rdf['nlu'],rdf['belegblatt'],"Negative Bestätigung",message_name="NegativeBestaetigung",rdf_telegram=rdf_telegram_neg_bestaetigung)
            self._add_message(rdf['nlu'],rdf['belegblatt'],"Positive Bestätigung",message_name="PositiveBestaetigung",rdf_telegram=rdf_telegram_pos_bestaetigung)
 
        # NLU --> Nothalt
        self._add_message(rdf['nlu'],rdf['nothalt'],"Nothaltauftrag von ZF",message_name="EmergencyStopRequest",rdf_telegram=rdf_telegram_emergency_stop_request)

        # Nothalt --> TTS
        self._add_message(rdf['nothalt'],rdf['tts'],"Nothaltauftrag über Funk",message_name="EmergencyStop",rdf_telegram=rdf_telegram_emergency_stop)

        # Nothalt --> Belegblatt
        if simulation_mode == SimulationMode.SUMO:
            self._add_message(rdf['nothalt'], rdf['sumo_simulation'], "Nothaltauftrag an Belegblatt zum Blockieren/Aktivieren der Steuerung", message_name="EmergencyStopControl", rdf_telegram=rdf_telegram_emergency_stop_control)
            self._add_message(rdf['sumo_simulation'], rdf['belegblatt'], "Nothaltauftrag an Belegblatt zum Blockieren/Aktivieren der Steuerung", message_name="EmergencyStopControl", rdf_telegram=rdf_telegram_emergency_stop_control)
        else:
            self._add_message(rdf['nothalt'], rdf['belegblatt'], "Nothaltauftrag an Belegblatt zum Blockieren/Aktivieren der Steuerung", message_name="EmergencyStopControl", rdf_telegram=rdf_telegram_emergency_stop_control)

        # Belegblatt --> Interlocking
        self._add_message(rdf['belegblatt'],rdf['interlocking'],"Fahrweg prüfen und einstellen",message_name="SetRoute",rdf_telegram=rdf_telegram_set_route)
        self._add_message(rdf['belegblatt'],rdf['interlocking'],"Fahrweg freigeben",message_name="FreeRoute",rdf_telegram=rdf_telegram_free_route)

        self._add_message(rdf['interlocking'], rdf['objectcontroller'], "Fahrweg einstellen", message_name="fahrweg_set")
        self._add_message(rdf['interlocking'], rdf['belegblatt'], "Fahrweg eingelaufen", message_name="fahrweg_state")

        # Interlocking --> Belegblatt
        self._add_message(rdf['interlocking'], rdf['belegblatt'], "Rückmeldung auf Fahrweg einstellen", message_name="SetRouteResponse", rdf_telegram=rdf_telegram_set_route_response)
        self._add_message(rdf['interlocking'], rdf['belegblatt'], "Rückmeldung auf Fahrweg freigeben", message_name="FreeRouteResponse", rdf_telegram=rdf_telegram_free_route_response)

        if simulation_mode == SimulationMode.SUMO:
            # Belegblatt --> SUMO-Simulation
            self._add_message(rdf['belegblatt'], rdf['sumo_simulation'], "Text-Nachricht von ZL", message_name="Command",
                              rdf_telegram=rdf_telegram_command)
            # SUMO-Simulation --> TTS
            # Um das Interface zur Simulation gleich dem des Rest-Systems zu halten, wurde hier darauf verzichtet,
            # differenzierte Nachrichten zu verschicken. Die Simulation wird die Text-Nachrichten parsen.
            self._add_message(rdf['sumo_simulation'], rdf['tts'], "Text-Nachricht von ZL", message_name="Command",
                              rdf_telegram=rdf_telegram_command)
        else: 
            # Belegblatt --> TTS
            self._add_message(rdf['belegblatt'],rdf['tts'],"Text-Nachricht von ZL",message_name="Command",rdf_telegram=rdf_telegram_command)

        # NLU --> TTS (Fehlermeldung von NLU)
        self._add_message(rdf['nlu'],rdf['tts'],"Fehlermeldung von NLU", message_name="NLUFehler", rdf_telegram=rdf_telegram_command)

        # TTS --> SPI
        self._add_message(rdf['tts'], rdf['sip'], "WAV-File aus Konvertierung von Text", message_name="ZLiCAudio", rdf_telegram=rdf_audio_telegram)
        self._add_message(rdf['tts'],rdf['sip'],"Nothalt WAV-File aus Konvertierung von Text",message_name="ZLiCEmergencyStopAudio",rdf_telegram=rdf_nothalt_audio_telegram)


        # Tracing, add additional Receiver
        for p_message in self.sparql_wrapper.get_instances_of_type(SNS.Message):

            # only add Messages with Telegram (can be traced)
            if not self.sparql_wrapper.has_out_reference(p_message, TNS.hasStructure):
                continue

            if simulation_mode == SimulationMode.SUMO:
                # also exclude Messages to Simulation
                p_receivers = self.sparql_wrapper.get_out_references(p_message, SNS.hasReceiver)
                if len(p_receivers) == 1:
                    if p_receivers[0] == rdf['sumo_simulation']:
                        continue
                else:
                    raise ValueError("multiple receivers not implemented")

            self.wrapper.add_reference(SNS.hasReceiver, p_message, rdf['tracing'])

        # Sequenz
        # [ rdf_message_fahranfrage , rdf_message_fahrweganstoss , rdf_message_fahrwegeingelaufen , rdf_message_ueberfahrbare_balisen ]
        self._new_sequence("Fahranfrage per Funk durch Zugführer an den Zugleiter in der Cloud.")
        self._new_sequence("Fahrweganstoss über Stellprozess.")
        self._new_sequence("Fahrweg eingelaufen.")
        self._new_sequence("Fahrbefehl.")
        self._new_sequence("Fahrauftrag Wiederholung.")
        self._new_sequence("Bestätigung.")

        return rdf
