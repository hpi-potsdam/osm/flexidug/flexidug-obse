from typing import List
from rdflib import URIRef, Seq, BNode
from obse.graphwrapper import GraphWrapper
from util.flexidug.namespace import FNS


# Available railway network models
class RailwayNetworkModel:
    LAUSITZ = 'lausitz'
    JOEHSTADT = 'joehstadt'
    _choices = [LAUSITZ, JOEHSTADT]
    _default = LAUSITZ


def read_points(filename):
    with open(filename, 'r', encoding='UTF-8') as file:
        lines = [line.strip() for line in file if line.strip()]
    return lines


class DataModelDefinition:

    def __init__(self, wrapper: GraphWrapper):
        self.wrapper = wrapper
        self.rdf_station_cache = {}

    def _add_station(self, rdf_model: URIRef, station_name: str):
        rdf_station = self.wrapper.add_labeled_instance(FNS.Station, f"Station {station_name}")
        self.wrapper.add_str_property(FNS.stationName, rdf_station, station_name)
        self.wrapper.add_reference(FNS.hasStation, rdf_model, rdf_station)
        self.rdf_station_cache[station_name] = rdf_station

    def _add_track(self, rdf_model: URIRef, stations):

        rdf_track = self.wrapper.add_labeled_instance(FNS.Track, f"Track for {'-'.join(stations)}")
        for station in stations:
            self.wrapper.add_reference(FNS.hasStopOverStation, rdf_track, self.rdf_station_cache[station])
        self.wrapper.add_reference(FNS.hasTrack, rdf_model, rdf_track)

        return rdf_track

    def _add_route_section(self, route_section: Seq, from_signal: str, to_signal: str, direction: str, station: str = None):

        rdf_route_section = self.wrapper.add_labeled_instance(FNS.RouteSection, f"RouteSection from {from_signal} to {to_signal}")
        self.wrapper.add_str_property(FNS.fromSignal, rdf_route_section, from_signal)
        self.wrapper.add_str_property(FNS.toSignal, rdf_route_section, to_signal)
        self.wrapper.add_str_property(FNS.direction, rdf_route_section, direction)
        if station:
            self.wrapper.add_reference(FNS.inStation, rdf_route_section, self.rdf_station_cache[station])

        route_section.append(rdf_route_section)

    def _add_signal_ne1(self, rdf_models: List[URIRef], name: str, signal_name: str):
        rdf_signal = self.wrapper.add_labeled_instance(FNS.SignalNe1, name, unique_name=f"SignalNe1 {signal_name}")
        self.wrapper.add_str_property(FNS.identifier, rdf_signal, signal_name)

        for rdf_model in rdf_models:
            self.wrapper.add_reference(FNS.hasInfrastructure, rdf_model, rdf_signal)

    def _add_signal_ne5(self, rdf_models: List[URIRef], name: str, signal_name: str):
        rdf_signal = self.wrapper.add_labeled_instance(FNS.SignalNe5, name, unique_name=f"SignalNe5 {signal_name}")
        self.wrapper.add_str_property(FNS.identifier, rdf_signal, signal_name)

        for rdf_model in rdf_models:
            self.wrapper.add_reference(FNS.hasInfrastructure, rdf_model, rdf_signal)

    def _add_point(self, rdf_models: List[URIRef], name: str, point_uuid: str):
        rdf_point = self.wrapper.add_labeled_instance(FNS.Point, name, unique_name=f"Point {point_uuid}")
        self.wrapper.add_str_property(FNS.identifier, rdf_point, point_uuid)

        for rdf_model in rdf_models:
            self.wrapper.add_reference(FNS.hasInfrastructure, rdf_model, rdf_point)

    def create_network_model_lausitz(self, rdf):

        # Planpro Model
        self.wrapper.add_str_property(FNS.filename, rdf['plan_pro_model'], "data/lausitz.ppxml.ppxml")

        # Automatic Train Model for SUMO component
        self.wrapper.add_str_property(FNS.filename, rdf['automatic-trains-model'], "datamodel/automatic-trains-lausitz.json")

        # Meldestelle
        stations = ["S-Berg", "C-Dorf"]

        for station in stations:
            self._add_station(rdf['station_model'], station)

        # Trackmodel
        rdf_track = self._add_track(rdf['track_model'], stations)
        bnode = BNode()
        route_section_seq = self.wrapper.add_sequence_to(bnode)
        self.wrapper.add_reference(FNS.hasRouteSection, rdf_track, bnode)

        # Routen
        self._add_route_section(route_section_seq, from_signal="60E1", to_signal="60A2", direction="out", station="S-Berg")
        self._add_route_section(route_section_seq, from_signal="60A2", to_signal="60E3", direction="out")
        self._add_route_section(route_section_seq, from_signal="60E3", to_signal="60WE1", direction="out")
        self._add_route_section(route_section_seq, from_signal="60WE1", to_signal="60B2", direction="out", station="C-Dorf")

        self._add_route_section(route_section_seq, from_signal="60E4", to_signal="60B1", direction="back", station="C-Dorf")
        self._add_route_section(route_section_seq, from_signal="60B1", to_signal="60E2", direction="back")
        self._add_route_section(route_section_seq, from_signal="60E2", to_signal="60WE2", direction="back")
        self._add_route_section(route_section_seq, from_signal="60WE2", to_signal="60A1", direction="back", station="S-Berg")

        # Signale
        self._add_signal_ne1([rdf['infrastructure_model'], rdf['simulator_model']], "Trapeztafel E1", "60E1")
        self._add_signal_ne1([rdf['infrastructure_model'], rdf['simulator_model']], "Trapeztafel E2", "60E2")
        self._add_signal_ne1([rdf['infrastructure_model'], rdf['simulator_model']], "Trapeztafel E3", "60E3")
        self._add_signal_ne1([rdf['infrastructure_model'], rdf['simulator_model']], "Trapeztafel E4", "60E4")

        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel A1", "60A1")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel A1b", "60A1b")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel A2", "60A2")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel A2b", "60A2b")

        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel B1", "60B1")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel B1b", "60B1b")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel B2", "60B2")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel B2b", "60B2b")

        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Weichenende WE1", "60WE1")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Weichenende WE2", "60WE2")

        points = read_points("sandbox/stellprozess/data/lausitz_points.txt")
        for i, point in enumerate(points):
            self._add_point([rdf['infrastructure_model'], rdf['simulator_model']], f"Weiche {i}", point)

    def create_network_model_joehstadt(self, rdf):

        # Planpro Model
        self.wrapper.add_str_property(FNS.filename, rdf['plan_pro_model'], "data/joehstadt.ppxml")

        # Automatic Train Model for SUMO component
        self.wrapper.add_str_property(FNS.filename, rdf['automatic-trains-model'], "datamodel/automatic-trains-joehstadt.json")

        # Station Model
        # Meldestelle
        # Jöhstadt, Fahrzeughalle, Schlössel, Loreleofelsen, Schmalzgrube, Forellenhof, Andreas-Gegentrum-Stollen, Steinbach
        stations = ["Jöhstadt", "Fahrzeughalle", "Schlössel"]

        for station in stations:
            self._add_station(rdf['station_model'], station)

        # Track Model
        rdf_track = self._add_track(rdf['track_model'], stations)
        bnode = BNode()
        route_section_seq = self.wrapper.add_sequence_to(bnode)
        self.wrapper.add_reference(FNS.hasRouteSection, rdf_track, bnode)

        # Routen
        self._add_route_section(route_section_seq, from_signal="60ES1", to_signal="60AS2", direction="out", station="Jöhstadt")
        self._add_route_section(route_section_seq, from_signal="60AS2", to_signal="60ES3", direction="out")
        self._add_route_section(route_section_seq, from_signal="60ES3", to_signal="60AS4", direction="out", station="Fahrzeughalle")
        self._add_route_section(route_section_seq, from_signal="60AS4", to_signal="60ES5", direction="out")
        self._add_route_section(route_section_seq, from_signal="60ES5", to_signal="60AS6", direction="out", station="Schlössel")

        self._add_route_section(route_section_seq, from_signal="60ES7", to_signal="60AS8", direction="back", station="Schlössel")
        self._add_route_section(route_section_seq, from_signal="60AS8", to_signal="60ES9", direction="back")
        self._add_route_section(route_section_seq, from_signal="60ES9", to_signal="60AS10", direction="back", station="Fahrzeughalle")
        self._add_route_section(route_section_seq, from_signal="60AS10", to_signal="60ES11", direction="back")
        self._add_route_section(route_section_seq, from_signal="60ES11", to_signal="60AS12", direction="back", station="Jöhstadt")

        self._add_signal_ne1([rdf['infrastructure_model'], rdf['simulator_model']], "Trapeztafel ES1", "60ES1")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel AS2", "60AS2")
        self._add_signal_ne1([rdf['infrastructure_model'], rdf['simulator_model']], "Trapeztafel ES3", "60ES3")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel AS4", "60AS4")
        self._add_signal_ne1([rdf['infrastructure_model'], rdf['simulator_model']], "Trapeztafel ES5", "60ES5")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel AS6", "60AS6")
        self._add_signal_ne1([rdf['infrastructure_model'], rdf['simulator_model']], "Trapeztafel ES7", "60ES7")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel AS8", "60AS8")
        self._add_signal_ne1([rdf['infrastructure_model'], rdf['simulator_model']], "Trapeztafel ES9", "60ES9")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel AS10", "60AS10")
        self._add_signal_ne1([rdf['infrastructure_model'], rdf['simulator_model']], "Trapeztafel ES11", "60ES11")
        self._add_signal_ne5([rdf['infrastructure_model'], rdf['simulator_model']], "Haltetafel AS12", "60AS12")

        points = read_points("sandbox/stellprozess/data/joehstadt_points.txt")
        for i, point in enumerate(points):
            self._add_point([rdf['infrastructure_model'], rdf['simulator_model']], f"Weiche {i}", point)

    def apply(self, args):

        print(f"Use network model '{args.config_network_model}'")
        print(f"Simulation parameter: automatic_trains_autoplay '{args.automatic_trains_autoplay}'")

        rdf = {}

        area = args.config_network_model

        # Models for interlocking/stellprozess
        rdf['plan_pro_model'] = self.wrapper.add_labeled_instance(FNS.PlanProModel, f"{area} PlanProModel")
        rdf['station_model'] = self.wrapper.add_labeled_instance(FNS.StationModel, f"{area} Station Model")
        rdf['track_model'] = self.wrapper.add_labeled_instance(FNS.TrackModel, f"{area} Track Model")

        # mit Simulatoren models infrastructure-simulator or traci/sumo or none
        rdf['infrastructure_model'] = self.wrapper.add_labeled_instance(FNS.InfrastructureModel, f"{area} Infrastruktur Model")
        rdf['traci_model'] = self.wrapper.add_labeled_instance(FNS.TraciModel, f"{area} SUMO / Traci Model")

        # SUMO Models
        rdf['simulator_model'] = self.wrapper.add_labeled_instance(FNS.SimulatorModel, f"{area} Simulator Model")

        rdf['automatic-trains-model'] = self.wrapper.add_labeled_instance(FNS.AutomaticTrainsModel, f"{area} Automatic Train Model")
        self.wrapper.add_bool_property(FNS.autoplay, rdf['automatic-trains-model'], args.automatic_trains_autoplay)

        if args.config_network_model == RailwayNetworkModel.LAUSITZ:
            self.create_network_model_lausitz(rdf)
        elif args.config_network_model == RailwayNetworkModel.JOEHSTADT:
            self.create_network_model_joehstadt(rdf)
        else:
            raise ValueError(f"Network model {args.config_network_model} not implemented.")

        print(f"Voice Protocol option: automatic_train_registration '{args.automatic_train_registration}'")
        print(f"Voice Protocol option: automatic_train_deregistration '{args.automatic_train_deregistration}'")
        rdf['voice_protocol_options_model'] = self.wrapper.add_labeled_instance(FNS.VoiceProtocolOptionsModel, "Voice Protocol Options Model")
        self.wrapper.add_bool_property(FNS.automaticTrainRegistration, rdf['voice_protocol_options_model'], args.automatic_train_registration)
        self.wrapper.add_bool_property(FNS.automaticTrainDeregistration, rdf['voice_protocol_options_model'], args.automatic_train_deregistration)

        return rdf
