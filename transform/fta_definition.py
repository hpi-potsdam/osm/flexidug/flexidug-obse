from typing import List
from rdflib import URIRef, RDFS
from obse.graphwrapper import GraphWrapper
from obse.sparql_queries import SparQLWrapper

from util.fta.namespace import FTANS
from util.namespaces import SNS


class FaultTreeAnalysisDefinition:

    def __init__(self, wrapper: GraphWrapper, sparql_wrapper: SparQLWrapper):
        self.wrapper = wrapper
        self.sparql_wrapper = sparql_wrapper

    def _create_or_gate(self, p_parent, p_childs, unique_name):
        p_or = self.wrapper.add_labeled_instance(FTANS.OrGate, "OR", unique_name=unique_name)
        self.wrapper.add_reference(FTANS.hasGate, p_parent, p_or)
        for p_child in p_childs:
            self.wrapper.add_reference(FTANS.hasEvent, p_or, p_child)

    def apply(self):

        rdf = {}

        # Systemfehler

        rdf['zlic_failure'] = self.wrapper.add_labeled_instance(FTANS.TopEvent, "ZLiC fällt aus", unique_name='zlic_failure')

        subsystem_failures = []

        for p_subsystem in self.sparql_wrapper.get_instances_of_type(SNS.Subsystem):
            name_subsystem = self.sparql_wrapper.get_single_object_property(p_subsystem, RDFS.label)
            

            component_failures = []
            for p_component in self.sparql_wrapper.get_out_references(p_subsystem, SNS.hasComponent):
                name_component = self.sparql_wrapper.get_single_object_property(p_component, RDFS.label)
                component_failure = self.wrapper.add_labeled_instance(FTANS.IntermediateEvent, f"{name_component} fällt aus", unique_name=f'{name_component}_failure')
                component_failures.append(component_failure)

                memory_failure = self.wrapper.add_labeled_instance(FTANS.BasicEvent, "Hauptpeicher voll", unique_name=f'{name_component}_failure_memory')
                harddisk_failure = self.wrapper.add_labeled_instance(FTANS.BasicEvent, "Festplatte voll", unique_name=f'{name_component}_failure_harddisk')

                component_detail_failures = [memory_failure, harddisk_failure]
                for p_function in self.sparql_wrapper.get_out_references(p_component, SNS.hasFunction):
                    name_function = self.sparql_wrapper.get_single_object_property(p_function, RDFS.label)

                    function_failure = self.wrapper.add_labeled_instance(FTANS.BasicEvent, f"Function '{name_function}' nicht verfügbar", unique_name=f'{name_component}_{name_function}_failure')
                    component_detail_failures.append(function_failure)

                self._create_or_gate(component_failure, component_detail_failures, unique_name=f"or_{name_component}_failure")


            if len(component_failures) > 0:
                subsystem_failure = self.wrapper.add_labeled_instance(FTANS.IntermediateEvent, f"{name_subsystem} fällt aus", unique_name=f'{name_subsystem}_failure')
                subsystem_failures.append(subsystem_failure)
                self._create_or_gate(subsystem_failure, component_failures, unique_name=f"or_{name_subsystem}_failure")
            else:
                subsystem_failure = self.wrapper.add_labeled_instance(FTANS.BasicEvent, f"{name_subsystem} fällt aus", unique_name=f'{name_subsystem}_failure')
                subsystem_failures.append(subsystem_failure)

        self._create_or_gate(rdf['zlic_failure'], subsystem_failures, unique_name="or_zlic_failure")


        # Fachliche Fehler
        rdf['zf_wrong_understanding'] = self.wrapper.add_labeled_instance(FTANS.TopEvent, "Zf hats falsch verstanden", unique_name='zf_wrong_understanding')

        return rdf
