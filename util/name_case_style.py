"""
camelCase
PascalCase
snake_case
kebab-case
"""

import re



def split_name(name):
    parts = re.sub(r"([-_\s]+)", r" ",name)
    return re.sub( r"([A-Z])", r" \1",parts).lower().split()


#dieser_satz_ist_snake_case_geschrieben
def snake_case(name):
    parts = split_name(name)
    return "_".join(parts)

#dieser-satz-ist-kebab-case-geschrieben

def kebab_case(name):
    """
    converts names to kebab-case
    """
    parts = split_name(name)
    return "-".join(parts)

#SieserSatzIstInPascalCaseGeschrieben

def pascal_case(name):
    """
    converts names to PascalCase
    """
    parts = split_name(name)
    return " ".join(parts).title().replace(" ", "")