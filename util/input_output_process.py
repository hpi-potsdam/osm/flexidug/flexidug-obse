from rdflib import URIRef, RDFS

from obse.sparql_queries import SparQLWrapper

from .telegram.message_structure import get_message_structure, message_struct_to_config
from .name_case_style import snake_case, pascal_case
from .telegram.namespace import TNS
from .namespaces import SNS

class InputOutputProcess:

    def __init__(self, wrapper: SparQLWrapper, name: str):
        self.wrapper = wrapper
        self.name = name
        self.messages = []

    def _resolve_message(self, p_msg: URIRef, direction: str):

        msg_name = self.wrapper.get_single_object_property(p_msg, SNS.messageName)

        p_sender = self.wrapper.get_single_out_reference(p_msg, SNS.hasSender)
        sender_name = self.wrapper.get_single_object_property(p_sender, RDFS.label)
        topic_name = f"{pascal_case(sender_name)}{pascal_case(msg_name)}"
        # print(msg_name, "send by", sender_name, "=>", topic_name)

        rdf_sequence = self.wrapper.get_single_out_reference(p_msg, TNS.hasStructure)

        msg_structure = get_message_structure(self.wrapper, rdf_sequence)
        # print(msg_structure)
        message_config = message_struct_to_config(msg_structure)
        # print(message_config)
        message_config_dds = message_struct_to_config(msg_structure, dds=True)
        # print(message_config_dds)
        return {
            "direction": direction,
            "msg_name": msg_name,
            "topic_name": topic_name,
            "msg_structure": msg_structure,
            "message_config": message_config,
            "message_config_dds": message_config_dds
        }

    def add_input_message(self,p_input_msg : URIRef):
        if self.wrapper.has_out_reference(p_input_msg,TNS.hasStructure):
            msg = self._resolve_message(p_input_msg,"INPUT")
            self.messages.append(msg)


    def add_output_message(self,p_output_msg : URIRef):
        if self.wrapper.has_out_reference(p_output_msg,TNS.hasStructure):
            msg = self._resolve_message(p_output_msg,"OUTPUT")
            self.messages.append(msg)


    def get_message_configs(self):
        configs = []
        for msg in self.messages:
            configs.extend(msg['message_config'])
        return configs
    
    def get_dds_message_configs(self):

        configs = []
        for msg in self.messages:
            configs.extend(msg['message_config_dds'])
        return configs
    
    def get_proxy_config(self):
        proxy_config = {
            "message_imports" : [],
            "component_name" : f"{pascal_case(self.name)}ProxyInterface",
            "callback_funcs" : [],
            "apply_funcs" : []
        }

        imports = []

        for msg in self.messages:
            message_config = msg['message_config']
            message_class_name = message_config[-1]['MessageClassName']
            if message_class_name not in imports:
                imports.append(message_class_name)
        
            if msg['direction'] == "OUTPUT":
                proxy_config["callback_funcs"].append({
                        "name" : msg['msg_name'], #TODO snake_case(msg_name) , 
                        "data_type" : message_class_name
                })
            else: # INPUT
                proxy_config["apply_funcs"].append({
                    "name" : msg['msg_name'], #TODO snake_case(msg_name) , 
                    "data_name" : snake_case(message_class_name),
                    "data_type" : message_class_name
                })

        for imp in imports:
            proxy_config["message_imports"].append({
                "package" : f".{imp}",
                "name" : imp
            })
        return proxy_config
    
    def _get_mapper_properties(self,from_properties,to_properties,property_structs,convert):


        l = len(from_properties)
        if l != len(to_properties):
            raise ValueError("different length of properties {from_properties} / {to_properties}")
        if l != len(property_structs):
            raise ValueError("different length of properties {from_properties} / {property_structs}")
        #print("from",from_properties)

        properties = []
        for i in range(l):
            properties.append({
                'from_name' : from_properties[i]['name'],
                'to_name' : to_properties[i]['name']
            })

            ft = property_structs[i].get_data_type()
           

            if ft == "Array":
                properties[-1]['arraycast'] = f"map_{from_properties[i]['item_type']}_to_{to_properties[i]['item_type']}"
                continue

            if ft == "Structure":
                properties[-1]['cast'] = f"map_{from_properties[i]['item_type']}_to_{to_properties[i]['item_type']}"
                continue

            if ft in convert:
                properties[-1]['cast'] = convert[ft]
                continue

            properties[-1]['nocast'] = True

        properties[-1]['last'] = True
        return properties

    def get_mapper_config(self):
        mapper_config = {
            "message_imports" : [],
            "mapper" : []
        }


        convert_from_dds = {
            "Bytes" : "bytes"
        }
        convert_to_dds = {}

        imports = []

        for msg in self.messages:
            message_config = msg['message_config']
            msg_structure = msg['msg_structure']

            message_config_dds = msg['message_config_dds']

            l = len(message_config)

            # TODO this check to constructor
            if len(message_config_dds) != len(message_config): 
                raise ValueError(f"{message_config_dds} and {message_config} needs same length")
            if len(msg_structure) != len(message_config):
                raise ValueError(f"{msg_structure} and {message_config} needs same length")
            

            for i in range(l):
                message_class_name = message_config[i]['MessageClassName']
                message_class_name_dds = message_config_dds[i]['MessageClassName']

                if message_class_name not in imports:
                    imports.append(message_class_name)
                if message_class_name_dds not in imports:
                    imports.append(message_class_name_dds)


                if msg['direction'] == "OUTPUT": # Mapper internal to external (dds) Message
                    mapper_config['mapper'].append({
                        'from_name' : snake_case(message_class_name),
                        'from_type' : message_class_name,
                        'to_name' : snake_case(message_class_name_dds),
                        'to_type' : message_class_name_dds,
                        'properties' : self._get_mapper_properties(
                        message_config[i]['properties'],
                        message_config_dds[i]['properties'],
                        msg_structure[i].get_data_properties(),
                        convert_to_dds)
                    })
                else: # INPUT # Mapper external (dds) to internal Message
                    mapper_config['mapper'].append({
                        'from_name' : snake_case(message_class_name_dds),
                        'from_type' : message_class_name_dds,
                        'to_name' : snake_case(message_class_name),
                        'to_type' : message_class_name,
                        'properties' : self._get_mapper_properties(
                            message_config_dds[i]['properties'],
                            message_config[i]['properties'],
                            msg_structure[i].get_data_properties(),
                            convert_from_dds)
                    })

        for imp in imports:
            mapper_config["message_imports"].append({
                "package" : f".{imp}",
                "name" : imp
            })

        return mapper_config

    def get_subscriber_config(self):
        subscriber_config = {
            "message_imports": [],
            "proxy_import": [],
            "topics": [],
            "data_reader": [],
            "data_writer": [],
            "apply_funcs": [],
            "proxy_name": f"{pascal_case(self.name)}Proxy",
            "proxy_callbacks": [],
            "read_messages": []
        }

        subscriber_config["proxy_import"].append({
            'package': f'src.{pascal_case(self.name)}Proxy',
            'name': f"{pascal_case(self.name)}Proxy"
        })

        imports = []
        for msg in self.messages:
            message_config = msg['message_config']
            message_config_dds = msg['message_config_dds']

            message_class_name = message_config[-1]['MessageClassName']
            if message_class_name not in imports:
                imports.append(message_class_name)

            message_class_name_dds = message_config_dds[-1]['MessageClassName']
            if message_class_name_dds not in imports:
                imports.append(message_class_name_dds)

            subscriber_config["topics"].append({
                "name": msg["topic_name"],
                "msg_type": message_class_name_dds
            })

            if msg['direction'] == "OUTPUT":
                subscriber_config["data_writer"].append({
                    "name": msg["msg_name"],
                    "topic_name": msg["topic_name"],
                })
                subscriber_config["apply_funcs"].append({
                    "name": msg["msg_name"],
                    "msg_name": snake_case(message_class_name_dds),
                    "msg_type": message_class_name_dds,
                    "data_name": snake_case(message_class_name),
                    "data_type": message_class_name
                })
                subscriber_config["proxy_callbacks"].append({
                    "name": msg["msg_name"]
                })
            else:  # INPUT
                subscriber_config["data_reader"].append({
                    "name": msg["msg_name"],
                    "topic_name": msg["topic_name"]
                })
                subscriber_config["read_messages"].append({
                    "name": msg["msg_name"],
                    "msg_name": snake_case(message_class_name_dds),
                    "msg_type": message_class_name_dds,
                    "data_name": snake_case(message_class_name),
                    "data_type": message_class_name
                })

        for imp in imports:
            subscriber_config["message_imports"].append({
                "package": f"src.{imp}",
                "name": imp
            })
        return subscriber_config
