from numbers import Number
from rdflib import Literal
from rdflib import BNode
from obse.graphwrapper import GraphWrapper
from .asset.namespace import ANS


class JsonToRdfMapper:

    def __init__(self, wrapper: GraphWrapper):
        self.wrapper = wrapper

    def process_value(self, value):
        """Verarbeitet einen JSON-Wert (Zahl, String, Liste oder Objekt) und gibt RDF-Knoten zurück."""
        if isinstance(value, dict):
            return self.process_json_object(value)  # Wenn das value ein Objekt ist
        elif isinstance(value, list):
            return self.process_json_list(value)    # Wenn das value eine Liste ist
        elif isinstance(value, (Number, str)):
            return Literal(value)                   # Wenn es ein einfacher Wert ist (z.B. Zahl, String)
        else:
            raise ValueError(f'Unsupported value {value} of type {type(value)}')

    def process_json_list(self, json_list):
        """Verarbeitet eine JSON-Liste (Array) und gibt den RDF-Knoten (rdf:Seq) zurück."""

        # Erstelle einen anonymen RDF-Knoten für die Liste
        seq_node = BNode()
        seq = self.wrapper.add_sequence_to(seq_node)

        for item in json_list:
            item_node = self.process_value(item)  # Verarbeite jedes Element der Liste
            seq.append(item_node)
        return seq_node

    def process_json_object(self, json_obj, obj_node=None):
        """Verarbeitet ein JSON-Objekt (Dictionary) und gibt den RDF-Knoten zurück."""

        if not obj_node:
            # Erstelle einen anonymen RDF-Knoten
            obj_node = self.wrapper.add_bnode(ANS.Dictionary)

        for key, value in json_obj.items():
            key_node = Literal(key)  # Verwende den Key als Literal
            value_node = self.process_value(value)  # Verarbeite den Wert rekursiv

            # Erstelle einen neuen Blank Node für das Key-Value-Paar
            kv_pair = self.wrapper.add_bnode(ANS.KeyValuePair)
            self.wrapper.add_reference(ANS.key, kv_pair, key_node)
            self.wrapper.add_reference(ANS.value, kv_pair, value_node)

            # Füge das Key-Value-Paar zu dem aktuellen Objektknoten hinzu
            self.wrapper.add_reference(ANS.hasKeyValuePair, obj_node, kv_pair)

        return obj_node

    def apply(self, data, data_id):
        rdf_dictionary = self.wrapper.add_labeled_instance(ANS.Dictionary, "Dictionary", unique_name=data_id)
        return self.process_json_object(data, rdf_dictionary)
