import json
from .create_uuid import create_uuid_from_json, create_uuid_from_string

def test_create_uuid_from_json():
    uuid1 = create_uuid_from_json(json.loads('{"test": "abc"}'))
    uuid2 = create_uuid_from_json(json.loads('{ "test" : "abc" }'))
    assert uuid1 == uuid2
    assert str(uuid1)  == "ea63c300-c845-f3e7-6d8f-5827aa0a1f89"

def test_create_uuid_from_string():
    uuid1 = create_uuid_from_string('{"test": "abc"}')
    uuid2 = create_uuid_from_string('{ "test" : "abc" }')
    assert uuid1 != uuid2
    assert str(uuid1) == "ea63c300-c845-f3e7-6d8f-5827aa0a1f89"
