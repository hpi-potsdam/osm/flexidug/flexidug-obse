from rdflib import URIRef
from obse.graphwrapper import GraphWrapper
from .namespace import TNS


rdf_data_type_cache = {} #TODO add functionality to Graph-Wrapper

class TelegramStructureBuilder:

    def __init__(self,wrapper: GraphWrapper,name : str):
        self.wrapper = wrapper
        self.name = name
        self.rdf = wrapper.add_labeled_instance(TNS.Structure,name)
        self.wrapper.add_str_property(TNS.structureName,self.rdf,name)


    def _add_data_field(self,var_name : str,rdf_data_type : URIRef):
        rdf_data_field = self.wrapper.add_instance(TNS.DataField,var_name,unique_name=f"{self.name}-{var_name}")
        self.wrapper.add_str_property(TNS.elementName,rdf_data_field,var_name)
        self.wrapper.add_reference(TNS.hasDataType,rdf_data_field,rdf_data_type)
        self.wrapper.add_reference(TNS.hasDataField,self.rdf,rdf_data_field)
        return rdf_data_field

    def add_string_data_field(self,var_name : str):
        if "string" not in rdf_data_type_cache:
            rdf_data_type_cache['string'] = self.wrapper.add_instance(TNS.String,"String")
        self._add_data_field(var_name,rdf_data_type_cache['string'])

    def add_bytes_data_field(self,var_name : str):
        if "bytes" not in rdf_data_type_cache:
            rdf_data_type_cache['bytes'] = self.wrapper.add_instance(TNS.Bytes,"Bytes")
        self._add_data_field(var_name,rdf_data_type_cache['bytes'])

    def add_integer_data_field(self,var_name : str):
        if "integer" not in rdf_data_type_cache:
            rdf_data_type_cache['integer'] = self.wrapper.add_instance(TNS.Integer,"Integer")
        self._add_data_field(var_name,rdf_data_type_cache['integer'])

    def add_array_of_structure(self,var_name : str, rdf_structure : URIRef):
        if "array" not in rdf_data_type_cache:
            rdf_data_type_cache['array'] = self.wrapper.add_instance(TNS.Array,"Array")
        rdf_data_field = self._add_data_field(var_name,rdf_data_type_cache['array'])

        self.wrapper.add_reference(TNS.hasArrayItemDataType,rdf_data_field,rdf_structure)

    def to_rdf(self):
        return self.rdf
    