from obse.sparql_queries import SparQLWrapper
from .namespace import TNS
from rdflib import URIRef

class DataProperty:

    def __init__(self,name : str,data_type : str,items : str):
        self.name = name
        self.data_type = data_type
        self.items = items
  
    def get_data_type(self):
        return self.data_type
    
    def get_items(self):
        return self.items
    
    def get_name(self):
        return self.name

    def __repr__(self):
        return f'({self.name}, {self.data_type}, {self.items})'

class DataStruct:

    def __init__(self,name : str):
        self.data_properties = []
        self.name = name

    def add_data_property(self,data_property : DataProperty):
        self.data_properties.append(data_property)

    def get_name(self):
        return self.name
    
    def get_data_properties(self):
        return self.data_properties

    def __repr__(self):
        return f"({self.name}, {self.data_properties})"



def get_message_structure(sparql_wrapper: SparQLWrapper,rdf_sequence):
    data_structs = []


    var_structure_name = sparql_wrapper.get_single_object_property(rdf_sequence,TNS.structureName)
    data_struct= DataStruct(var_structure_name)

    for rdf_data_field in sparql_wrapper.get_out_references(rdf_sequence,TNS.hasDataField):
       

        var_name = sparql_wrapper.get_single_object_property(rdf_data_field,TNS.elementName)
        rdf_type = sparql_wrapper.get_single_out_reference(rdf_data_field,TNS.hasDataType)
        var_type = sparql_wrapper.get_type(rdf_type)
        items = None
        if var_type == TNS.Array:
            rdf_item_type = sparql_wrapper.get_single_out_reference(rdf_data_field,TNS.hasArrayItemDataType)
            var_item_type = sparql_wrapper.get_type(rdf_item_type)

            if var_item_type == TNS.Structure:
                ds = get_message_structure(sparql_wrapper,rdf_item_type)
                items = ds[-1].name
                data_structs.extend(ds)
            else:
                raise ValueError(f"Unknown type {var_item_type}")

        data_property = DataProperty(name = var_name,data_type = var_type.split("#")[1],items = items)
            
        data_struct.add_data_property(data_property)
    data_structs.append(data_struct)
    return data_structs



def message_struct_to_config(data_structs,dds=False):

    name_ext = ""
    if dds:
        name_ext = "Message"

    message_configs = []
    for data_struct in data_structs:
        message_config = {
            "MessageClassName" : f"{data_struct.get_name()}{name_ext}",
            "properties" : []
        }

        imports = set()
        for data_property in data_struct.get_data_properties():

            data_type = data_property.get_data_type()
            items = data_property.get_items()
            if items and items not in ["str","int"]:
                imports.add(items)

 
            if data_type == "Array":
                if dds:
                    data_type = f"sequence[{items}{name_ext}]"  #TODO etwas generischer 
                else:
                    data_type = f"List[{items}{name_ext}]"
            if data_type == "structure":
                data_type = f"{items}{name_ext}"


            if dds and data_type == "Bytes":
                data_type = "sequence[int]" 

            #TODO Mapping
            if data_type == "Integer": data_type = "int"
            if data_type == "String": data_type = "str"
            if data_type == "Bytes": data_type = "bytes"


            message_config['properties'].append({
                "name" : data_property.get_name(),
                "type" : data_type,
                "item_type" : f"{items}{name_ext}"
            })

        if len(imports) > 0:
             message_config['message_imports'] = []
             for imp in imports:
                 message_config['message_imports'].append({
                     "name" : f"{imp}{name_ext}",
                     "package" : f".{imp}{name_ext}"
                 })

        message_configs.append(message_config)

    return message_configs
