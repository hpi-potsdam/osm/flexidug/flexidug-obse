from rdflib.namespace import DefinedNamespace
from rdflib import URIRef
from ..ontology_reader import OntologyReader

ontology = OntologyReader("models/telegram-0.0.1.ttl")

class TNS(DefinedNamespace):

    _NS = ontology.get_namespace()

    DataField: URIRef = ontology.get_class('#DataField')

    DataType: URIRef = ontology.get_class('#DataType')

    String: URIRef = ontology.get_class('#String')
    Bytes: URIRef = ontology.get_class('#Bytes')
    Integer: URIRef = ontology.get_class('#Integer')
    Array: URIRef = ontology.get_class('#Array')
    Structure: URIRef = ontology.get_class('#Structure')

    elementName: URIRef = ontology.get_datatype_property('#elementName')
    structureName: URIRef = ontology.get_datatype_property('#structureName')

    hasStructure : URIRef = ontology.get_object_property('#hasStructure')
    hasDataField: URIRef = ontology.get_object_property('#hasDataField')
    hasDataType: URIRef = ontology.get_object_property('#hasDataType')
    hasArrayItemDataType: URIRef = ontology.get_object_property('#hasArrayItemDataType')
