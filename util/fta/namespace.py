from rdflib.namespace import DefinedNamespace
from rdflib import URIRef
from ..ontology_reader import OntologyReader

ontology = OntologyReader("models/fault-tree-analysis-0.0.1.ttl")


class FTANS(DefinedNamespace):

    _NS = ontology.get_namespace()

    BasicEvent: URIRef = ontology.get_class('#BasicEvent')
    IntermediateEvent: URIRef = ontology.get_class('#IntermediateEvent')
    TopEvent: URIRef = ontology.get_class('#TopEvent')

    OrGate: URIRef = ontology.get_class('#OrGate')
    AndGate: URIRef = ontology.get_class('#AndGate')
    VotingOrGate: URIRef = ontology.get_class('#VotingOrGate')

    probability: URIRef = ontology.get_datatype_property('#probability')
    threshold: URIRef = ontology.get_datatype_property('#threshold')

    hasGate: URIRef = ontology.get_object_property('#hasGate')
    hasEvent: URIRef = ontology.get_object_property('#hasEvent')
