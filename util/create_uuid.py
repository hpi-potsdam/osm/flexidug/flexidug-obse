import uuid
import hashlib
import json

def create_uuid_from_string(val: str):
    hex_string = hashlib.md5(val.encode("UTF-8")).hexdigest()
    return uuid.UUID(hex=hex_string)

def create_uuid_from_json(json_object: object):
    val = json.dumps(json_object, sort_keys = True).encode("UTF-8")
    hex_string = hashlib.md5(val).hexdigest()
    return uuid.UUID(hex=hex_string)