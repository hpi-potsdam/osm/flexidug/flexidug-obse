from rdflib.namespace import DefinedNamespace
from rdflib import URIRef
from ..ontology_reader import OntologyReader

ontology = OntologyReader("models/flexidug-0.0.4.ttl")


class FNS(DefinedNamespace):

    _NS = ontology.get_namespace()

    PlanProModel: URIRef = ontology.get_class('#PlanProModel')
    StationModel: URIRef = ontology.get_class('#StationModel')
    TrackModel: URIRef = ontology.get_class('#TrackModel')
    InfrastructureModel: URIRef = ontology.get_class('#InfrastructureModel')
    SimulatorModel: URIRef = ontology.get_class('#SimulatorModel')
    AutomaticTrainsModel: URIRef = ontology.get_class('#AutomaticTrainsModel')
    TraciModel: URIRef = ontology.get_class('#TraciModel')
    VoiceProtocolOptionsModel: URIRef = ontology.get_class('#VoiceProtocolOptionsModel')

    Station: URIRef = ontology.get_class('#Station')
    Track: URIRef = ontology.get_class('#Track')
    RouteSection: URIRef = ontology.get_class('#RouteSection')

    Point: URIRef = ontology.get_class('#Point')
    SignalNe1: URIRef = ontology.get_class('#SignalNe1')
    SignalNe5: URIRef = ontology.get_class('#SignalNe5')

    stationName: URIRef = ontology.get_datatype_property('#stationName')
    filename: URIRef = ontology.get_datatype_property('#filename')
    autoplay: URIRef = ontology.get_datatype_property('#autoplay')

    fromSignal: URIRef = ontology.get_datatype_property('#fromSignal')
    toSignal: URIRef = ontology.get_datatype_property('#toSignal')
    direction: URIRef = ontology.get_datatype_property('#direction')

    identifier: URIRef = ontology.get_datatype_property('#identifier')

    automaticTrainRegistration: URIRef = ontology.get_datatype_property('#automaticTrainRegistration')
    automaticTrainDeregistration: URIRef = ontology.get_datatype_property('#automaticTrainDeregistration')

    hasDataModel: URIRef = ontology.get_object_property('#hasDataModel')
    hasStation: URIRef = ontology.get_object_property('#hasStation')
    hasStopOverStation: URIRef = ontology.get_object_property('#hasStopOverStation')
    hasTrack: URIRef = ontology.get_object_property('#hasTrack')
    hasRouteSection: URIRef = ontology.get_object_property('#hasRouteSection')
    inStation: URIRef = ontology.get_object_property('#inStation')
    hasInfrastructure: URIRef = ontology.get_object_property('#hasInfrastructure')
