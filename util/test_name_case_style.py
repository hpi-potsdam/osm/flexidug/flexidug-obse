from .name_case_style import split_name, snake_case, kebab_case, pascal_case


def test_split():
    assert ["abc", "def", "ghi"] == split_name("AbcDefGhi")
    assert ["abc", "def", "ghi"] == split_name("abc_def_ghi")
    assert ["abc", "def", "ghi"] == split_name("abcDefGhi")
    assert ["abc", "def", "ghi"] == split_name("abc-def-ghi")
    assert ["abc", "def", "ghi"] == split_name("Abc Def Ghi")


def test_snake_case():
    assert "abc_def_ghi" == snake_case("AbcDefGhi")
    assert "abc_def_ghi" == snake_case("abc_def_ghi")
    assert "abc_def_ghi" == snake_case("abcDefGhi")
    assert "abc_def_ghi" == snake_case("abc-def-ghi")
    assert "abc_def_ghi" == snake_case("Abc Def Ghi")


def test_kebab_case():
    assert "abc-def-ghi" == kebab_case("AbcDefGhi")
    assert "abc-def-ghi" == kebab_case("abc_def_ghi")
    assert "abc-def-ghi" == kebab_case("abcDefGhi")
    assert "abc-def-ghi" == kebab_case("abc-def-ghi")
    assert "abc-def-ghi" == kebab_case("Abc Def Ghi")

def test_pascal_case():
    assert "AbcDefGhi" == pascal_case("AbcDefGhi")
    assert "AbcDefGhi" == pascal_case("abc_def_ghi")
    assert "AbcDefGhi" == pascal_case("abcDefGhi")
    assert "AbcDefGhi" == pascal_case("abc-def-ghi")
    assert "AbcDefGhi" == pascal_case("Abc Def Ghi")
