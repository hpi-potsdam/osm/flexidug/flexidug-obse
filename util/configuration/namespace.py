from rdflib.namespace import DefinedNamespace
from rdflib import URIRef
from ..ontology_reader import OntologyReader

ontology = OntologyReader("models/configuration-0.0.2.ttl")

class CNS(DefinedNamespace):
    
    _NS = ontology.get_namespace()


    CredentialConfiguration: URIRef = ontology.get_class('#CredentialConfiguration')
    EndpointConfiguration: URIRef = ontology.get_class('#EndpointConfiguration')
    KubernetesConfiguration: URIRef = ontology.get_class('#KubernetesConfiguration')


    portNumber: URIRef = ontology.get_datatype_property('#portNumber')
    hostName: URIRef = ontology.get_datatype_property('#hostName')
    environmentNamePortNumber: URIRef = ontology.get_datatype_property('#environmentNamePortNumber')

    credentialFilename: URIRef = ontology.get_datatype_property('#credentialFilename')
    namespace: URIRef = ontology.get_datatype_property('#namespace')


    hasConfiguration : URIRef = ontology.get_object_property('#hasConfiguration')
   