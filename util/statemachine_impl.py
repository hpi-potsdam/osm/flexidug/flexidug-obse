from rdflib import URIRef, RDFS

from obse.sparql_queries import SparQLWrapper

from .statemachine.namespace import SMNS
from util.name_case_style import pascal_case, snake_case


class StateMachine:

    def __init__(self, wrapper: SparQLWrapper, p_rdf: URIRef):

        self.wrapper = wrapper
        self.config = {
            "states": [],
            "events": [],
            "guards": [],
            "actions": [],
            "transitions": [],
        }
        name = self.wrapper.get_single_object_property(p_rdf, RDFS.label)

        self.config["classname"] = f"{pascal_case(name)}StateMachine"

        for p_sm_element in self.wrapper.get_out_references(p_rdf, SMNS.contains):
            sm_element_type = self.wrapper.get_type(p_sm_element)
            label = self.wrapper.get_single_object_property(
                p_sm_element, RDFS.label)

            if sm_element_type == SMNS.State:
                self.config['states'].append({
                    "name": label,
                    "var_name": f"state_{snake_case(label)}"
                })

            if sm_element_type == SMNS.InitialState:
                self.config['states'].append({
                    "name": label,
                    "var_name": f"state_{snake_case(label)}"
                })
                self.config["init_state"] = f"state_{snake_case(label)}"

            


            if sm_element_type == SMNS.Action:
                comments = []
                for comment in self.wrapper.get_single_object_property(p_sm_element,RDFS.comment).split('\n'):
                    comments.append({ "text" : comment})

                self.config['actions'].append({
                    "func_name": f"do_{snake_case(label)}",
                    "comments" : comments
                })
                
            if sm_element_type == SMNS.Guard:
                self.config['guards'].append({
                    "func_name": f"is_{snake_case(label)}"
                })
            if sm_element_type == SMNS.Event:
                self.config['events'].append({
                    "name": label,
                    "var_name": f"event_{snake_case(label)}"
                })
            if sm_element_type == SMNS.Transition:

                transition = {
                    "name": label,
                    "event": f"event_{snake_case(self._get_child_label(p_sm_element,SMNS.transitionEvent))}",
                    "guard": f"is_{snake_case(self._get_child_label(p_sm_element,SMNS.transitionGuard,default_label='true'))}",
                    "action": f"do_{snake_case(self._get_child_label(p_sm_element,SMNS.transitionAction,default_label='nop'))}",
                    "source": f"state_{snake_case(self._get_child_label(p_sm_element,SMNS.source))}",
                    "target": f"state_{snake_case(self._get_child_label(p_sm_element,SMNS.target))}",
                }
                self.config['transitions'].append(transition)

    def _get_child_label(self, p_rdf, prop: URIRef, default_label=None):
        p_children = self.wrapper.get_out_references(p_rdf, prop)

        if len(p_children) > 1:
            raise ValueError(
                f"More than one child found. For {p_rdf} found {p_children}")

        if len(p_children) == 0:
            if default_label:
                return default_label
            raise ValueError(f"No child {prop} found for {p_rdf}")

        return self.wrapper.get_single_object_property(p_children[0], RDFS.label)

    def get_config(self):
        return self.config

    def get_classname(self):
        return self.config["classname"]
