from rdflib.namespace import DefinedNamespace
from rdflib import URIRef
from ..ontology_reader import OntologyReader

ontology = OntologyReader("models/statemachine-1.0.0.ttl")

class SMNS(DefinedNamespace):

    _NS = ontology.get_namespace()

    StateMachine: URIRef = ontology.get_class('#StateMachine')
    State: URIRef = ontology.get_class('#State')
    InitialState: URIRef = ontology.get_class('#InitialState')
    FinalState: URIRef = ontology.get_class('#FinalState')
    Junction: URIRef = ontology.get_class('#Junction')
    Transition: URIRef = ontology.get_class('#Transition')
    Event: URIRef = ontology.get_class('#Event')
    Guard: URIRef = ontology.get_class('#Guard')
    Action: URIRef = ontology.get_class('#Action')


    contains: URIRef = ontology.get_object_property("#contains")
    source: URIRef = ontology.get_object_property("#source")
    target: URIRef = ontology.get_object_property("#target")
    transitionEvent: URIRef = ontology.get_object_property("#transitionEvent")
    transitionAction: URIRef = ontology.get_object_property("#transitionAction")
    transitionGuard: URIRef = ontology.get_object_property("#transitionGuard")

  