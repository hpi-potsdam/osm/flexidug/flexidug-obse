from rdflib.namespace import DefinedNamespace
from rdflib import URIRef
from util.ontology_reader import OntologyReader

ontology = OntologyReader("models/system-0.0.1.ttl")



class SNS(DefinedNamespace):

    _NS = ontology.get_namespace()

    Subsystem: URIRef = ontology.get_class('#Subsystem')
    Component: URIRef = ontology.get_class('#Component')
    Actor: URIRef = ontology.get_class('#Actor')
    Message: URIRef = ontology.get_class('#Message')
    Function: URIRef = ontology.get_class('#Function')
    Interface: URIRef = ontology.get_class('#Interface')

    messageName: URIRef = ontology.get_datatype_property('#messageName')

    usesComponent: URIRef = ontology.get_object_property('#usesComponent')
    hasComponent: URIRef = ontology.get_object_property('#hasComponent')
    hasSender: URIRef = ontology.get_object_property('#hasSender')
    hasReceiver: URIRef = ontology.get_object_property('#hasReceiver')
    hasFunction: URIRef = ontology.get_object_property('#hasFunction')
    requireInterface: URIRef = ontology.get_object_property('#requireInterface')
    provideInterface: URIRef = ontology.get_object_property('#provideInterface')
    isImplementedBy: URIRef = ontology.get_object_property('#isImplementedBy')

