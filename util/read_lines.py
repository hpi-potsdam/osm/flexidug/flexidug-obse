import os
#
# Hilfsfunktion der mitgegeben werden kann ob die Datei existieren muss. Falls nicht wird ein leeres Array zurückgegeben
# TODO kann die Verwendung ersetzt werden, das aktuell damit zeilenweise Konfigurationen eingelesen werden.

def read_lines(path,filename,must_exists = True):
    if not os.path.exists(path):
        raise ValueError(f"{path} must exists.")
    file_path = os.path.join(path,filename)
    if not os.path.exists(file_path):
        if must_exists:
            raise ValueError(f"{file_path} must exists.")
        return []
    with open(file_path,'r',encoding='UTF-8') as f:
        return f.readlines()
