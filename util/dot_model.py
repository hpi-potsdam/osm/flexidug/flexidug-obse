class DotModel:

    def __init__(self, name):
        self.name = name
        self.nodes = {}
        self.edges = []

    def add_node(self, shape, node_id, label):

        if shape not in self.nodes:
            self.nodes[shape] = []
        self.nodes[shape].append(f'{node_id} [label="{label}"]')

    def add_directed_edge(self, node_id1, node_id2):
        self.edges.append(f'{node_id1} -> {node_id2}')

    def serialize(self, filename):

        with open(filename, "w", encoding="UTF-8") as f:
            f.write(f"digraph {self.name} {{\n")

            for shape, node_list in self.nodes.items():
                f.write(f"node [shape={shape}]\n")
                f.write("\n".join(node_list))
                f.write("\n")
                
            f.write("\n".join(self.edges))
            f.write("}\n")
