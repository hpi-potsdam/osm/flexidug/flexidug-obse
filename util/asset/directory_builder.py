import os
from rdflib import URIRef
from obse.graphwrapper import GraphWrapper

from .namespace import ANS


class DirectoryBuilder():

    def __init__(self, _wrapper: GraphWrapper):
        self.wrapper = _wrapper
        self.cache = {}

    def get_directory(self, rdf_parent: URIRef, folder_path: str, key: str):

        _, folder_name = os.path.split(folder_path)

        hash_key = str(rdf_parent)+folder_name
        if hash_key in self.cache:
            return self.cache[hash_key]

        rdf_directory = self.wrapper.add_labeled_instance(ANS.Directory, folder_path.replace("\\", "/")+"-"+key)
        self.wrapper.add_str_property(ANS.path, rdf_directory, folder_name)
        self.wrapper.add_reference(ANS.hasSubdirectory, rdf_parent, rdf_directory)

        self.cache[hash_key] = rdf_directory
        return rdf_directory
