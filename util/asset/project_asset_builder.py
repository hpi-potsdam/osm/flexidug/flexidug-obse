import os
from rdflib import URIRef
from obse.graphwrapper import GraphWrapper

from .namespace import ANS
from .directory_builder import DirectoryBuilder


class IgnoreFilesAndTmpFolder():

    def ignore_folder(self, path: str):
        _, filename = os.path.split(path)
        if filename.lower() in ["tmp", "temp", ".pytest_cache", "__pycache__"]:
            return True
        return False

    def ignore_file(self, path: str):
        filename = os.path.split(path)[1]
        if filename == "requirements.txt":
            return False
        return True


class IgnorePythonIgnores():

    def ignore_folder(self, path: str):
        _, filename = os.path.split(path)
        if filename.lower() in ["tmp", "temp", ".pytest_cache", "__pycache__"]:
            return True
        return False
    
    def ignore_file(self, path: str):
        _, filename = os.path.split(path)
        if filename.lower() in ["__pycache__"]:
            return True
        return False


class ProjectAssetBuilder:

    def __init__(self, _wrapper: GraphWrapper, _directory_builder: DirectoryBuilder, _name: str):
        self.wrapper = _wrapper
        self.directory_builder = _directory_builder
        self.name = _name

    def add_asset(self, rdf_template, rdf_configuration, asset_name, rdf_directory, filename):

        # Asset that is generated with template and configuration
        rdf_asset = self.wrapper.add_labeled_instance(ANS.Asset, f"Generated {asset_name} {self.name}")

        rdf_target = self.wrapper.add_labeled_instance(ANS.Target, f"Generated {asset_name} {self.name}")
        self.wrapper.add_str_property(ANS.filename, rdf_target, filename)
        self.wrapper.add_reference(ANS.hasDirectory, rdf_target, rdf_directory)
        self.wrapper.add_reference(ANS.hasTarget, rdf_asset, rdf_target)

        self.wrapper.add_reference(ANS.hasTemplate, rdf_asset, rdf_template)
        self.wrapper.add_reference(ANS.hasConfiguration, rdf_asset, rdf_configuration)

    def copy_folder(self, s_path: str, rdf_source_directory: URIRef, rdf_target_directory: URIRef, ignore):

        # logging.info(f"Copy {s_path} to {t_path} {type(ignore)}")

        for file in os.listdir(s_path):
            source = os.path.join(s_path, file)

            if os.path.isdir(source):
                if ignore.ignore_folder(source):
                    continue

                # Copy folder

                rdf_source_subdirectory = self.directory_builder.get_directory(rdf_source_directory, source, key="Generated")
                rdf_target_subdirectory = self.directory_builder.get_directory(rdf_target_directory, source, key="Sandbox")

                self.copy_folder(source, rdf_source_subdirectory, rdf_target_subdirectory, ignore=IgnorePythonIgnores())

            if os.path.isfile(source):
                if ignore.ignore_file(source):
                    continue

                _, filename = os.path.split(source)
                source_path = source.replace("\\", "/")

                # Asset that is copied from source
                rdf_asset = self.wrapper.add_labeled_instance(ANS.Asset, f"Copied {source_path} {self.name}")

                rdf_target = self.wrapper.add_labeled_instance(ANS.Target, f"Copied {source_path} {self.name}")
                self.wrapper.add_str_property(ANS.filename, rdf_target, filename)
                self.wrapper.add_reference(ANS.hasDirectory, rdf_target, rdf_target_directory)
                self.wrapper.add_reference(ANS.hasTarget, rdf_asset, rdf_target)

                rdf_source = self.wrapper.add_labeled_instance(ANS.Source, f"Copied {source_path} {self.name}")
                self.wrapper.add_str_property(ANS.filename, rdf_source, filename)
                self.wrapper.add_reference(ANS.hasDirectory, rdf_source, rdf_source_directory)
                self.wrapper.add_reference(ANS.hasSource, rdf_asset, rdf_source)
