from rdflib.namespace import DefinedNamespace
from rdflib import URIRef
from ..ontology_reader import OntologyReader

ontology = OntologyReader("models/assets-0.0.2.ttl")


class ANS(DefinedNamespace):

    _NS = ontology.get_namespace()

    Asset: URIRef = ontology.get_class('#Asset')
    Target: URIRef = ontology.get_class('#Target')
    Source: URIRef = ontology.get_class('#Source')
    Template: URIRef = ontology.get_class('#Template')
    Directory: URIRef = ontology.get_class('#Directory')

    Configuration: URIRef = ontology.get_class('#Configuration')
    Dictionary: URIRef = ontology.get_class('#Dictionary')
    KeyValuePair: URIRef = ontology.get_class('#KeyValuePair')

    filename: URIRef = ontology.get_datatype_property('#filename')
    path: URIRef = ontology.get_datatype_property('#path')
    key: URIRef = ontology.get_datatype_property('#key')
    value: URIRef = ontology.get_datatype_property('#value')

    hasTarget: URIRef = ontology.get_object_property('#hasTarget')
    hasSource: URIRef = ontology.get_object_property('#hasSource')
    hasTemplate: URIRef = ontology.get_object_property('#hasTemplate')
    hasDirectory: URIRef = ontology.get_object_property('#hasDirectory')
    hasSubdirectory: URIRef = ontology.get_object_property('#hasSubdirectory')

    hasConfiguration: URIRef = ontology.get_object_property('#hasConfiguration')
    hasKeyValuePair: URIRef = ontology.get_object_property('#hasKeyValuePair')
